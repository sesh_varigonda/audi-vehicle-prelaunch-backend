﻿using System;
using System.ServiceProcess;
using log4net;

namespace Audi.Prelaunch.BackgroundService.Notifications
{
    static class Program
    {
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();

            var serviceToRun = new NotificationQueueService(); 
            
            if (Environment.UserInteractive)
            {
                serviceToRun.TestRun();
            }
            else
            {
                ServiceBase.Run(serviceToRun);    
            }
        }
    }
}
