﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Timers;
using Audi.Prelaunch.Business.Entities;
using Audi.Prelaunch.Business.Enum;
using Audi.Prelaunch.Marketo.Endpoints;
using Audi.Prelaunch.Marketo.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using StackExchange.Profiling.Helpers.Dapper;

namespace Audi.Prelaunch.BackgroundService.Notifications
{
    public partial class NotificationQueueService : ServiceBase
    {
        public NotificationQueueService()
        {
            InitializeComponent();
            if (!EventLog.SourceExists("PrelaunchNotificationBackgroundService"))
            {
                EventLog.CreateEventSource(
                    "PrelaunchNotificationBackgroundService", "PrelaunchNotificationQueueServiceLog");
            }
            eventLog.Source = "PrelaunchNotificationBackgroundService";
            eventLog.Log = "PrelaunchNotificationQueueServiceLog";
        }

        internal void TestRun()
        {
            OnTimer(null, null);
        }

        protected override void OnStart(string[] args)
        {
            eventLog.WriteEntry("Service Started");

            var pollInterval = int.Parse(ConfigurationManager.AppSettings["PollInterval"]);
            var timer = new Timer { Interval = pollInterval*1000 };
            timer.Elapsed += OnTimer;
            timer.Start();
        }

        protected override void OnStop()
        {
            eventLog.WriteEntry("Service Stopped");
        }

        public void OnTimer(object sender, ElapsedEventArgs args)
        {
            TriggerNotifications();
        }

        public void TriggerNotifications()
        {
            try
            {
                var notificationList = GetBookingNotifications();
                if(notificationList.Any())
                {
                    foreach (var notification in notificationList)
                    {
                        SendBookingNotification(notification);
                    }
                }
            }
            catch (Exception ex)
            {
                eventLog.WriteEntry("Notification Send Failed: " + ex, EventLogEntryType.Error);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private List<BookingNotificationInfo> GetBookingNotifications()
        {
            List<BookingNotificationInfo> bookingNotifications = new List<BookingNotificationInfo>();

            using (System.Data.IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString))
            {
                db.Open();
                bookingNotifications = db.Query<BookingNotificationInfo>("Select [BookingNotificationId], [BookingId], [NotificationCode], [Content], [NotificationStatusType] ,[CreateDate] ,[CreateSource] ,[UpdateDate] ,[UpdateSource] from audiBookingNotification").Where(a => a.NotificationStatusType == (int)NotificationStatusType.Queued).ToList();
                db.Close();
            }

            return bookingNotifications;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private List<NotificationInfo> GetNotifications()
        {
            List<NotificationInfo> notifications = new List<NotificationInfo>();

            using (System.Data.IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString))
            {
                db.Open();
                notifications = db.Query<NotificationInfo>("Select [NotificationId],[NotificationCode],[Name],[Source],[Description],[Type],[ExternalId],[Status],[CreateDate],[CreateSource] from audiNotification").ToList();
                db.Close();
            }

            return notifications;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        public void SendBookingNotification(BookingNotificationInfo data)
        {
            data.NotificationStatusType = (int) NotificationStatusType.Processing;
            UpdateBookingNotification(data);

            var externalId = GetNotifications().Where(a => a.NotificationCode == data.NotificationCode).First().ExternalId;

            try
            {
                eventLog.WriteEntry("Processing Notification.");

                var lead = new LeadEndpoint();
                var request = new LeadCreate() { input = GetCustomerLeadInput(data) };
                var result = lead.Create(request).FirstOrDefault();

                eventLog.WriteEntry(string.Format("Lead {0} Created/Updated", result.id.ToString()));

                if (result.reasons != null)
                {
                    eventLog.WriteEntry(string.Format("Unable to update the Lead Information : {0}", string.Join(",", result.reasons.Select(a => a.message))), EventLogEntryType.Error);
                    return;
                }

                var campaign = new CampaignEndpoint();
                var trigger = new CampaignRequest() { input = GetCampaignInput(result.id, data.Content) };

                var processed = campaign.Request(trigger, externalId);

                eventLog.WriteEntry(string.Format("Campaign Requested {0}", externalId.ToString()));

                data.NotificationStatusType = (int) NotificationStatusType.Processed;
                UpdateBookingNotification(data);

                eventLog.WriteEntry("Notification Successfully Processed.");
            }
            catch (Exception ex)
            {
                eventLog.WriteEntry(string.Format("Unable to process the request reason : {0}", ex.Message),  EventLogEntryType.Error);
                eventLog.WriteEntry(string.Format("Stack Trace : {0}", ex.StackTrace), EventLogEntryType.Error);

                data.NotificationStatusType = (int) NotificationStatusType.Failed;
                UpdateBookingNotification(data);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public int UpdateBookingNotification(BookingNotificationInfo data)
        {
            int rowsAffected = 0;

            using (System.Data.IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString))
            {
                db.Open();
                string sqlQuery = string.Format("Update audiBookingNotification SET NotificationStatusType = {0} Where BookingNotificationId = {1}", data.NotificationStatusType, data.BookingNotificationId);
                rowsAffected = db.Execute(sqlQuery);
                db.Close();
            }

            return rowsAffected;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private List<Dictionary<string, string>> GetCustomerLeadInput(BookingNotificationInfo data)
        {
            Dictionary<string, string> leadAttributes = new Dictionary<string, string>();

            var tokens = JsonConvert.DeserializeObject<List<CampaignRequestToken>>(data.Content, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            var testMode = bool.Parse(ConfigurationManager.AppSettings["TestMode"]);
            var testEmail = ConfigurationManager.AppSettings["TestEmail"];
            var whitelistDomains = ConfigurationManager.AppSettings["WhiteListDomains"].Split(new[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (var token in tokens.Where(a => !a.name.Contains("my.")))
            {
                if (testMode && token.name.ToLower() == "email")
                {
                    leadAttributes.Add(token.name, testEmail);
                }
                else
                {
                    if(token.name.ToLower() == "email")
                    {
                        eventLog.WriteEntry(string.Format("Processing Customer Order, EmailAddress : {0}", token.value), EventLogEntryType.Information);
                    }

                    leadAttributes.Add(token.name, token.value);
                }
            }

            return new List<Dictionary<string, string>>()
            {
                leadAttributes
            };
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="leadId"></param>
        /// <param name="tokenData"></param>
        /// <returns></returns>
        private static CampaignRequestInput GetCampaignInput(int leadId, string tokenData)
        {
            var tokens = JsonConvert.DeserializeObject<List<CampaignRequestToken>>(tokenData, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            return new CampaignRequestInput
            {
                leads = new List<CampaignRequestLead> { new CampaignRequestLead() { id = leadId } },
                tokens = tokens.Where(a => a.name.Contains("my.")).ToList()
            };
        }
    }
}
