﻿using System.Collections.Generic;
// ReSharper disable InconsistentNaming

namespace Audi.Prelaunch.Marketo.Models
{
    public class CampaignRequest
    {
        public CampaignRequestInput input { get; set; }
    }

    public class CampaignRequestInput
    {
        public List<CampaignRequestLead> leads { get; set; }
        public List<CampaignRequestToken> tokens { get; set; }
    }

    public class CampaignRequestToken
    {
        public string name { get; set; }
        public string value { get; set; }
    }

    public class CampaignRequestLead
    {
        public int id { get; set; }
    }

    public class CampaignRequestResponse
    {
        public string requestId { get; set; }
        public bool success { get; set; }
        public ErrorResult[] errors { get; set; }
    }
}
