﻿// ReSharper disable InconsistentNaming

namespace Audi.Prelaunch.Marketo.Models
{
    public class ErrorResult
    {
        public string code { get; set; }
        public string message { get; set; }
    }
}
