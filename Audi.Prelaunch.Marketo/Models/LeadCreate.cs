﻿using System.Collections.Generic;
// ReSharper disable InconsistentNaming

namespace Audi.Prelaunch.Marketo.Models
{
    public class LeadCreate
    {
        public List<Dictionary<string,string>> input { get; set; }
    }

    public class LeadCreateResponse
    {
        public string requestId { get; set; }
        public bool success { get; set; }
        public LeadCreateResult[] result { get; set; }
        public ErrorResult[] errors { get; set; }
    }

    public class LeadCreateResult
    {
        public int id { get; set; }
        public string status { get; set; }
        public ErrorResult[] reasons { get; set; }
    }
}
