﻿using System;
using System.Configuration;
using System.Net.Http;
using System.Net.Http.Headers;
using Audi.Prelaunch.Marketo.Models;

namespace Audi.Prelaunch.Marketo
{
    public class Endpoint : IDisposable
    {
        readonly HttpClient _client = new HttpClient();
        readonly TokenInfo _tokenInfo;

        readonly string _clientId = ConfigurationManager.AppSettings["MarketoClientId"];
        readonly string _clientSecret = ConfigurationManager.AppSettings["MarketoClientSecret"];
        readonly string _grantType = ConfigurationManager.AppSettings["MarketoGrantType"];
        readonly string _baseUrl = ConfigurationManager.AppSettings["MarketoBaseUrl"];
        readonly string _identityEndpoint = ConfigurationManager.AppSettings["MarketoIdentityEndpoint"];
        readonly string _createLeadEndpoint = ConfigurationManager.AppSettings["MarketoCreateLeadEndpoint"];
        readonly string _requestCampaignEndpoint = ConfigurationManager.AppSettings["MarketoRequestCampaignEndpoint"];

        public HttpClient Client { get { return _client; } }
        public TokenInfo TokenInfo { get { return _tokenInfo; } }
        public string CreateLeadEndpoint { get { return _createLeadEndpoint; } }
        public string RequestCampaignEndpoint { get { return _requestCampaignEndpoint; } }

        public Endpoint()
        {
            _client.BaseAddress = new Uri(_baseUrl);
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var response = _client.GetAsync(string.Format(_identityEndpoint, _grantType, _clientId, _clientSecret)).Result;

            if (!response.IsSuccessStatusCode) throw new Exception("Marketo did not return Authentication Token");

            _tokenInfo = response.Content.ReadAsAsync<TokenInfo>().Result;
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}
