﻿using System;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Audi.Prelaunch.Marketo.Models;
using log4net;
using System.Linq;

namespace Audi.Prelaunch.Marketo.Endpoints
{
    public class CampaignEndpoint : Endpoint
    {
        ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public bool Request(CampaignRequest campaign, int campaignId)
        {
            var serializer = new JavaScriptSerializer();
            var data = serializer.Serialize(campaign);

            var content = new StringContent(data, Encoding.UTF8, "application/json");
            var response = Client.PostAsync(string.Format(RequestCampaignEndpoint, TokenInfo.access_token, campaignId), content).Result;

            if (!response.IsSuccessStatusCode) throw new Exception("Request Campaign Failed");

            var result = response.Content.ReadAsAsync<CampaignRequestResponse>().Result;

            if (result.errors != null && result.errors.Length > 0)
            {
                logger.DebugFormat("Unable to send an email campaign out: {0}", string.Join(",", result.errors.Select(a => a.message)));
                return false;
            }

            if (result.success) return true;

            throw new Exception(result.errors != null ? serializer.Serialize(result.errors) : "Request Campaign Failed for Payload |" + data + "|");
        }
    }
}
