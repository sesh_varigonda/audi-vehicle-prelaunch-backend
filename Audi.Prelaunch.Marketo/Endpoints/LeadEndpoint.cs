﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Script.Serialization;
using Audi.Prelaunch.Marketo.Models;
using log4net;

namespace Audi.Prelaunch.Marketo.Endpoints
{
    public class LeadEndpoint : Endpoint
    {
        ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<LeadCreateResult> Create(LeadCreate lead)
        {
            var serializer = new JavaScriptSerializer();
            var data = serializer.Serialize(lead);

            var content = new StringContent(data, Encoding.UTF8, "application/json");
            var response = Client.PostAsync(string.Format(CreateLeadEndpoint, TokenInfo.access_token), content).Result;

            if (!response.IsSuccessStatusCode) throw new Exception("Lead Create/Update Failed");

            var rtn = response.Content.ReadAsAsync<LeadCreateResponse>().Result;

            logger.DebugFormat("Marketo Response {0}", serializer.Serialize(rtn));

            if (rtn.success && rtn.result != null)
            {
                var output = rtn.result.ToList();
                if (output.Count > 0 && output.First().id > 0) return output;
            }

            throw new ApplicationException(rtn.errors != null ? serializer.Serialize(rtn.errors) : "Lead Create/Update Failed for Payload");
        }

        public List<LeadCreateResult> Create(string serializedData)
        {
            var serializer = new JavaScriptSerializer();

            var content = new StringContent(serializedData, Encoding.UTF8, "application/json");
            var response = Client.PostAsync(string.Format(CreateLeadEndpoint, TokenInfo.access_token), content).Result;

            if (!response.IsSuccessStatusCode) throw new Exception("Lead Create/Update Failed");

            var rtn = response.Content.ReadAsAsync<LeadCreateResponse>().Result;

            if (rtn.success && rtn.result != null)
            {
                var output = rtn.result.ToList();
                if (output.Count > 0 && output.First().id > 0) return output;
            }

            throw new ApplicationException(rtn.errors != null ? serializer.Serialize(rtn.errors) : "Lead Create/Update Failed for Payload |" + serializedData + "|");
        }
    }
}
