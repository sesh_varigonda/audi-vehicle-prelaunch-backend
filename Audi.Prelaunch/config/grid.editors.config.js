[
    {
        "name": "Rich text editor",
        "alias": "rte",
        "view": "rte",
        "icon": "icon-article"
    },
    {
        "name": "Image",
        "alias": "media",
        "view": "media",
        "icon": "icon-picture"
    },
    {
        "name": "Macro",
        "alias": "macro",
        "view": "macro",
        "icon": "icon-settings-alt"
    },
    {
        "name": "Embed",
        "alias": "embed",
        "view": "embed",
        "icon": "icon-movie-alt"
    },
    {
        "name": "Headline",
        "alias": "headline",
        "view": "textstring",
        "icon": "icon-coin",
        "config": {
            "style": "font-size: 36px; line-height: 45px; font-weight: bold",
            "markup": "<h1>#value#</h1>"
        }
    },
    {
        "name": "Quote",
        "alias": "quote",
        "view": "textstring",
        "icon": "icon-quote",
        "config": {
            "style": "border-left: 3px solid #ccc; padding: 10px; color: #ccc; font-family: serif; font-variant: italic; font-size: 18px",
            "markup": "<blockquote>#value#</blockquote>"
        }
    },
    {
        "name": "Section Intro",
        "alias": "sectionIntro",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-quote",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Html Text",
                    "alias": "htmlText",
                    "propretyType": {},
                    "dataType": "ca90c950-0aff-4e72-b976-a30b1ac57dad"
                }
            ],
            "frontView": "",
            "renderInGrid": "1"
        }
    },
    {
        "name": "Section Text",
        "alias": "sectionText",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-edit",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Html Text",
                    "alias": "htmlText",
                    "propretyType": {},
                    "dataType": "ca90c950-0aff-4e72-b976-a30b1ac57dad"
                }
            ],
            "frontView": "",
            "renderInGrid": "1",
            "min": 1,
            "max": 3
        }
    },
    {
        "name": "Section Gallery",
        "alias": "sectionGallery",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-window-sizes",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Media Type",
                    "alias": "contentMediaType",
                    "propretyType": {},
                    "dataType": "c2cf9dcc-fd66-4c30-a780-b46de4f0cfb0"
                },
                {
                    "name": "Media",
                    "alias": "media",
                    "propretyType": {},
                    "dataType": "18597760-8cee-492d-9f61-f4bf9a91868e"
                },
                {
                    "name": "Alternate Text",
                    "alias": "altText",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                }
            ],
            "min": 1,
            "max": 20,
            "frontView": "",
            "renderInGrid": "1"
        }
    },
    {
        "name": "Section Left Copy",
        "alias": "sectionLeftCopy",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-navigation-left",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Media Type",
                    "alias": "contentMediaType",
                    "propretyType": {},
                    "dataType": "c2cf9dcc-fd66-4c30-a780-b46de4f0cfb0"
                },
                {
                    "name": "Media",
                    "alias": "media",
                    "propretyType": {},
                    "dataType": "18597760-8cee-492d-9f61-f4bf9a91868e"
                },
                {
                    "name": "Alternate Text",
                    "alias": "altText",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "Html Text",
                    "alias": "htmlText",
                    "propretyType": {},
                    "dataType": "ca90c950-0aff-4e72-b976-a30b1ac57dad"
                }
            ],
            "frontView": "",
            "renderInGrid": "1",
            "min": 1,
            "max": 1,
            "expiration": 360
        }
    },
    {
        "name": "Section Right Copy",
        "alias": "sectionRightCopy",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-arrow-right",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Media Type",
                    "alias": "contentMediaType",
                    "propretyType": {},
                    "dataType": "c2cf9dcc-fd66-4c30-a780-b46de4f0cfb0"
                },
                {
                    "name": "Media",
                    "alias": "media",
                    "propretyType": {},
                    "dataType": "18597760-8cee-492d-9f61-f4bf9a91868e"
                },
                {
                    "name": "Alternate Text",
                    "alias": "altText",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                },
                {
                    "name": "Html Text",
                    "alias": "htmlText",
                    "propretyType": {},
                    "dataType": "ca90c950-0aff-4e72-b976-a30b1ac57dad"
                }
            ],
            "frontView": "",
            "renderInGrid": "1",
            "min": 1,
            "max": 1,
            "expiration": 360
        }
    },
    {
        "name": "Section Image Short Copy",
        "alias": "sectionImageShortCopy",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-umb-media",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Media",
                    "alias": "media",
                    "propretyType": {},
                    "dataType": "18597760-8cee-492d-9f61-f4bf9a91868e"
                },
                {
                    "name": "MediaType",
                    "alias": "mediaType",
                    "propretyType": {},
                    "dataType": "836d9e1b-0edf-482a-8e5d-daa8cc6a7ba0"
                },
                {
                    "name": "Url",
                    "alias": "url",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae",
                    "description": ""
                },
                {
                    "name": "Html Text",
                    "alias": "htmlText",
                    "propretyType": {},
                    "dataType": "ca90c950-0aff-4e72-b976-a30b1ac57dad"
                }
            ],
            "renderInGrid": "1",
            "frontView": "",
            "min": 1,
            "max": 1,
            "expiration": 360
        }
    },
    {
        "name": "Doc Type",
        "alias": "docType",
        "view": "/App_Plugins/DocTypeGridEditor/Views/doctypegrideditor.html",
        "render": "/App_Plugins/DocTypeGridEditor/Render/DocTypeGridEditor.cshtml",
        "icon": "icon-item-arrangement",
        "config": {
            "allowedDocTypes": [],
            "nameTemplate": "",
            "enablePreview": true,
            "viewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/",
            "previewViewPath": "/Views/Partials/Grid/Editors/DocTypeGridEditor/Previews/",
            "previewCssFilePath": "",
            "previewJsFilePath": ""
        }
    },
    {
        "name": "Section RTE",
        "alias": "sectionRTE",
        "view": "rte",
        "icon": "icon-settings-alt"
    },
    {
        "name": "Section Image",
        "alias": "sectionImage",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-screensharing",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Media",
                    "alias": "media",
                    "propretyType": {},
                    "dataType": "18597760-8cee-492d-9f61-f4bf9a91868e"
                },
                {
                    "name": "Alternate Text",
                    "alias": "altText",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                }
            ],
            "min": 1,
            "max": 3,
            "frontView": "",
            "renderInGrid": "1"
        }
    },
    {
        "name": "Section Quote",
        "alias": "sectionQuote",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-quote",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "renderInGrid": "1",
            "frontView": "",
            "editors": [
                {
                    "name": "Orientation",
                    "alias": "orientation",
                    "propretyType": {},
                    "dataType": "0adb1187-d2c7-4487-b904-aaa1cf175375"
                },
                {
                    "name": "Html Text",
                    "alias": "htmlText",
                    "propretyType": {},
                    "dataType": "ca90c950-0aff-4e72-b976-a30b1ac57dad"
                },
                {
                    "name": "Quote",
                    "alias": "quote",
                    "propretyType": {},
                    "dataType": "ca90c950-0aff-4e72-b976-a30b1ac57dad"
                }
            ]
        }
    },
    {
        "name": "Section Author Quote",
        "alias": "sectionAuthorQuote",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-settings-alt",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Quote",
                    "alias": "quote",
                    "propretyType": {},
                    "dataType": "ca90c950-0aff-4e72-b976-a30b1ac57dad"
                }
            ],
            "frontView": ""
        }
    },
    {
        "name": "Section Subtitle",
        "alias": "sectionSubtitle",
        "view": "/App_Plugins/LeBlender/editors/leblendereditor/LeBlendereditor.html",
        "icon": "icon-settings-alt",
        "render": "/App_Plugins/LeBlender/editors/leblendereditor/views/Base.cshtml",
        "config": {
            "editors": [
                {
                    "name": "Subtitle",
                    "alias": "plainText",
                    "propretyType": {},
                    "dataType": "0cc0eba1-9960-42c9-bf9b-60e150b429ae"
                }
            ],
            "frontView": "",
            "renderInGrid": "1"
        }
    }
]