using System.Configuration;
using System.Linq;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Description;
using Swashbuckle.Application;
using Swashbuckle.Swagger;
using WebActivatorEx;
using Audi.Prelaunch.Cms;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace Audi.Prelaunch.Cms
{
    /// <summary>
    /// 
    /// </summary>
    public class SwaggerConfig
    {
        /// <summary>
        /// 
        /// </summary>
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;
            var enableSwagger = bool.Parse(ConfigurationManager.AppSettings["EnableSwagger"]);

            if (enableSwagger)
            {
                GlobalConfiguration.Configuration
                    .EnableSwagger("docs/{apiVersion}", c =>
                    {
                        c.MultipleApiVersions(
                            ResolveVersionSupportByRouteConstraint,
                            (vc) =>
                            {
                                vc.Version("ContentApiV1", "Version 1").Description("<span style='font-size:12px;font-weight:bold'>AUDI Prelaunch CMS REST API INTERACTIVE DOCUMENTATION</span>");
                            });
                        c.DocumentFilter<RemoveVerbsFilter>();
                        c.IncludeXmlComments(HostingEnvironment.MapPath("/App_Data/Audi.Prelaunch.Cms.XML"));
                        c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
                    })
                    .EnableSwaggerUi(c =>
                    {
                        c.InjectStylesheet(thisAssembly, "Audi.Prelaunch.Cms.Content.styles.css");
                        c.DisableValidator();
                        c.CustomAsset("index", thisAssembly, "Audi.Prelaunch.Cms.Content.index.html");
                        c.EnableDiscoveryUrlSelector();
                        c.DocExpansion(DocExpansion.List);
                    });
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="apiDesc"></param>
        /// <param name="targetApiVersion"></param>
        /// <returns></returns>
        public static bool ResolveVersionSupportByRouteConstraint(ApiDescription apiDesc, string targetApiVersion)
        {
            return apiDesc.Route.RouteTemplate.Split('/').ToList().Contains(targetApiVersion) || apiDesc.Route.RouteTemplate.Split('/').ToList().Contains("1");
        }

        /// <summary>
        /// 
        /// </summary>
        public class RemoveVerbsFilter : IDocumentFilter
        {
            public void Apply(SwaggerDocument swaggerDoc, SchemaRegistry schemaRegistry, IApiExplorer apiExplorer)
            {
                var keyList = swaggerDoc.paths.Keys.ToList();
                foreach (string key in keyList)
                {
                    if (!key.ToLower().StartsWith("/1/") && !key.ToLower().StartsWith("/cms/"))
                    {
                        //swaggerDoc.paths[key] = null;
                    }
                }
            }
        }
    }
}
