﻿using System.Configuration;
using System.Web.Http;
using System.Web.Http.Cors;
using Audi.Prelaunch.Cms.Business.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Audi.Prelaunch.Cms
{
    /// <summary>
    /// 
    /// </summary>
    public static class WebApiConfig
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        public static void Register(HttpConfiguration config)
        {
            var origins = ConfigurationManager.AppSettings["CorsAllowedOrigins"];

            var settings = config.Formatters.JsonFormatter.SerializerSettings;

            settings.Formatting = Formatting.Indented;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            settings.NullValueHandling = NullValueHandling.Ignore;

            // Web API configuration and services
            var cors = new EnableCorsAttribute(origins, "*", "*") { SupportsCredentials = true, PreflightMaxAge = 1728000 };
            config.EnableCors(cors);

            // Web API / odata routes
            config.MapHttpAttributeRoutes();

            //Configure HTTP Caching using Entity Tags (ETags)
            //config.MessageHandlers.Add(CachingFactory.GetCachingHandlerByCacheStore(config, CacheStores.SqlCacheStore));

            //config.Routes.MapHttpRoute("DefaultApi", "{controller}/{id}", new { id = RouteParameter.Optional });
        }
    }
}