﻿using System;
using Umbraco.Web;
using System.Web.Http;
using System.Web.Routing;
using System.Configuration;
using System.Diagnostics;
using System.Net;

namespace Audi.Prelaunch.Cms
{
    /// <summary>
    /// 
    /// </summary>
    public class Global : UmbracoApplication
    {
        /// <summary>
        /// 
        /// </summary>
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            log4net.Config.XmlConfigurator.Configure();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void OnApplicationStarted(object sender, EventArgs e)
        {
            log4net.Config.XmlConfigurator.Configure();

            base.OnApplicationStarted(sender, e);

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            GlobalConfiguration.Configuration.EnsureInitialized();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void OnApplicationEnd(object sender, EventArgs e)
        {
            if (Debugger.IsAttached) return;

            var client = new WebClient();
            var url = ConfigurationManager.AppSettings["BaseUrl"] + "/1/ping";

            client.DownloadString(url);
            Trace.WriteLine("Application Shut Down Ping: " + url);
        }
    }
}