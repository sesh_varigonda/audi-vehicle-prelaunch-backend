﻿using System;
using System.Collections.Generic;
using System.IO;
using Umbraco.Core.IO;
using System.Text.RegularExpressions;
using System.Net;

namespace Audi.Prelaunch.Cms
{
    /// <summary>
    /// 
    /// </summary>
    public class CustomFileSystemProvider : IFileSystem
    {
        private IFileSystem _provider { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="virtualRoot"></param>
        public CustomFileSystemProvider(string virtualRoot)
        {
            _provider = new PhysicalFileSystem(virtualRoot);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rootPath"></param>
        /// <param name="rootUrl"></param>
        public CustomFileSystemProvider(string rootPath, string rootUrl)
        {
            _provider = new PhysicalFileSystem(rootPath, rootUrl);
        }

        #region physicalfilesystem

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public IEnumerable<string> GetDirectories(string path)
        {
            return _provider.GetDirectories(path);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        public void DeleteDirectory(string path)
        {
            _provider.DeleteDirectory(path);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="recursive"></param>
        public void DeleteDirectory(string path, bool recursive)
        {
            _provider.DeleteDirectory(path, recursive);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool DirectoryExists(string path)
        {
            return _provider.DirectoryExists(GetFullPath(path));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="stream"></param>
        public void AddFile(string path, Stream stream)
        {
            _provider.AddFile(path, stream);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="stream"></param>
        /// <param name="overrideIfExists"></param>
        public void AddFile(string path, Stream stream, bool overrideIfExists)
        {
            _provider.AddFile(path, stream, overrideIfExists);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public IEnumerable<string> GetFiles(string path)
        {
            return _provider.GetFiles(path, path);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public IEnumerable<string> GetFiles(string path, string filter)
        {
            return _provider.GetFiles(path, filter);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public Stream OpenFile(string path)
        {
            return _provider.OpenFile(path);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        public void DeleteFile(string path)
        {
            _provider.DeleteFile(path);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool FileExists(string path)
        {
            return _provider.FileExists(path);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fullPathOrUrl"></param>
        /// <returns></returns>
        public string GetRelativePath(string fullPathOrUrl)
        {
            return _provider.GetRelativePath(fullPathOrUrl);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string GetFullPath(string path)
        {
            return _provider.GetFullPath(path);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public string GetUrl(string path)
        {
            return _provider.GetUrl(path);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public DateTimeOffset GetCreated(string path)
        {
            return _provider.GetCreated(path);
        }

        #endregion physicalfilesystem

        #region customcode

        /// <summary>
        /// 
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public DateTimeOffset GetLastModified(string path)
        {
            // ML - Work around for using a UNC DFS path
            if (Regex.IsMatch(path, "^(http|https)://.*$"))
            {
                var request = WebRequest.Create(path) as HttpWebRequest;

                if (request != null)
                {
                    request.Method = "HEAD";

                    var response = request.GetResponse() as HttpWebResponse;

                    if (response != null)
                    {
                        if (response.StatusCode == HttpStatusCode.OK)
                        {
                            return response.LastModified;
                        }
                    }
                }
            }

            return _provider.GetLastModified(path);
        }

        #endregion
    }
}