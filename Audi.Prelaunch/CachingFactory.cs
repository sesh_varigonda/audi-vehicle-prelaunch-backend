﻿using System;
using System.Configuration;
using CacheCow.Server;
using CacheCow.Server.EntityTagStore.SqlServer;
using System.Net.Http.Headers;
using CacheCow.Common;
using CacheCow.Server.CacheRefreshPolicy;

namespace Audi.Prelaunch.Cms.Business.Helpers
{
    public static class CachingFactory
    {
        /// <summary>
        /// Gets the caching handler by cache store.
        /// </summary>
        /// <param name="cacheStore">The cache store.</param>
        /// <returns></returns>
        public static CachingHandler GetCachingHandlerByCacheStore(System.Web.Http.HttpConfiguration configuration, CacheStores cacheStore)
        {
            var cachingHandler = new CachingHandler(configuration, "Authorization");
            switch (cacheStore)
            {
                case CacheStores.SqlCacheStore:
                    cachingHandler = WithSqlCacheStore(configuration);
                    break;
                default:
                    break;
            }
            return cachingHandler;
        }

        /// <summary>
        /// With the SQL cache store.
        /// </summary>
        /// <returns></returns>
        private static CachingHandler WithSqlCacheStore(System.Web.Http.HttpConfiguration config)
        {
            //You need to execute *.sql script included in the nuget package folder of CacheCow
            var connectionString = ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
            var eTagStore = new SqlServerEntityTagStore(connectionString);
            var cacheHandler = new CachingHandler(config, eTagStore, "Authorization")
            {
                CacheControlHeaderProvider = (message, configuration) => new CacheControlHeaderValue()
                {
                    Private = true,
                    MustRevalidate = true,
                    MaxAge = TimeSpan.Zero
                },

                CacheRefreshPolicyProvider = new AttributeBasedCacheRefreshPolicy(TimeSpan.FromSeconds(5 * 60 * 60)).GetCacheRefreshPolicy
            };

            return cacheHandler;
        }

        public static IEntityTagStore GetTagStore(CacheStores cacheStore)
        {
            IEntityTagStore eTagStore = null;

            switch (cacheStore)
            {
                case CacheStores.SqlCacheStore:
                    var connectionString = ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString;
                    eTagStore = new SqlServerEntityTagStore(connectionString);
                    break;
                default:
                    break;
            }

            return eTagStore;
        }
    }

    public enum CacheStores
    {
        /// <summary>
        /// The SQL cache store
        /// </summary>
        SqlCacheStore = 0,
        /// <summary>
        /// The memory cache store
        /// </summary>
        MemoryCacheStore = 1,
        /// <summary>
        /// The memcached cache store
        /// </summary>
        MemcachedCacheStore = 2,
        /// <summary>
        /// The mongo database cache store
        /// </summary>
        MongoDbCacheStore = 3
    }
}