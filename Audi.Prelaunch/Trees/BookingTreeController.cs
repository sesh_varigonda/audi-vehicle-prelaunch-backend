﻿using System;
using Umbraco.Core;
using Umbraco.Web.Models.Trees;
using Umbraco.Web.Mvc;
using Umbraco.Web.Trees;
using System.Net.Http.Formatting;
using Umbraco.Web;

namespace Audi.Prelaunch.Cms.Trees
{
    /// <summary>
    /// 
    /// </summary>
    [Tree("custom", "custom", "Custom")]
    [PluginController("Custom")]
    public class CustomTreeController : TreeController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="queryStrings"></param>
        /// <returns></returns>
        protected override MenuItemCollection GetMenuForNode(string id, FormDataCollection queryStrings)
        {
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="queryStrings"></param>
        /// <returns></returns>
        protected override TreeNodeCollection GetTreeNodes(string id, FormDataCollection queryStrings)
        {
            //check if we're rendering the root node's children
            if (id == "-1")
            {
                var nodes = new TreeNodeCollection();
                nodes.Add(CreateTreeNode("booking", id, queryStrings, "Booking", "icon-people", false, FormDataCollectionExtensions.GetValue<string>(queryStrings, "application") + StringExtensions.EnsureStartsWith(this.TreeAlias, '/') + "/overviewBooking/all"));
                nodes.Add(CreateTreeNode("notification", id, queryStrings, "Notification", "icon-mailbox", false, FormDataCollectionExtensions.GetValue<string>(queryStrings, "application") + StringExtensions.EnsureStartsWith(this.TreeAlias, '/') + "/notification/all"));
                return nodes;
            }

            //this tree doesn't suport rendering more than 2 levels
            throw new NotSupportedException();
        }
    }
}