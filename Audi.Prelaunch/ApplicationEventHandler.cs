﻿using Umbraco.Core;
using log4net;
using System.Reflection;
using Umbraco.Core.Persistence;
using System;
using System.Linq;
using Umbraco.Core.Services;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using System.Web.Configuration;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Our.Umbraco.Ditto;
using Audi.Prelaunch.Models;
using System.Configuration;
using System.Net;
using System.Web;
using System.Drawing;
using System.IO;
using Audi.Prelaunch.Business.Service;
using Audi.Prelaunch.Business.Helpers;
using Audi.Prelaunch.Business.Entities;

namespace Audi.Prelaunch.Cms
{
    /// <summary>
    /// This function will update publish date property only the first time.
    /// </summary>
    public class ApplicationEventHandler : Umbraco.Core.ApplicationEventHandler
    {
        ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Table Names
        /// </summary>
        public const string TABLE_NOTIFICATION = "audiNotification";
        public const string TABLE_BOOKING = "audiBooking";
        public const string TABLE_BOOKINGNOTIFICATION = "audiBookingNotification";
        public const string TABLE_TRADEVEHICLE = "audiTradeVehicle";
        public const string TABLE_JOB = "audiJob";
        public const string TABLE_IDSDATA= "audiIDSData";
        public const string TABLE_IDSCODE = "audiIDSCode";
        public const string TABLE_PAINTCODE = "audiPaintCode";
        public const string TABLE_CART = "audiCart";

        /// <summary>
        /// Triggering events on application initialisation.
        /// </summary>
        /// <param name="umbracoApplication"></param>
        /// <param name="applicationContext"></param>
        protected override void ApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            ContentService.Publishing += ContentService_Publishing;
            ContentService.Saving += ContentService_Saving;
        }

        /// <summary>
        /// Modifying the CMS Content on Publishing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ContentService_Publishing(Umbraco.Core.Publishing.IPublishingStrategy sender, Umbraco.Core.Events.PublishEventArgs<Umbraco.Core.Models.IContent> e)
        {
            //To do
        }

        #region Generate Images

        /// <summary>
        /// Modifying the CMS Content on Saving
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="saveEventArgs"></param>
        private void ContentService_Saving(IContentService sender, SaveEventArgs<IContent> saveEventArgs)
        {
            ResizeImages(sender, saveEventArgs);
        }

        /// <summary>
        /// Create Carousel Images for the Homepage.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="saveEventArgs"></param>
        private void ResizeImages(IContentService sender, SaveEventArgs<IContent> saveEventArgs)
        {
            string nodeTypeAlias = "model";
            foreach (var content in saveEventArgs.SavedEntities.Where(a => a.ContentType.Alias == nodeTypeAlias))
            {
                //Create Resized Images for Model
                var exteriors = content.GetValue("exteriors");
                var updateImages = bool.Parse(content.GetValue("updateImages").ToString() == "0" ? "false" : "true");
                if (exteriors != null)
                {
                    CreateResizedImage(content, exteriors, updateImages);
                }
            }
        }

        /// <summary>
        /// Cropping vehicle exterior Images.
        /// </summary>
        /// <param name="content"></param>
        /// <param name="exteriors"></param>
        /// <param name="updateImages"></param>
        private void CreateResizedImage(IContent content, object exteriors, bool updateImages)
        {
            ModelService modelService = new ModelService();

            List<string> perspectives = new List<string>() { "aoa01open", "aoa14open", "aoa12open", "aoa11open", "aoa03open" };
            List<string> colorCodes = new List<string>() { "A2A2", "H1H1", "L5L5", "S1S1", "Y6Y6", "0E0E", "2D2D", "2L2L", "2Y2Y", "3P3P", "6Y6Y", "7M7M" };

            var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);

            List<IPublishedContent> contentItems = new List<IPublishedContent>();
            var exteriorImages = JsonConvert.DeserializeObject<List<ExteriorImage>>(exteriors.ToString(), new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(), NullValueHandling = NullValueHandling.Ignore, Formatting = Formatting.None });
            foreach (var exterior in exteriorImages)
            {
                if(colorCodes.Where(a=>a == exterior.code).Any())
                {
                    if (exterior.image != null)
                    {
                        if (updateImages)
                        {
                            var imageList = ResizeImages(exterior);

                            //Delete Old MediaItems
                            var images = exterior.image.Split(',');
                            foreach (var imageUrl in images)
                            {
                                var image = umbracoHelper.TypedMedia(imageUrl).As<MediaInfo>();
                                if (image != null)
                                {
                                    modelService.DeleteMediaItem(image.Id);
                                }
                            }

                            exterior.image = imageList;
                        }
                    }
                    else
                    {
                        exterior.key = Guid.NewGuid().ToString();
                        exterior.image = ResizeImages(exterior);
                    }
                }
            }

            //Updating the contentItem
            content.SetValue("exteriors", JsonConvert.SerializeObject(exteriorImages));
        }

        private string ResizeImages(ExteriorImage exterior)
        {
            ModelService modelService = new ModelService();

            List<string> perspectives = new List<string>() { "aoa01open", "aoa14open", "aoa12open", "aoa11open", "aoa03open" };
            List<string> colorCodes = new List<string>() { "A2A2", "H1H1", "L5L5", "S1S1", "Y6Y6", "0E0E", "2D2D", "2L2L", "2Y2Y", "3P3P", "6Y6Y", "7M7M" };

            int width = int.Parse(WebConfigurationManager.AppSettings["ImageResizeWidth"]);
            int height = int.Parse(WebConfigurationManager.AppSettings["ImageResizeHeight"]);
            string fileNameSuffix = WebConfigurationManager.AppSettings["ImageResizeSuffix"];
            string physicalFilePath = WebConfigurationManager.AppSettings["PhysicalPath"];

            var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);
            ImageHelper imageHelper = new ImageHelper();

            var mediaItemUrlCollection = new List<string>();
            foreach (var item in perspectives)
            {
                string fileName = string.Format("exterior_{0}_{1}.png", exterior.code, item);
                string imageUrl = string.Format(ConfigurationManager.AppSettings["MediaServiceUrl"], item, exterior.code);

                logger.DebugFormat("Creating Media Item {0}", fileName);
                var mediaItem = CreateMedia(fileName, imageUrl, fileName);
                logger.DebugFormat("Successfully Created Media Item {0}", fileName);

                var media = umbracoHelper.Media(mediaItem.Id);
                var relativeUrl = imageHelper.ToRelative(new Uri(media.Url));

                logger.DebugFormat("Media ImagePath {0}", relativeUrl);

                string originalFilePath = string.Format("{0}{1}", physicalFilePath, relativeUrl.Replace("/", "\\"));
                logger.DebugFormat("Media Image FilePath {0}", originalFilePath);

                string newFilePath = imageHelper.GetNewFilePath(originalFilePath, string.Format("{0}x{1}", width, height));
                string newFileName = imageHelper.GetNewFileName(originalFilePath, string.Format("{0}x{1}", width, height));

                logger.DebugFormat("Cropping Image");
                imageHelper.CreateCroppedImage(originalFilePath, newFilePath, width, height);
                logger.DebugFormat("Successfully Cropped Image");

                logger.DebugFormat("Creating Media Item {0}", newFileName);
                var croppedMediaItem = modelService.CreateMediaItem(newFileName, newFilePath);
                logger.DebugFormat("Successfully Created Media Item {0}", newFileName);

                mediaItemUrlCollection.Add(croppedMediaItem.GetUdi().ToString());
            }

            var imageList = string.Join(",", mediaItemUrlCollection);

            return imageList;
        }

        /// <summary>
        /// Downloads Image and Saves the Image to Media Library in Umbraco.
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="imageUrl"></param>
        /// <param name="altText"></param>
        /// <returns></returns>
        private IMedia CreateMedia(string Name, string imageUrl, string altText)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                   | SecurityProtocolType.Tls11
                   | SecurityProtocolType.Tls12
                   | SecurityProtocolType.Ssl3;

            IMedia mediaImage = null;
            var mediaService = ApplicationContext.Current.Services.MediaService;

            var request = WebRequest.Create(imageUrl);
            var webResponse = request.GetResponse();
            var responseStream = webResponse.GetResponseStream();

            string fileName = Path.GetFileName(imageUrl);
            var path = HttpContext.Current.Server.MapPath(string.Format("~/temp/{0}", fileName));

            if (responseStream != null)
            {
                var originalImage = new Bitmap(responseStream);

                if (!System.IO.File.Exists(path)) originalImage.Save(path);

                FileStream fileStream = new FileStream(path, FileMode.Open);

                mediaImage = mediaService.CreateMedia(Name, int.Parse(ConfigurationManager.AppSettings["ExteriorMediaFolderId"]), "Image");
                mediaImage.SetValue("umbracoFile", Name, fileStream);
                mediaImage.SetValue("umbracoAltText", altText);
                mediaService.Save(mediaImage);

                responseStream.Dispose();
                webResponse.Dispose();
                originalImage.Dispose();
                fileStream.Dispose();
            }

            System.IO.File.Delete(path);

            return mediaImage;
        }

        #endregion

        #region DB

        /// <summary>
        /// Triggering events once the application has started.
        /// </summary>
        /// <param name="umbracoApplication"></param>
        /// <param name="applicationContext"></param>
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            //Get the Umbraco Database context
            var ctx = applicationContext.DatabaseContext;
            var db = new DatabaseSchemaHelper(ctx.Database, applicationContext.ProfilingLogger.Logger, ctx.SqlSyntax);

            if (!db.TableExist(TABLE_NOTIFICATION))
            {
                db.CreateTable<NotificationInfo>(false);
            }

            if (!db.TableExist(TABLE_BOOKING))
            {
                db.CreateTable<BookingInfo>(false);
            }

            if (!db.TableExist(TABLE_BOOKINGNOTIFICATION))
            {
                db.CreateTable<BookingNotificationInfo>(false);
            }

            if (!db.TableExist(TABLE_TRADEVEHICLE))
            {
                db.CreateTable<TradeVehicleInfo>(false);
            }

            if (!db.TableExist(TABLE_JOB))
            {
                db.CreateTable<JobInfo>(false);
            }

            if (!db.TableExist(TABLE_IDSDATA))
            {
                db.CreateTable<IDSDataInfo>(false);
            }

            if (!db.TableExist(TABLE_IDSCODE))
            {
                db.CreateTable<IDSCodeInfo>(false);
            }

            if (!db.TableExist(TABLE_PAINTCODE))
            {
                db.CreateTable<PaintCodeInfo>(false);
            }

            if (!db.TableExist(TABLE_CART))
            {
                db.CreateTable<CartInfo>(false);
            }

            //Setting Notification Data
            //var umbracoDatabase = ApplicationContext.Current.DatabaseContext.Database;
            //umbracoDatabase.Execute("DELETE FROM dbo.audiNotification");

            //Web Notifications
            AddNotification(new NotificationInfo() { NotificationCode = "BookingConfirmation", Name = "Booking Confirmation", Source = "Service", Description = "Booking Confirmation", Type = "Web Request", ExternalId = 9129, Status = 1, CreateDate = DateTime.Now, CreateSource = "Seed" });
            AddNotification(new NotificationInfo() { NotificationCode = "SaveOptions", Name = "Save Options", Source = "Service", Description = "Save Options", Type = "Web Request", ExternalId = 9128, Status = 1, CreateDate = DateTime.Now, CreateSource = "Seed" });

            //Backend Notifications
            AddNotification(new NotificationInfo() { NotificationCode = "Ordered", Name = "Ordered", Source = "Service", Description = "Ordered", Type = "Web Request", ExternalId = 9129, Status = 1, CreateDate = DateTime.Now, CreateSource = "Seed" });
            AddNotification(new NotificationInfo() { NotificationCode = "InProduction", Name = "In Production", Source = "Service", Description = "In Production", Type = "Web Request", ExternalId = 9206, Status = 1, CreateDate = DateTime.Now, CreateSource = "Seed" });
            AddNotification(new NotificationInfo() { NotificationCode = "OnWater", Name = "On Water", Source = "Service", Description = "On Water", Type = "Web Request", ExternalId = 9207, Status = 1, CreateDate = DateTime.Now, CreateSource = "Seed" });
            AddNotification(new NotificationInfo() { NotificationCode = "ArrivedInOZ", Name = "Arrived in OZ", Source = "Service", Description = "Arrived in OZ", Type = "Web Request", ExternalId = 9208, Status = 1, CreateDate = DateTime.Now, CreateSource = "Seed" });
            AddNotification(new NotificationInfo() { NotificationCode = "CompoundDispatched", Name = "Compound Dispatched", Source = "Service", Description = "Compound Dispatched", Type = "Web Request", ExternalId = 9216, Status = 1, CreateDate = DateTime.Now, CreateSource = "Seed" });
            AddNotification(new NotificationInfo() { NotificationCode = "RDAed", Name = "RDA'd", Source = "Service", Description = "RDA'd", Type = "Web Request", ExternalId = 9209, Status = 1, CreateDate = DateTime.Now, CreateSource = "Seed" });

            //Add IDS Codes
            AddIDSCodes(new IDSCodeInfo() { Code = "A", Description = "Ordered", Name = "Ordered" });
            AddIDSCodes(new IDSCodeInfo() { Code = "B", Description = "In Production", Name = "InProduction" });
            AddIDSCodes(new IDSCodeInfo() { Code = "D", Description = "On Water", Name = "OnWater" });
            AddIDSCodes(new IDSCodeInfo() { Code = "1", Description = "Arrived in OZ", Name = "ArrivedInOZ" });
            AddIDSCodes(new IDSCodeInfo() { Code = "S", Description = "Compound Dispatched", Name = "CompoundDispatched" });
            AddIDSCodes(new IDSCodeInfo() { Code = "8", Description = "RDA'd", Name = "RDAed" });

            //Add PaintCodes
            AddPaintCodes(new PaintCodeInfo() { Code = "A2A2", Description = "Brilliant Black" });
            AddPaintCodes(new PaintCodeInfo() { Code = "H1H1", Description = "Manhattan Gray Metallic" });
            AddPaintCodes(new PaintCodeInfo() { Code = "L5L5", Description = "Floret Silver Metallic" });
            AddPaintCodes(new PaintCodeInfo() { Code = "S1S1", Description = "Galaxy Blue Metallic" });
            AddPaintCodes(new PaintCodeInfo() { Code = "0E0E", Description = "Mythos Black Metallic" });
            AddPaintCodes(new PaintCodeInfo() { Code = "2D2D", Description = "Navarra Blue Metallic" });
            AddPaintCodes(new PaintCodeInfo() { Code = "2L2L", Description = "Typhoon Gray Metallic" });
            AddPaintCodes(new PaintCodeInfo() { Code = "2Y2Y", Description = "Glacier White Metallic" });
            AddPaintCodes(new PaintCodeInfo() { Code = "3P3P", Description = "Antigua Blue Metallic" });
            AddPaintCodes(new PaintCodeInfo() { Code = "6Y6Y", Description = "Daytona Gray Pearlescent" });
            AddPaintCodes(new PaintCodeInfo() { Code = "7M7M", Description = "Siam Beige Metallic" });
            AddPaintCodes(new PaintCodeInfo() { Code = "Y6Y6", Description = "Catalunya Red Metallic" });
        }

        /// <summary>
        /// Add Or Update Notifications
        /// </summary>
        /// <param name="notificationInfo"></param>
        private void AddNotification(NotificationInfo notificationInfo)
        {
            var umbracoDatabase = ApplicationContext.Current.DatabaseContext.Database;
            var notification = umbracoDatabase.Query<NotificationInfo>("Select * from audiNotification").Where(a => a.NotificationCode == notificationInfo.NotificationCode).FirstOrDefault();
            if(notification == null)
            {
                umbracoDatabase.Save(notificationInfo);
            }
            else
            {
                notificationInfo.NotificationId = notification.NotificationId;
                umbracoDatabase.Update(notificationInfo, notification.NotificationId);
            }
        }

        /// <summary>
        /// Add Or Update IDS Codes
        /// </summary>
        /// <param name="iDSCodeInfo"></param>
        private void AddIDSCodes(IDSCodeInfo iDSCodeInfo)
        {
            var umbracoDatabase = ApplicationContext.Current.DatabaseContext.Database;
            var idscode = umbracoDatabase.Query<IDSCodeInfo>("Select * from audiIDSCode").Where(a => a.Code == iDSCodeInfo.Code).FirstOrDefault();
            if (idscode == null)
            {
                umbracoDatabase.Save(iDSCodeInfo);
            }
            else
            {
                iDSCodeInfo.Id = idscode.Id;
                umbracoDatabase.Update(iDSCodeInfo, idscode.Id);
            }
        }

        /// <summary>
        /// Add Or Update Paint Codes
        /// </summary>
        /// <param name="paintCodeInfo"></param>
        private void AddPaintCodes(PaintCodeInfo paintCodeInfo)
        {
            var umbracoDatabase = ApplicationContext.Current.DatabaseContext.Database;
            var paintCode = umbracoDatabase.Query<PaintCodeInfo>("Select * from audiPaintCode").Where(a => a.Code == paintCodeInfo.Code).FirstOrDefault();
            if (paintCode == null)
            {
                umbracoDatabase.Save(paintCodeInfo);
            }
            else
            {
                paintCodeInfo.Id = paintCode.Id;
                umbracoDatabase.Update(paintCodeInfo, paintCode.Id);
            }
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public class ExteriorImage
    {
        /// <summary>
        /// 
        /// </summary>
        public string key { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ncContentTypeAlias { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string code { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string thumbnail { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string image { get; set; }
    }
}