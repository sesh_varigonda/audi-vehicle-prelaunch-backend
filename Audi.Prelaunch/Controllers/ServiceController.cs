﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using Umbraco.Web.WebApi;
using Swashbuckle.Swagger.Annotations;
using log4net;
using System.Reflection;
using System.Web.Http.Cors;
using Audi.Prelaunch.Models;
using System.Threading.Tasks;
using System.Net.Http;
using System.Configuration;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Net.Http.Formatting;
using Newtonsoft.Json.Serialization;
using Umbraco.Web.Mvc;
using Audi.Prelaunch.Cms.Service;
using Audi.Prelaunch.Business.Models;
using Audi.Prelaunch.Business.Service;

namespace Audi.Prelaunch.Cms.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [PluginController("services")]
    [RoutePrefix("services")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PluginServiceController : UmbracoApiController
    {
        ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private static readonly IContentNegotiator _conneg = GlobalConfiguration.Configuration.Services.GetContentNegotiator();

        private static readonly IEnumerable<JsonMediaTypeFormatter> _mediaTypeFormatters = new[] {
        new JsonMediaTypeFormatter {
            SerializerSettings = new JsonSerializerSettings {
                ContractResolver = new DefaultContractResolver()
            }
        }
    };

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("dealers")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(DealerGetInfo), Description = "success")]
        public async Task<IHttpActionResult> GetDealers()
        {
            var result = string.Empty;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["ApiCMSInternalBaseUrl"]);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = client.GetAsync(string.Format("/2/legacy/dealers/model/{0}", "Prelaunch")).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsStringAsync().Result;
                    }
                }

                var responseMessage = JsonConvert.DeserializeObject<ResponseMessage>(result);
                return Ok(responseMessage.data);
            }
            catch (Exception e)
            {
                logger.Error(e);
                return InternalServerError(e);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [Route("packages")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(PackageInfo), Description = "success")]
        public async Task<IHttpActionResult> GetPackages()
        {
            List<Prelaunch.Models.PackageGetInfo> packageGetInfos = new List<Prelaunch.Models.PackageGetInfo>();
            ModelService modelService = new ModelService();

            try
            {
                var packages = modelService.GetList().packages;

                foreach(var package in packages)
                {
                    var packageInfo = new Prelaunch.Models.PackageGetInfo() { Id = package.Id, Name = package.Name };
                    packageGetInfos.Add(packageInfo);
                }

                return Ok(packageGetInfos);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Unable to get home page content, ERROR MESSAGE : {0}", ex.Message);
                return InternalServerError(ex);
            }
        }
    }

    
}


