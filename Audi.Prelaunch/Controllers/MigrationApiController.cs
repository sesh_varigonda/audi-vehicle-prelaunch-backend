﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using Umbraco.Web.WebApi;
using Swashbuckle.Swagger.Annotations;
using System.Configuration;
using Umbraco.Core.Models;
using Umbraco.Core.Persistence;
using umbraco.cms.businesslogic.web;
using Umbraco.Web;
using Umbraco.Core;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Newtonsoft.Json.Serialization;
using System.Net.Http;
using System.Net.Http.Headers;
using AutoMapper;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Drawing;
using System.Web;
using System.IO;
using CsvHelper;
using System.Dynamic;
using System.Text.RegularExpressions;
using Umbraco.Core.Services;
using umbraco;
using log4net;
using System.Reflection;
using Audi.Magazine.Model;
using Audi.Magazine.Cms.Business.Helpers;

namespace Audi.Magazine.Cms.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("1/cms")]
    public class MigrationController : UmbracoApiController
    {
        ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        readonly string _baseUrl = ConfigurationManager.AppSettings["MagazineBaseUrl"];
        readonly string _rootImageFolderId = ConfigurationManager.AppSettings["RootImageFolderId"];

        readonly string _rootArticlesFolderId = ConfigurationManager.AppSettings["RootArticlesFolderId"];
        readonly string _rootVideosFolderId = ConfigurationManager.AppSettings["RootVideosFolderId"];
        readonly string _rootAmbassadorsFolderId = ConfigurationManager.AppSettings["RootAmbassadorsFolderId"];
        readonly string _rootGalleriesFolderId = ConfigurationManager.AppSettings["RootGalleriesFolderId"];
        readonly string _rootPartnersFolderId = ConfigurationManager.AppSettings["RootPartnersFolderId"];
        readonly string _rootQuotesFolderId = ConfigurationManager.AppSettings["RootQuotesFolderId"];
        readonly string _rootCategoriesFolderId = ConfigurationManager.AppSettings["RootCategoriesFolderId"];

        readonly string _rootMediaArticlesFolderId = ConfigurationManager.AppSettings["RootMediaArticlesFolderId"];
        readonly string _rootMediaVideosFolderId = ConfigurationManager.AppSettings["RootMediaVideosFolderId"];
        readonly string _rootMediaAmbassadorsFolderId = ConfigurationManager.AppSettings["RootMediaAmbassadorsFolderId"];
        readonly string _rootMediaGalleriesFolderId = ConfigurationManager.AppSettings["RootMediaGalleriesFolderId"];
        readonly string _rootMediaPartnersFolderId = ConfigurationManager.AppSettings["RootMediaPartnersFolderId"];
        readonly string _rootMediaCategoriesFolderId = ConfigurationManager.AppSettings["RootMediaCategoriesFolderId"];


        string mediaPrefix = "http://audimagazine.com.au/sites/default/files/{0}";
        string imagePrefix = "http://audimagazine.com.au/sites/default/files/styles/hero_image/public/{0}";
        string tileImagePrefix = "http://audimagazine.com.au/sites/default/files/{0}";
        string heroImagePrefix = "http://audimagazine.com.au/sites/default/files/styles/article800/public/{0}";

        private const string CONTROLTYPE_HTMLCONTENT = "HtmlContent";
        private const string CONTROLTYPE_MEDIACONTENT = "MediaContent";
        private const string CONTROLTYPE_MEDIATYPE = "MediaType";
        private const string CONTROLTYPE_TEXTSTRING = "TextString";
        private const string CONTROLTYPE_MEDIAS = "Medias";

        private const string DEFAULT_URI_STRING = "public://";

        private const int MAX_QUOTE_TEXT_LENGTH = 500;

        #region Controllers

        /// <summary>
        /// returns content information by id.
        /// </summary>
        /// <param name="id">id of the content</param>
        /// <param name="ispublished">verify if the content is published.</param>
        /// <returns></returns>
        [Route("getContent")]
        public IHttpActionResult GetContentById(int id, bool ispublished = true)
        {
            ContentInfo contentInfo = new ContentInfo();

            try
            {
                var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
                var contentItem = umbracoHelper.TypedContent(id);
                contentInfo = GetContent(contentItem);

                return Ok(contentInfo);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }


        /// <summary>
        /// This API has been created for test purpose
        /// </summary>
        /// <param name="limit">number of articles which will be copied from legacy system and saved onto the new CMS system</param>
        /// <param name="offset"></param>
        /// <param name="articletype">article type</param>
        /// <param name="ispublished"></param>
        /// <returns></returns>
        [Route("saveArticleContent")]
        public IHttpActionResult SaveArticleContent(int limit, int offset, string articletype = "", bool ispublished = true)
        {
            ContentInfo contentInfo = new ContentInfo();

            try
            {
                var categories = OldCMSApiFunctions.GetCategoryList();
                var tags = OldCMSApiFunctions.GetTagList();
                var feeds = OldCMSApiFunctions.GetFeeds(limit, offset);
                var sectors = OldCMSApiFunctions.GetSectors();

                List<dynamic> categoryCollection = new List<dynamic>();
                var myAudiMagazineCategoryList = ParseCSVFile(ConfigurationManager.AppSettings["myAudiMagazineCategoryList"]);
                foreach (var article in myAudiMagazineCategoryList)
                {
                    if(article.Id != null && !string.IsNullOrEmpty(article.Id))
                    {
                        dynamic data = new ExpandoObject();

                        data.Categories = article.CategoriesNEW.ToString();
                        data.Id = article.Id.ToString();
                        data.Type = article.Type.ToString();
                        data.Slug = article.Slug.ToString();

                        categoryCollection.Add(data);
                    }
                }

                var feedList = feeds.Where(a => ContentTypes().Where(b => b == a.type).Any());

                var categoryList = feeds.Select(a => a.type).Distinct().ToList();
                if (!string.IsNullOrEmpty(articletype))
                {
                    feedList = feedList.Where(a => a.type == articletype).ToList();
                }

                foreach (var feed in feedList)
                {
                    try
                    {
                        logger.InfoFormat("Processing Article : {0}, Type : {1}", feed.title, feed.type);
                        var articleInfo = GetArticleDetailsBySlug(feed.path, categories, tags, categoryCollection, null, sectors);

                        if (articleInfo != null && GetValidFeedTypes().Where(a=> a == feed.type).Any())
                        {
                            var contentService = ApplicationContext.Current.Services.ContentService;
                            var contentItem = contentService.CreateContent(feed.title, GetRootContentFolderIdByType(feed.type), feed.type, 0);

                            //Set Sections
                            if (articleInfo.Sections != null && articleInfo.Sections.Any())
                            {
                                var gridContent = CreateGridContent(articleInfo, feed.type);
                                if (!string.IsNullOrEmpty(gridContent))
                                {
                                    contentItem.SetValue("bodyText", gridContent);
                                }
                            }

                            //Set PageTitle
                            if (articleInfo.Title != null)
                            {
                                contentItem.SetValue("title", articleInfo.Title);
                            }

                            //Set Link
                            if (!string.IsNullOrEmpty(articleInfo.Link))
                            {
                                contentItem.SetValue("link", articleInfo.Link);
                            }

                            //Set SubTitle
                            if (articleInfo.SubTitle != null)
                            {
                                contentItem.SetValue("subtitle", articleInfo.SubTitle);
                            }

                            //Set VideoType
                            if (feed != null && !string.IsNullOrEmpty(feed.video_type))
                            {
                                SetDropDownValue(contentItem, "videoType", feed.video_type);
                            }

                            //Set SectorType
                            if (feed != null && !string.IsNullOrEmpty(feed.sectors))
                            {
                                SetDropDownValue(contentItem, "sectors", feed.sectors);
                            }

                            //Set Video
                            if (feed != null && !string.IsNullOrEmpty(feed.video))
                            {
                                contentItem.SetValue("video", feed.video);
                            }

                            //Set Youtube
                            if (feed != null && !string.IsNullOrEmpty(feed.youtube))
                            {
                                contentItem.SetValue("youtube", feed.youtube);
                            }

                            //Set EmbedCode
                            if (feed != null && feed.embed_code != null)
                            {
                                //contentItem.SetValue("title", feed.embed_code);
                            }

                            //Set Published Date
                            if (feed != null && feed.post_date.HasValue)
                            {
                                contentItem.SetValue("publishDate", feed.post_date.Value);
                            }

                            //Set Updated Date
                            if (feed != null && feed.updated_date != null)
                            {
                                contentItem.UpdateDate = feed.updated_date;
                            }

                            //Set MetaTags
                            if (articleInfo.MetaTags != null)
                            {
                                contentItem.SetValue("seoMetaDescription", articleInfo.MetaTags.Description);
                            }

                            //Set Image
                            if (articleInfo.Image != null)
                            {
                                string imageUrl = string.Format(tileImagePrefix, Uri.EscapeUriString(articleInfo.Image["uri"].Replace(DEFAULT_URI_STRING, string.Empty)));
                                var mediaItem = CreateMedia(articleInfo.Image["filename"], imageUrl, articleInfo.Image["alt"], articleInfo.Title, feed.type, articleInfo.Image["type"]);

                                if(mediaItem != null)
                                {
                                    contentItem.SetValue("image", mediaItem.GetUdi().ToString());
                                }
                            }

                            //Set Logo
                            if (articleInfo.Logo != null)
                            {
                                string imageUrl = string.Format(tileImagePrefix, Uri.EscapeUriString(articleInfo.Logo["uri"].Replace(DEFAULT_URI_STRING, string.Empty)));
                                var mediaItem = CreateMedia(articleInfo.Logo["filename"], imageUrl, articleInfo.Logo["alt"], articleInfo.Title, feed.type, articleInfo.Logo["type"]);

                                if (mediaItem != null)
                                {
                                    contentItem.SetValue("logo", mediaItem.GetUdi().ToString());
                                }
                            }

                            //Set TileImage
                            if (articleInfo.TileImage != null)
                            {
                                string imageUrl = string.Format(tileImagePrefix, Uri.EscapeUriString(articleInfo.TileImage["uri"].Replace(DEFAULT_URI_STRING, string.Empty)));
                                var mediaItem = CreateMedia(articleInfo.TileImage["filename"], imageUrl, articleInfo.TileImage["alt"], articleInfo.Title, feed.type, articleInfo.TileImage["type"]);

                                if (mediaItem != null)
                                {
                                    contentItem.SetValue("tileImage", mediaItem.GetUdi().ToString());
                                }
                            }

                            //Set HeroImage
                            if (articleInfo.HeroMedia != null)
                            {
                                string imageUrl = string.Format(heroImagePrefix, Uri.EscapeUriString(articleInfo.HeroMedia["uri"].Replace(DEFAULT_URI_STRING, string.Empty)));
                                var mediaItem = CreateMedia(articleInfo.HeroMedia["filename"], imageUrl, articleInfo.HeroMedia["alt"], articleInfo.Title, feed.type, articleInfo.HeroMedia["type"]);

                                if (mediaItem != null)
                                {
                                    contentItem.SetValue("heroMedia", mediaItem.GetUdi().ToString());
                                }
                            }

                            //Set Categories
                            if (articleInfo.Categories != null)
                            {
                                if(articleInfo.Categories == null || !articleInfo.Categories.Any())
                                {
                                    articleInfo.Categories = new List<string>() { "All Articles" };
                                }

                                SetMultipleDropDownValue(contentItem, "categories", articleInfo.Categories);
                            }

                            //Set Tags
                            if (articleInfo.Tags != null)
                            {
                                contentItem.SetValue("tags", GetDBTags(articleInfo.Tags));
                            }

                            if (articleInfo.Tags != null)
                            {
                                contentItem.SetValue("keywords", GetDBTags(articleInfo.Tags));
                            }

                            if(!string.IsNullOrEmpty(articleInfo.Sectors))
                            {
                                SetDropDownValue(contentItem, "sectors", articleInfo.Sectors);
                            }

                            //logger.InfoFormat("Saving Article : {0}", feed.title);
                            contentService.SaveAndPublishWithStatus(contentItem);
                            logger.InfoFormat("Saved Article : {0}", feed.title);
                        }
                    }
                    catch(Exception e)
                    {
                        logger.ErrorFormat("Unable to process Article : {0}", feed.title);
                        logger.Error(e);
                    }
                }

                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// This API has been created for test purpose, saves primaryly partners and ambassadors data to the DB.
        /// </summary>
        /// <param name="category">category</param>
        /// <param name="ispublished"></param>
        /// <returns></returns>
        [Route("saveOtherContent")]
        public IHttpActionResult SaveOtherContent(string category, bool ispublished = true)
        {
            ContentInfo contentInfo = new ContentInfo();

            try
            {
                var categories = OldCMSApiFunctions.GetCategoryList();
                var tags = OldCMSApiFunctions.GetTagList();
                var sectors = OldCMSApiFunctions.GetSectors();

                List<MigrationArticleInfo> feedList = new List<MigrationArticleInfo>(); 
                if(category == "partners")
                {
                    feedList = OldCMSApiFunctions.GetPartners();
                }
                if(category == "ambassadors")
                {
                    feedList = OldCMSApiFunctions.GetAmbassadors();
                }

                List<dynamic> categoryCollection = new List<dynamic>();
                var myAudiMagazineCategoryList = ParseCSVFile(ConfigurationManager.AppSettings["myAudiMagazineCategoryList"]);
                foreach (var article in myAudiMagazineCategoryList)
                {
                    if (article.Id != null && !string.IsNullOrEmpty(article.Id))
                    {
                        dynamic data = new ExpandoObject();

                        data.Categories = article.CategoriesNEW.ToString();
                        data.Id = article.Id.ToString();
                        data.Type = article.Type.ToString();
                        data.Slug = article.Slug.ToString();

                        categoryCollection.Add(data);
                    }
                }

                foreach (var feed in feedList)
                {
                    try
                    {
                        logger.InfoFormat("Processing Article : {0}, Type : {1}", feed.title, feed.type);
                        var articleInfo = GetArticleDetailsBySlug(string.Empty, categories, tags, categoryCollection, feed, sectors);

                        if (articleInfo != null && GetValidFeedTypes().Where(a => a == feed.type).Any())
                        {
                            var contentService = ApplicationContext.Current.Services.ContentService;
                            var contentItem = contentService.CreateContent(feed.title, GetRootContentFolderIdByType(feed.type), feed.type, 0);

                            //Set Sections
                            if (articleInfo.Sections != null && articleInfo.Sections.Any())
                            {
                                var gridContent = CreateGridContent(articleInfo, feed.type);
                                if (!string.IsNullOrEmpty(gridContent))
                                {
                                    contentItem.SetValue("bodyText", gridContent);
                                }
                            }

                            //Set PageTitle
                            if (articleInfo.Title != null)
                            {
                                contentItem.SetValue("title", articleInfo.Title);
                            }

                            //Set Link
                            if (!string.IsNullOrEmpty(articleInfo.Link))
                            {
                                contentItem.SetValue("link", articleInfo.Link);
                            }

                            //Set SubTitle
                            if (articleInfo.SubTitle != null)
                            {
                                contentItem.SetValue("subtitle", articleInfo.SubTitle);
                            }

                            //Set MetaTags
                            if (articleInfo.MetaTags != null)
                            {
                                contentItem.SetValue("seoMetaDescription", articleInfo.MetaTags.Description);
                            }

                            //Set Image
                            if (articleInfo.Image != null)
                            {
                                string imageUrl = string.Format(tileImagePrefix, Uri.EscapeUriString(articleInfo.Image["uri"].Replace(DEFAULT_URI_STRING, string.Empty)));
                                var mediaItem = CreateMedia(articleInfo.Image["filename"], imageUrl, articleInfo.Image["alt"], articleInfo.Title, feed.type, articleInfo.Image["type"]);

                                if (mediaItem != null)
                                {
                                    contentItem.SetValue("image", mediaItem.GetUdi().ToString());
                                }
                            }

                            //Set Logo
                            if (articleInfo.Logo != null)
                            {
                                string imageUrl = string.Format(tileImagePrefix, Uri.EscapeUriString(articleInfo.Logo["uri"].Replace(DEFAULT_URI_STRING, string.Empty)));
                                var mediaItem = CreateMedia(articleInfo.Logo["filename"], imageUrl, articleInfo.Logo["alt"], articleInfo.Title, feed.type, articleInfo.Logo["type"]);

                                if (mediaItem != null)
                                {
                                    contentItem.SetValue("logo", mediaItem.GetUdi().ToString());
                                }
                            }

                            //Set TileImage
                            if (articleInfo.TileImage != null)
                            {
                                string imageUrl = string.Format(tileImagePrefix, Uri.EscapeUriString(articleInfo.TileImage["uri"].Replace(DEFAULT_URI_STRING, string.Empty)));
                                var mediaItem = CreateMedia(articleInfo.TileImage["filename"], imageUrl, articleInfo.TileImage["alt"], articleInfo.Title, feed.type, articleInfo.TileImage["type"]);

                                if (mediaItem != null)
                                {
                                    contentItem.SetValue("tileImage", mediaItem.GetUdi().ToString());
                                }
                            }

                            //Set HeroImage
                            if (articleInfo.HeroMedia != null)
                            {
                                string imageUrl = string.Format(heroImagePrefix, Uri.EscapeUriString(articleInfo.HeroMedia["uri"].Replace(DEFAULT_URI_STRING, string.Empty)));
                                var mediaItem = CreateMedia(articleInfo.HeroMedia["filename"], imageUrl, articleInfo.HeroMedia["alt"], articleInfo.Title, feed.type, articleInfo.HeroMedia["type"]);

                                if (mediaItem != null)
                                {
                                    contentItem.SetValue("heroMedia", mediaItem.GetUdi().ToString());
                                }
                            }

                            //Set Categories
                            if (articleInfo.Categories != null)
                            {
                                if (articleInfo.Categories == null || !articleInfo.Categories.Any())
                                {
                                    articleInfo.Categories = new List<string>() { "All Articles" };
                                }

                                SetMultipleDropDownValue(contentItem, "categories", articleInfo.Categories);
                            }

                            //Set Tags
                            if (articleInfo.Tags != null)
                            {
                                contentItem.SetValue("tags", GetDBTags(articleInfo.Tags));
                            }

                            if (articleInfo.Tags != null)
                            {
                                contentItem.SetValue("keywords", GetDBTags(articleInfo.Tags));
                            }

                            if (!string.IsNullOrEmpty(articleInfo.Sectors))
                            {
                                SetDropDownValue(contentItem, "sectors", articleInfo.Sectors);
                            }

                            //logger.InfoFormat("Saving {1} : {0}", feed.title, category);
                            contentService.SaveAndPublishWithStatus(contentItem);
                            logger.InfoFormat("Saved {1} : {0}", feed.title, category);
                        }
                    }
                    catch (Exception e)
                    {
                        logger.ErrorFormat("Unable to process Article : {0}", feed.title);
                        logger.Error(e);
                    }
                }

                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// List of valid feedtypes
        /// </summary>
        /// <returns></returns>
        private List<string> GetValidFeedTypes()
        {
            return new List<string>() { "article", "video", "ambassador", "partner", "quote", "gallery" };
        }

        /// <summary>
        /// returns content folderId by feedtype
        /// </summary>
        /// <param name="type">DocumentType</param>
        /// <returns></returns>
        private int GetRootContentFolderIdByType(string type)
        {
            if(type == "article")
            {
                return int.Parse(_rootArticlesFolderId);
            }
            else if (type == "video")
            {
                return int.Parse(_rootVideosFolderId);
            }
            else if (type == "ambassador")
            {
                return int.Parse(_rootAmbassadorsFolderId);
            }
            else if (type == "partner")
            {
                return int.Parse(_rootPartnersFolderId);
            }
            else if (type == "quote")
            {
                return int.Parse(_rootQuotesFolderId);
            }
            else if (type == "gallery")
            {
                return int.Parse(_rootGalleriesFolderId);
            }

            return 0;
        }


        /// <summary>
        /// returns media folderId by feedtype
        /// </summary>
        /// <param name="type">DocumentType</param>
        /// <returns></returns>
        private int GetRootMediaContentFolderIdByType(string type)
        {
            if (type == "article")
            {
                return int.Parse(_rootMediaArticlesFolderId);
            }
            else if (type == "video")
            {
                return int.Parse(_rootMediaVideosFolderId);
            }
            else if (type == "ambassador")
            {
                return int.Parse(_rootMediaAmbassadorsFolderId);
            }
            else if (type == "partner")
            {
                return int.Parse(_rootMediaPartnersFolderId);
            }
            else if (type == "category")
            {
                return int.Parse(_rootMediaCategoriesFolderId);
            }
            else if (type == "gallery")
            {
                return int.Parse(_rootMediaGalleriesFolderId);
            }

            return 0;
        }


        /// <summary>
        /// This API has been created for Migration/Testing purpose
        /// </summary>
        /// <remarks>
        /// The purpose of this API is to download content by slug from the legacy system and save it the new CMS.
        /// </remarks>
        /// <param name="slugName">slugname of the content from the legacy system.</param>
        /// <param name="ispublished"></param>
        /// <returns></returns>
        [Route("saveArticleContentBySlug")]
        private IHttpActionResult SaveArticleContentBySlug(string slugName, bool ispublished = true)
        {
            ContentInfo contentInfo = new ContentInfo();

            try
            {
                var categories = OldCMSApiFunctions.GetCategoryList();
                var tags = OldCMSApiFunctions.GetTagList();
                var feeds = OldCMSApiFunctions.GetFeeds(10000);
                var sectors = OldCMSApiFunctions.GetSectors();

                var feed = feeds.Where(a => a.path == slugName).FirstOrDefault();

                List<dynamic> categoryCollection = new List<dynamic>();
                var myAudiMagazineCategoryList = ParseCSVFile(ConfigurationManager.AppSettings["myAudiMagazineCategoryList"]);
                foreach (var article in myAudiMagazineCategoryList)
                {
                    if (article.Id != null && !string.IsNullOrEmpty(article.Id))
                    {
                        dynamic data = new ExpandoObject();

                        data.Categories = article.CategoriesNEW.ToString();
                        data.Id = article.Id.ToString();
                        data.Type = article.Type.ToString();
                        data.Slug = article.Slug.ToString();

                        categoryCollection.Add(data);
                    }
                }

                var articleInfo = GetArticleDetailsBySlug(slugName, categories, tags, categoryCollection, null, sectors);

                try
                {
                    logger.InfoFormat("Processing Article : {0}, Type : {1}", articleInfo.Title, articleInfo.Type);
                    if (articleInfo != null)
                    {
                        var contentService = ApplicationContext.Current.Services.ContentService;
                        var contentItem = contentService.CreateContent(articleInfo.Title, GetRootContentFolderIdByType(feed.type), articleInfo.Type, 0);

                        //Set Sections
                        if (articleInfo.Sections != null && articleInfo.Sections.Any())
                        {
                            var gridContent = CreateGridContent(articleInfo, feed.type);
                            if (!string.IsNullOrEmpty(gridContent))
                            {
                                contentItem.SetValue("bodyText", gridContent);
                            }
                        }

                        //Set PageTitle
                        if (articleInfo.Title != null)
                        {
                            contentItem.SetValue("title", articleInfo.Title);
                        }

                        //Set Link
                        if (!string.IsNullOrEmpty(articleInfo.Link))
                        {
                            contentItem.SetValue("link", articleInfo.Link);
                        }

                        //Set SubTitle
                        if (articleInfo.SubTitle != null)
                        {
                            contentItem.SetValue("subtitle", articleInfo.SubTitle);
                        }

                        //Set VideoType
                        if (feed != null && !string.IsNullOrEmpty(feed.video_type))
                        {
                            SetDropDownValue(contentItem, "videoType", feed.video_type);
                        }

                        //Set SectorType
                        if (feed != null && !string.IsNullOrEmpty(feed.sectors))
                        {
                            SetDropDownValue(contentItem, "sectors", feed.sectors);
                        }

                        //Set Video
                        if (feed != null && !string.IsNullOrEmpty(feed.video))
                        {
                            contentItem.SetValue("video", feed.video);
                        }

                        //Set Youtube
                        if (feed != null && !string.IsNullOrEmpty(feed.youtube))
                        {
                            contentItem.SetValue("youtube", feed.youtube);
                        }

                        //Set EmbedCode
                        if (feed != null && feed.embed_code != null)
                        {
                            //contentItem.SetValue("title", feed.embed_code);
                        }

                        //Set MetaTags
                        if (articleInfo.MetaTags != null)
                        {
                            contentItem.SetValue("seoMetaDescription", articleInfo.MetaTags.Description);
                        }

                        //Set Image
                        if (articleInfo.Image != null)
                        {
                            string imageUrl = string.Format(tileImagePrefix, Uri.EscapeUriString(articleInfo.Image["uri"].Replace(DEFAULT_URI_STRING, string.Empty)));
                            var mediaItem = CreateMedia(articleInfo.Image["filename"], imageUrl, articleInfo.Image["alt"], articleInfo.Title, feed.type, articleInfo.Image["type"]);

                            if (mediaItem != null)
                            {
                                contentItem.SetValue("image", mediaItem.GetUdi().ToString());
                            }
                        }

                        //Set Logo
                        if (articleInfo.Logo != null)
                        {
                            string imageUrl = string.Format(tileImagePrefix, Uri.EscapeUriString(articleInfo.Logo["uri"].Replace(DEFAULT_URI_STRING, string.Empty)));
                            var mediaItem = CreateMedia(articleInfo.Logo["filename"], imageUrl, articleInfo.Logo["alt"], articleInfo.Title, feed.type, articleInfo.Logo["type"]);

                            if (mediaItem != null)
                            {
                                contentItem.SetValue("logo", mediaItem.GetUdi().ToString());
                            }
                        }

                        //Set TileImage
                        if (articleInfo.TileImage != null)
                        {
                            string imageUrl = string.Format(tileImagePrefix, Uri.EscapeUriString(articleInfo.TileImage["uri"].Replace(DEFAULT_URI_STRING, string.Empty)));
                            var mediaItem = CreateMedia(articleInfo.TileImage["filename"], imageUrl, articleInfo.TileImage["alt"], articleInfo.Title, feed.type, articleInfo.TileImage["type"]);

                            if (mediaItem != null)
                            {
                                contentItem.SetValue("tileImage", mediaItem.GetUdi().ToString());
                            }
                        }

                        //Set HeroImage
                        if (articleInfo.HeroMedia != null)
                        {
                            string imageUrl = string.Format(heroImagePrefix, Uri.EscapeUriString(articleInfo.HeroMedia["uri"].Replace(DEFAULT_URI_STRING, string.Empty)));
                            var mediaItem = CreateMedia(articleInfo.HeroMedia["filename"], imageUrl, articleInfo.HeroMedia["alt"], articleInfo.Title, feed.type, articleInfo.HeroMedia["type"]);

                            if (mediaItem != null)
                            {
                                contentItem.SetValue("heroMedia", mediaItem.GetUdi().ToString());
                            }
                        }

                        //Set Categories
                        if (articleInfo.Categories != null)
                        {
                            if (articleInfo.Categories == null || !articleInfo.Categories.Any())
                            {
                                articleInfo.Categories = new List<string>() { "All Articles" };
                            }

                            SetMultipleDropDownValue(contentItem, "categories", articleInfo.Categories);
                        }

                        //Set Tags
                        if (articleInfo.Tags != null)
                        {
                            contentItem.SetValue("tags", GetDBTags(articleInfo.Tags));
                        }

                        //logger.InfoFormat("Saving Article : {0}", articleInfo.Title);
                        contentService.SaveAndPublishWithStatus(contentItem);
                        logger.InfoFormat("Saved Article : {0}", articleInfo.Title);
                    }
                }
                catch (Exception e)
                {
                    logger.ErrorFormat("Unable to process Article : {0}", articleInfo.Title);
                    logger.Error(e);
                }

                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        
        /// <summary>
        /// returns list of legacy categories.
        /// </summary>
        /// <returns></returns>
        [Route("categories")]
        public IHttpActionResult GetCategories()
        {
            try
            {
                return Ok(OldCMSApiFunctions.GetCategoryList());
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// returns list of legacy tags.
        /// </summary>
        /// <returns></returns>
        [Route("tags")]
        public IHttpActionResult GetTags()
        {
            try
            {
                return Ok(OldCMSApiFunctions.GetTagList());
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// saves all the categories from the old cms into new cms.
        /// </summary>
        /// <returns></returns>
        [Route("saveCategories")]
        public void AddCategories()
        {
            var result = string.Empty;

            try
            {
                var categories = OldCMSApiFunctions.GetLegacyCategories();

                var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);
                foreach (var category in categories)
                {
                    var contentService = ApplicationContext.Current.Services.ContentService;
                    var contentItem = contentService.CreateContent(category.name, int.Parse(_rootCategoriesFolderId), "category", 0);

                    //Add Image
                    var mediaItem = CreateMedia(System.IO.Path.GetFileName(category.image), category.image, string.Empty, category.name, "category", "image");
                    if (mediaItem != null)
                    {
                        contentItem.SetValue("image", mediaItem.GetUdi().ToString());
                    }

                    contentItem.SetValue("title", category.name);
                    contentItem.SetValue("subtitle", category.term_description);

                    contentService.SaveAndPublishWithStatus(contentItem);
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// this api has been built for testing purpose, will be removed once migration is complete.
        /// </summary>
        /// <param name="slugName"></param>
        /// <returns></returns>
        [Route("postArticleContent")]
        private async Task<IHttpActionResult> PostArticleContent(string slugName)
        {
            var categories = OldCMSApiFunctions.GetCategoryList();
            var tags = OldCMSApiFunctions.GetTagList();
            var sectors = OldCMSApiFunctions.GetSectors();

            List<dynamic> categoryCollection = new List<dynamic>();
            var myAudiMagazineCategoryList = ParseCSVFile(ConfigurationManager.AppSettings["myAudiMagazineCategoryList"]);
            foreach (var article in myAudiMagazineCategoryList)
            {
                dynamic data = new ExpandoObject();

                data.Categories = article.CategoriesNEW.ToString();
                data.Id = article.Id.ToString();
                data.Type = article.Type.ToString();
                data.Slug = article.Slug.ToString();

                categoryCollection.Add(data);

                logger.Info(string.Format("Record Successfully Inserted for {0}", data.EmailAddress));
            }


            return Ok(GetArticleDetailsBySlug(slugName, categories, tags, categoryCollection, null, sectors));
        }

        #endregion

        #region Unused Controllers

        /// <summary>
        /// returns list of article feeds from the old system using api's.
        /// </summary>
        /// <returns></returns>
        [Route("clientDataFeeds")]
        private async Task<IEnumerable<FeedInfo>> GetClientDataFeeds()
        {
            HttpClient _client = new HttpClient();

            _client.BaseAddress = new Uri(_baseUrl);
            _client.DefaultRequestHeaders.Accept.Clear();

            var response = await _client.GetAsync("api/views/feed.json");

            if (!response.IsSuccessStatusCode) throw new ApplicationException();

            var result = response.Content.ReadAsStringAsync().Result;
            var feeds = JsonConvert.DeserializeObject<IEnumerable<FeedInfo>>(result, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            var categories = OldCMSApiFunctions.GetCategoryList();
            var tags = OldCMSApiFunctions.GetTagList();

            //Write the Article Info to the CSV File.
            List<dynamic> feedList = new List<dynamic>();
            foreach (var feed in feeds)
            {
                dynamic data = new ExpandoObject();

                data.Id = feed.nid;
                data.Type = feed.type;
                data.Title = feed.title.Replace(",", "-");
                data.Slug = feed.path;
                data.SubTitle = feed.body.Replace(",", "-").Replace("\r\n", string.Empty).Replace("\n", string.Empty).Replace("<p class=\"p1\"><span class=\"s1\">", string.Empty).Replace("</span></p>", string.Empty)
                    .Replace("<p><style type=\"text/css\"><!--/*--><![CDATA[/* ><!--*/p.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 11.0px Helvetica; -webkit-text-stroke: #000000}span.s1 {font-kerning: none}/*--><!]]>*/</style></p>", string.Empty)
                    .Replace("<p><style type=\"text/css\"><!--/*--><![CDATA[/* ><!--*/p.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 11.0px Helvetica; color: #000000; -webkit-text-stroke: #000000}span.s1 {font-kerning: none}/*--><!]]>*/</style></p>", string.Empty);

                data.SubTitle = Regex.Replace(data.SubTitle, "<.*?>", String.Empty);
                data.SubTitle = Regex.Replace(data.SubTitle, @"[^0-9a-zA-Z \._]", string.Empty);

                data.PostDate = feed.post_date;
                data.Categories = !string.IsNullOrEmpty(feed.categories) ? feed.categories.Replace(",", ";") : string.Empty;
                data.Tags = !string.IsNullOrEmpty(feed.tags) ? feed.tags.Replace(",", ";") : string.Empty;

                feedList.Add(data);
            }

            WriteToCSVFile(feedList, ConfigurationManager.AppSettings["csvFilePath"]);

            return feeds;
        }

        /// <summary>
        /// Get AudiMagazine Feeds By Category
        /// </summary>
        /// <param name="categories">feedback payload</param>
        /// <returns></returns>
        [Route("articles/categories/{categories}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(Article), Description = "success")]
        private async Task<IHttpActionResult> GetArticlesByCategories(string categories)
        {
            var result = string.Empty;
            HttpResponseMessage response = null;

            try
            {
                Mapper.CreateMap<FeedInfo, Article>()
                .ForMember(dist => dist.featured, opts => opts.MapFrom(src => src.featured == "yes" ? true : false))
                .ForMember(dist => dist.id, opts => opts.MapFrom(src => src.nid))
                .ForMember(dist => dist.tileImage, opts => opts.MapFrom(src => src.tile_image))
                .ForMember(dist => dist.articleImage, opts => opts.MapFrom(src => src.article_image))
                .ForMember(dist => dist.fullPicture, opts => opts.MapFrom(src => src.full_picture))
                .ForMember(dist => dist.postDate, opts => opts.MapFrom(src => src.post_date))
                .ForMember(dist => dist.createdLabel, opts => opts.MapFrom(src => src.created_label));

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["MagazineBaseUrl"]);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var queryString = this.Request.RequestUri.Query;
                    if (string.IsNullOrEmpty(queryString))
                    {
                        response = client.GetAsync(string.Format("api/views/feed.json?categories={0}", categories)).Result;
                    }
                    else
                    {
                        response = client.GetAsync(string.Format("api/views/feed.json{0}&categories={1}", queryString, categories)).Result;
                    }

                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsStringAsync().Result;
                    }
                }

                var feeds = JsonConvert.DeserializeObject<IEnumerable<FeedInfo>>(result, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() }).DistinctBy(a => a.nid).ToList();

                var articles = Mapper.Map<IEnumerable<Article>>(feeds).ToList();

                return Ok(articles.OrderByDescending(a => a.postDate));
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        #endregion

        #region private methods

        /// <summary>
        /// translating umbraco content to an object.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private ContentInfo GetContent(object data)
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            if (data.GetType() == typeof(ContentInfo))
            {
                return (ContentInfo)data;
            }
            else if (data.GetType() == typeof(Content))
            {
                var content = (IContent)data;
                var contentTypeAlias = new Document(content.Id).ContentType.Alias;

                foreach (var property in content.Properties)
                {
                    properties.Add(property.Alias, property.Value);
                }

                var contentInfo = new ContentInfo() { Id = content.Id, Alias = contentTypeAlias, Properties = properties, Name = content.Name, isPublished = content.Published, Path = content.Path };

                if (content.Children().Any())
                {
                    foreach (IContent contentData in content.Children())
                    {
                        if (contentInfo.Children == null)
                        {
                            contentInfo.Children = new List<ContentInfo>();
                        }

                        contentInfo.Children.Add(GetContent(contentData));
                    }
                }

                return contentInfo;
            }
            else
            {
                var content = (IPublishedContent)data;

                foreach (var property in content.Properties)
                {
                    properties.Add(property.PropertyTypeAlias, property.DataValue);
                }

                var contentInfo = new ContentInfo() { Id = content.Id, Alias = content.DocumentTypeAlias, Properties = properties, Name = content.Name, isPublished = true, Path = content.Path };

                if (content != null && content.Children != null && content.Children.Any())
                {
                    foreach (IPublishedContent contentData in content.Children())
                    {
                        if (contentInfo.Children == null)
                        {
                            contentInfo.Children = new List<ContentInfo>();
                        }

                        contentInfo.Children.Add(GetContent(contentData));
                    }
                }

                return contentInfo;
            }
        }

        

        /// <summary>
        /// downloads Image and writes to umbraco.
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="imageUrl"></param>
        /// <param name="altText"></param>
        /// <returns></returns>
        private IMedia CreateMedia_Old(string Name, string imageUrl, string altText)
        {
            IMedia mediaImage = null;

            var mediaService = Services.MediaService;
            var request = WebRequest.Create(imageUrl);
            var webResponse = request.GetResponse();
            var responseStream = webResponse.GetResponseStream();

            string fileName = System.IO.Path.GetFileName(imageUrl);
            var path = HttpContext.Current.Server.MapPath(string.Format("~/temp/{0}", fileName));

            if (responseStream != null)
            {
                var originalImage = new Bitmap(responseStream);

                if(!System.IO.File.Exists(path)) originalImage.Save(path);

                FileStream fileStream = new FileStream(path, FileMode.Open);

                mediaImage = mediaService.CreateMedia(Name, int.Parse(_rootImageFolderId), "Image");
                mediaImage.SetValue("umbracoFile", Name, fileStream);
                mediaImage.SetValue("umbracoAltText", altText);
                mediaService.Save(mediaImage);

                responseStream.Dispose();
                webResponse.Dispose();
                originalImage.Dispose();
                fileStream.Dispose();
            }

            System.IO.File.Delete(path);

            return mediaImage;
        }

        /// <summary>
        /// downloads Image and writes to umbraco.
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="imageUrl"></param>
        /// <param name="altText"></param>
        /// <param name="pageTitle"></param>
        /// <param name="pageType"></param>
        /// <param name="mediaType"></param>
        /// <returns></returns>
        private IMedia CreateMedia(string Name, string imageUrl, string altText, string pageTitle = "", string pageType = "article", string mediaType="image")
        {
            IMedia mediaImage = null;
            var mediaService = Services.MediaService;

            string fileName = System.IO.Path.GetFileName(imageUrl);
            var path = HttpContext.Current.Server.MapPath(string.Format("~/temp/{0}", fileName));

            try
            {
                if(mediaType.ToLower() == "image")
                {
                    using (WebClient webClient = new WebClient())
                    {
                        Stream dataStream = webClient.OpenRead(imageUrl);
                        using (var originalImage = new Bitmap(dataStream))
                        {
                            if (!System.IO.File.Exists(path)) originalImage.Save(path);

                            using (FileStream fileStream = new FileStream(path, FileMode.Open))
                            {
                                var mediaFolderId = GetRootMediaContentFolderIdByType(pageType);

                                //If Article Type, Create Subfolders For Each Article.
                                if (pageType == "article")
                                {
                                    var childrens = mediaService.GetChildren(GetRootMediaContentFolderIdByType(pageType));
                                    var folderInfo = childrens.Where(a => a.Name == pageTitle).FirstOrDefault();
                                    if (folderInfo != null)
                                    {
                                        mediaFolderId = folderInfo.Id;
                                    }
                                    else
                                    {
                                        var mediaFolder = mediaService.CreateMedia(pageTitle, mediaFolderId, "Folder");
                                        mediaService.Save(mediaFolder);

                                        mediaFolderId = mediaFolder.Id;
                                    }
                                }

                                mediaImage = mediaService.CreateMedia(Name, mediaFolderId, "Image");

                                mediaImage.SetValue("umbracoFile", Name, fileStream);
                                mediaImage.SetValue("umbracoAltText", altText);

                                mediaService.Save(mediaImage);
                            }

                            //Delete the file from the Temp folder
                            System.IO.File.Delete(path);
                        }
                    }
                }
                else //This logic applies to downloading files to the server.
                {
                    using (WebClient webClient = new WebClient())
                    {
                        webClient.DownloadFile(imageUrl, path);
                        using (FileStream fileStream = new FileStream(path, FileMode.Open))
                        {
                            var mediaFolderId = GetRootMediaContentFolderIdByType(pageType);

                            //If Article Type, Create Subfolders For Each Article.
                            if (pageType == "article")
                            {
                                var childrens = mediaService.GetChildren(GetRootMediaContentFolderIdByType(pageType));
                                var folderInfo = childrens.Where(a => a.Name == pageTitle).FirstOrDefault();
                                if (folderInfo != null)
                                {
                                    mediaFolderId = folderInfo.Id;
                                }
                                else
                                {
                                    var mediaFolder = mediaService.CreateMedia(pageTitle, mediaFolderId, "Folder");
                                    mediaService.Save(mediaFolder);

                                    mediaFolderId = mediaFolder.Id;
                                }
                            }

                            mediaImage = mediaService.CreateMedia(Name, mediaFolderId, "File");
                            mediaImage.SetValue("umbracoFile", Name, fileStream);
                            mediaService.Save(mediaImage);

                            logger.InfoFormat("Saving MP4 file to the System, Filename : {0}, Url : {1}", Name, imageUrl);
                        }

                        //Delete the file from the Temp folder
                        System.IO.File.Delete(path);
                    }
                }
            }
            catch(Exception ex)
            {
                logger.ErrorFormat("Unable to upload the Media /r/n Image/Video {0}, /r/n Url : {1}, /r/n Error Message {2}", Name, imageUrl, ex.Message);
                return null;
            }

            return mediaImage;
        }

        

        /// <summary>
        /// function to write data to the csv file.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="filePath"></param>
        private void WriteToCSVFile(List<dynamic> data, string filePath)
        {
            using (var csv = new CsvWriter(new StreamWriter(filePath)))
            {
                var firstRecord = data.First() as IDictionary<string, object>;
                var columnList = firstRecord.Keys;

                foreach (var item in columnList)
                {
                    csv.WriteField(item, false);
                }

                csv.NextRecord();

                foreach (var item in data)
                {
                    try
                    {
                        var expandoDict = item as IDictionary<string, object>;
                        foreach (var value in expandoDict.Values)
                        {
                            csv.WriteField(value == null ? string.Empty : value.ToString(), false);
                        }

                        csv.NextRecord();
                    }
                    catch
                    {
                        //Supressing  the Exception
                    }
                }
            }
        }

        /// <summary>
        /// function to parse a csv file and write it to a dynamic object.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        protected List<dynamic> ParseCSVFile(string filename)
        {
            Dictionary<string, int> headers = new Dictionary<string, int>();
            List<dynamic> records = new List<dynamic>();

            using (var sr = new StreamReader(filename))
            {
                var reader = new CsvReader(sr);
                return reader.GetRecords<dynamic>().ToList();
            }
        }

        /// <summary>
        /// saving multiple dropdown values to the umbraco property field.
        /// </summary>
        /// <param name="editableNode"></param>
        /// <param name="propAlias"></param>
        /// <param name="values"></param>
        private static void SetMultipleDropDownValue(IContent editableNode, string propAlias, List<string> values)
        {
            // Grab data type service
            IDataTypeService dts = ApplicationContext.Current.Services.DataTypeService;

            //find the property on the content node by its alias
            Property prop = editableNode.Properties.FirstOrDefault(a => a.Alias == propAlias);

            if (prop != null)
            {
                //get data type from the property
                var dt = dts.GetDataTypeDefinitionById(prop.PropertyType.DataTypeDefinitionId);

                //get the id of the prevalue for 'val'  
                List<int> preValues = new List<int>();
                foreach (var val in values)
                {
                    var selectedItems = dts.GetPreValuesCollectionByDataTypeId(dt.Id).PreValuesAsDictionary.Where(d => d.Value.Value == val);
                    if(selectedItems.Any())
                    {
                        int preValueId = selectedItems.Select(f => f.Value.Id).First();
                        preValues.Add(preValueId);
                    }
                }

                if (preValues != null && preValues.Any())
                {
                    editableNode.SetValue(propAlias, String.Join(",", preValues));
                }
            }
        }

        /// <summary>
        /// saving dropdown value to the umbraco property field.
        /// </summary>
        /// <param name="editableNode"></param>
        /// <param name="propAlias"></param>
        /// <param name="val"></param>
        public static void SetDropDownValue(IContent editableNode, string propAlias, string val)
        {
            // Grab data type service
            IDataTypeService dts = ApplicationContext.Current.Services.DataTypeService;
            
            //find the property on the content node by its alias
            Property prop = editableNode.Properties.FirstOrDefault(a => a.Alias == propAlias);

            if (prop != null)
            {
                //get data type from the property
                var dt = dts.GetDataTypeDefinitionById(prop.PropertyType.DataTypeDefinitionId);

                //get the id of the prevalue for 'val'  
                int preValueId = dts.GetPreValuesCollectionByDataTypeId(dt.Id).PreValuesAsDictionary.Where(d => d.Value.Value.ToLower() == val.ToLower()).Select(f => f.Value.Id).First();

                editableNode.SetValue(propAlias, preValueId);

            }
        }

        /// <summary>
        /// returns datatypeId by dataTypeName.
        /// </summary>
        /// <param name="dataTypeName"></param>
        /// <returns></returns>
        private static int GetDataTypeId(string dataTypeName)
        {
            var dataTypeService = ApplicationContext.Current.Services.DataTypeService;
            var allTypes = dataTypeService.GetAllDataTypeDefinitions();
            return allTypes.First(x => dataTypeName.InvariantEquals(x.Name)).Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataTypeName"></param>
        /// <returns></returns>
        private static PreValueCollection GetPreValues(string dataTypeName)
        {
            int dataTypeId = GetDataTypeId(dataTypeName);
            var dts = ApplicationContext.Current.Services.DataTypeService;
            var preValues = dts.GetPreValuesCollectionByDataTypeId(dataTypeId);
            return preValues;
        }


        /// <summary>
        /// Returns PrevalueId by DatatypeName and Value
        /// </summary>
        /// <param name="dataTypeName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private int GetPreValueIdByDataTypeAndValue(string dataTypeName, string value)
        {
            var dataTypeService = ApplicationContext.Current.Services.DataTypeService;

            var dataTypeId = GetDataTypeId(dataTypeName);

            //get data type from the property
            var dt = dataTypeService.GetDataTypeDefinitionById(dataTypeId);

            //get the id of the prevalue for 'val'  
            int preValueId = dataTypeService.GetPreValuesCollectionByDataTypeId(dt.Id).PreValuesAsDictionary.Where(d => d.Value.Value.ToLower() == value.ToLower()).Select(f => f.Value.Id).First();

            return preValueId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataTypeName"></param>
        /// <returns></returns>
        private static List<System.Web.Mvc.SelectListItem> PreValuesToSelectList(string dataTypeName)
        {
            return (from x in GetPreValues(dataTypeName).PreValuesAsDictionary
                    select new System.Web.Mvc.SelectListItem
                    {
                        Text = x.Value.Value,
                        Value = x.Value.Id.ToString()
                    }).ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="preValueId"></param>
        /// <returns></returns>
        private static string GetPreValueString(string preValueId)
        {
            int Id;
            if (int.TryParse(preValueId, out Id))
            {
                return library.GetPreValueAsString(Id);
            }

            return string.Empty;
        }

        /// <summary>
        /// function to translate tagList to a umbraco taglist.
        /// </summary>
        /// <param name="tags"></param>
        /// <returns></returns>
        private string GetDBTags(List<string> tags)
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var tag in tags)
            {
                stringBuilder.AppendFormat("\"{0}\",", tag);
            }

            return string.Format("[{0}]", stringBuilder.ToString().TrimEnd(","));
        }

        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sectionData"></param>
        /// <returns></returns>
        private List<CustomControl> AddControls(Und sectionData)
        {
            var fieldList = GetFieldList();

            List<CustomControl> controls = new List<CustomControl>();

            if (!string.IsNullOrEmpty(sectionData.item.field_html_text.ToString()) && sectionData.item.field_html_text.GetType() == typeof(JObject))
            {
                controls.Add(new CustomControl() { Name = CONTROLTYPE_HTMLCONTENT, Value = GetJObjectData(sectionData.item.field_html_text.ToString(), fieldList) });
            }

            if (!string.IsNullOrEmpty(sectionData.item.field_media.ToString()) && sectionData.item.field_media.GetType() == typeof(JObject))
            {
                controls.Add(new CustomControl() { Name = CONTROLTYPE_MEDIACONTENT, Value = GetJObjectData(sectionData.item.field_media.ToString(), fieldList) });
            }

            if (!string.IsNullOrEmpty(sectionData.item.field_media_type.ToString()) && sectionData.item.field_media_type.GetType() == typeof(JObject))
            {
                controls.Add(new CustomControl() { Name = CONTROLTYPE_MEDIATYPE, Value = GetJObjectData(sectionData.item.field_media_type.ToString(), fieldList) });
            }

            if (!string.IsNullOrEmpty(sectionData.item.field_youtube.ToString()) && sectionData.item.field_youtube.GetType() == typeof(JObject))
            {
                controls.Add(new CustomControl() { Name = CONTROLTYPE_TEXTSTRING, Value = GetJObjectData(sectionData.item.field_youtube.ToString(), fieldList) });
            }

            if (!string.IsNullOrEmpty(sectionData.item.field_medias.ToString()) && sectionData.item.field_medias.GetType() == typeof(JObject))
            {
                JObject payloadItems = JsonConvert.DeserializeObject<JObject>(sectionData.item.field_medias.ToString(), new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
                foreach (var property in payloadItems.Properties())
                {
                    foreach (var data in property)
                    {
                        if (property.Type != JTokenType.Array && property.Type != JTokenType.Object)
                        {
                            var jObject = data as JArray;
                            foreach (JObject content in jObject.Children<JObject>())
                            {
                                Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();
                                var controlItem = new CustomControl() { Name = CONTROLTYPE_MEDIAS };

                                foreach (JProperty prop in content.Properties())
                                {
                                    if (!keyValuePairs.ContainsKey(prop.Name))
                                    {
                                        if (fieldList.Contains(prop.Name))
                                        {
                                            keyValuePairs.Add(prop.Name, prop.Value.ToString());
                                        }
                                    }
                                }

                                controlItem.Value = keyValuePairs;
                                controls.Add(controlItem);
                            }
                        }
                    }
                }
            }

            return controls;
        }

        /// <summary>
        /// Parses and translates a json string to a dictionary object. 
        /// </summary>
        /// <param name="jsonString"></param>
        /// <param name="fieldList"></param>
        /// <returns></returns>
        private Dictionary<string, string> GetJObjectData(string jsonString, List<string> fieldList)
        {
            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();

            JObject payloadItems = JsonConvert.DeserializeObject<JObject>(jsonString, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            foreach (var property in payloadItems.Properties())
            {
                foreach (var data in property)
                {
                    if (property.Type != JTokenType.Array && property.Type != JTokenType.Object)
                    {
                        var jObject = data as JArray;
                        foreach (JObject content in jObject.Children<JObject>())
                        {
                            foreach (JProperty prop in content.Properties())
                            {
                                if (fieldList.Contains(prop.Name))
                                {
                                    keyValuePairs.Add(prop.Name, prop.Value.ToString());
                                }
                            }
                        }
                    }
                }
            }

            return keyValuePairs;
        }

        /// <summary>
        /// Translates article to a Umbraco GridContent Type.
        /// </summary>
        /// <param name="customArticle"></param>
        /// <param name="contentType"></param>
        /// <returns></returns>
        private string CreateGridContent(CustomArticle customArticle, string contentType)
        {
            GridInfo gridInfo = new GridInfo();

            gridInfo.name = "1 column layout";

            gridInfo.sections = new List<GridSection>();

            GridSection gridSection = new GridSection();
            gridSection.grid = 12;
            gridSection.rows = new List<GridRow>();

            GridRow gridRow = new GridRow();
            gridRow.name = "Full Width";
            gridRow.hasConfig = false;
            gridRow.hasActiveChild = true;
            gridRow.active = true;
            gridRow.id = Guid.NewGuid().ToString();
            gridRow.areas = new List<GridArea>();

            GridArea gridArea = new GridArea();
            gridArea.grid = 12;
            gridArea.hasConfig = false;
            gridArea.allowAll = true;
            gridArea.active = false;
            gridArea.guid = Guid.NewGuid().ToString();
            gridArea.allowed = new List<string>();

            gridArea.allowed.Add("media");
            gridArea.allowed.Add("embed");
            gridArea.allowed.Add("headline");
            gridArea.allowed.Add("rte");
            gridArea.allowed.Add("macro");
            gridArea.allowed.Add("carousel");
            gridArea.allowed.Add("quote");
            gridArea.allowed.Add("terratype");

            gridArea.controls = new List<GridControl>();
            foreach (var section in customArticle.Sections)
            {
                if(section.SectionName == "sectionText")
                {
                    var gridControls = GetGridControlForSectionText(section.Controls, customArticle, contentType);
                    gridArea.controls.AddRange(gridControls);
                }
                else
                {
                    var gridControl = GetGridControlByType(GetNewSectionName(section.SectionName), section.Controls, customArticle, contentType, section.SectionName);
                    if(gridControl != null)
                    {
                        gridArea.controls.Add(gridControl);
                    }
                }
            }

            gridRow.areas.Add(gridArea);
            gridSection.rows.Add(gridRow);
            gridInfo.sections.Add(gridSection);

            return JsonConvert.SerializeObject(gridInfo, Formatting.None,
                            new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            });
        }

        /// <summary>
        /// Splitting the Section Text into Multiple Section Quote.
        /// </summary>
        /// <param name="customControls">List of Controls in Section Text.</param>
        /// <param name="article">Article from the Old CMS.</param>
        /// <param name="contentType">Image/Video</param>
        /// <returns></returns>
        private List<GridControl> GetGridControlForSectionText(List<CustomControl> customControls, CustomArticle article, string contentType)
        {
            List<GridControl> gridControls = new List<GridControl>();
            StringBuilder paragraphCollection = new StringBuilder();

            var controlItem = customControls.FirstOrDefault();
            if (controlItem.Name == CONTROLTYPE_HTMLCONTENT)
            {
                var htmlText = controlItem.Value["value"];
                if(htmlText.Length > MAX_QUOTE_TEXT_LENGTH)
                {
                    var paragraphCount = HtmlRemoval.GetParagraphCount(htmlText);
                    for(int i = 0; i <= paragraphCount - 1; i++)
                    {
                        var paragraph = HtmlRemoval.HtmlGetParagraphByOccurence(htmlText, i);
                        paragraphCollection.Append(paragraph);

                        logger.DebugFormat("Paragraph Content : {0}", paragraph);

                        if ((paragraphCollection.ToString().Count() > MAX_QUOTE_TEXT_LENGTH) || i == paragraphCount)
                        {
                            Dictionary<string, string> data = new Dictionary<string, string>();
                            data.Add("value", paragraphCollection.ToString());

                            logger.DebugFormat("The Section Text Content : {0}", paragraphCollection.ToString());

                            CustomControl controlControl = new CustomControl() { Name = CONTROLTYPE_HTMLCONTENT, Value = data };
                            var gridControl = GetGridControlByType("sectionQuote", new List<CustomControl>() { controlControl } , article, contentType, i % 2 == 0 ? "sectionLeftCopy" : "sectionRightCopy");

                            gridControls.Add(gridControl);

                            //Reset StringBuilder
                            paragraphCollection = new StringBuilder();
                        }
                    }
                }
                else
                {
                    var gridControl = GetGridControlByType("sectionQuote", customControls, article, contentType, "sectionLeftCopy");
                    gridControls.Add(gridControl);
                }
            }

            return gridControls;
        }

        /// <summary>
        /// returns migration section mappings
        /// </summary>
        /// <param name="sectionName"></param>
        /// <returns></returns>
        private string GetNewSectionName(string sectionName)
        {
            Dictionary<string, string> sectionMapping = new Dictionary<string, string>();

            sectionMapping.Add("sectionLeftCopy", "sectionQuote");
            sectionMapping.Add("sectionRightCopy", "sectionQuote");
            sectionMapping.Add("sectionText", "sectionQuote");

            if(sectionMapping.ContainsKey(sectionName))
            {
                return sectionMapping[sectionName];
            }

            return sectionName;
        }

        /// <summary>
        /// returns mediaId by mediaType.
        /// </summary>
        /// <param name="mediaType"></param>
        /// <returns></returns>
        private string GetMediaTypeId(string mediaType)
        {
            return GetPreValueIdByDataTypeAndValue("MediaType", mediaType).ToString();
        }

        /// <summary>
        /// returns mediaId by mediaType
        /// </summary>
        /// <param name="mediaType"></param>
        /// <returns></returns>
        private string GetContentMediaTypeId(string mediaType)
        {
            return GetPreValueIdByDataTypeAndValue("Dropdown MediaType", mediaType).ToString();
        }

        /// <summary>
        /// returns orientationId by type
        /// </summary>
        /// <param name="orientation"></param>
        /// <returns></returns>
        private string GetOrientationId(string orientation)
        {
            return GetPreValueIdByDataTypeAndValue("Dropdown Orientation", orientation).ToString();
        }


        /// <summary>
        /// returns gridcontrols by contentType
        /// </summary>
        /// <param name="controlType"></param>
        /// <param name="controls"></param>
        /// <param name="articleInfo"></param>
        /// <param name="contentType"></param>
        /// <param name="oldControlType">This is the name of the section type from the old system.</param>
        /// <returns></returns>
        private GridControl GetGridControlByType(string controlType, List<CustomControl> controls, CustomArticle articleInfo, string contentType, string oldControlType)
        {
            GridControl gridControl = GetGridControlByType(controlType);

            if((oldControlType == "sectionLeftCopy" || oldControlType == "sectionRightCopy") && !controls.Where(a=>a.Name == CONTROLTYPE_HTMLCONTENT).Any())
            {
                return null;
            }

            foreach (var controlItem in controls)
            {
                var ctrName = controlItem.Name;
                var item = GetControlByType(ctrName);
                if (ctrName == CONTROLTYPE_HTMLCONTENT)
                {
                    item.htmlText.value = controlItem.Value["value"];
                    if(oldControlType == "sectionLeftCopy")
                    {
                        item.orientation.value = new List<string>() { GetOrientationId("Left") };
                    }
                    if (oldControlType == "sectionRightCopy")
                    {
                        item.orientation.value = new List<string>() { GetOrientationId("Right") };
                    }
                }
                else if (ctrName == CONTROLTYPE_MEDIATYPE)
                {
                    item.mediaType.value = new List<string>() { GetMediaTypeId(controlItem.Value["value"]) };
                }
                else if (ctrName == CONTROLTYPE_TEXTSTRING)
                {
                    item.textString.value = controlItem.Value["url"];
                    item.url.value = controlItem.Value["url"];
                }
                else if (ctrName == CONTROLTYPE_MEDIACONTENT)
                {
                    var mediaType = controlItem.Value["type"];
                    string imageUrl = string.Format(mediaType == "video" ? mediaPrefix : imagePrefix, Uri.EscapeUriString(controlItem.Value["uri"].Replace(DEFAULT_URI_STRING, string.Empty)));
                    var mediaItem = CreateMedia(controlItem.Value["filename"], imageUrl, controlItem.Value["alt"], articleInfo.Title, contentType, mediaType);

                    if (mediaItem != null)
                    {
                        item.media.value = mediaItem.GetUdi().ToString();
                        item.altText.value = controlItem.Value["alt"];
                        item.contentMediaType.value = new List<string>() { GetContentMediaTypeId(controlItem.Value["type"]) };
                    }
                }
                else if (ctrName == CONTROLTYPE_MEDIAS)
                {
                    var mediaType = controlItem.Value["type"];
                    string imageUrl = string.Format(mediaType == "video" ? mediaPrefix : imagePrefix, Uri.EscapeUriString(controlItem.Value["uri"].Replace(DEFAULT_URI_STRING, string.Empty)));
                    var mediaItem = CreateMedia(controlItem.Value["filename"], imageUrl, controlItem.Value["alt"], articleInfo.Title, contentType, mediaType);

                    if (mediaItem != null)
                    {
                        item.media.value = mediaItem.GetUdi().ToString();
                        item.altText.value = controlItem.Value["alt"];
                        item.contentMediaType.value = new List<string>() { GetContentMediaTypeId(controlItem.Value["type"]) };
                    }
                }

                if ((GetLeBlenderDataTypeMulProperties().Contains(controlType)) && gridControl.value.Count == 1)
                {
                    var dataItem = gridControl.value.FirstOrDefault();
                    if(dataItem.htmlText == null)
                    {
                        dataItem.htmlText = item.htmlText;
                    }
                    if(dataItem.media == null)
                    {
                        dataItem.media = item.media;
                    }
                    if(dataItem.mediaType == null)
                    {
                        dataItem.mediaType = item.mediaType;
                    }
                    if (dataItem.url == null)
                    {
                        dataItem.url = item.url;
                    }
                    if (dataItem.textString == null)
                    {
                        dataItem.textString = item.textString;
                    }
                    if(dataItem.altText == null)
                    {
                        dataItem.altText = item.altText;
                    }
                    if(dataItem.contentMediaType == null)
                    {
                        dataItem.contentMediaType = item.contentMediaType;
                    }
                    if (dataItem.orientation == null)
                    {
                        dataItem.orientation = item.orientation;
                    }
                    if (dataItem.quote == null)
                    {
                        dataItem.quote = item.quote;
                    }
                }
                else
                {
                    gridControl.value.Add(item);
                }
            }

            return gridControl;
        }

        /// <summary>
        /// Returns GridContent Object
        /// </summary>
        /// <param name="controlType"></param>
        /// <returns></returns>
        private GridControl GetGridControlByType(string controlType)
        {
            GridControl gridControl = new GridControl();

            gridControl.active = true;
            gridControl.guid = Guid.NewGuid().ToString();
            gridControl.editor = new GridEditor();
            gridControl.editor.alias = controlType;

            gridControl.value = new List<Value>();

            return gridControl;
        }

        /// <summary>
        /// Returns Control by Type.
        /// </summary>
        /// <param name="controlType"></param>
        /// <returns></returns>
        private Value GetControlByType(string controlType)
        {
            var valueItem = new Value();

            if (controlType == CONTROLTYPE_HTMLCONTENT)
            {
                valueItem.htmlText = new HtmlText();
                valueItem.htmlText.value = "<p>Put Html Content Here</p>";
                valueItem.htmlText.dataTypeGuid = "ca90c950-0aff-4e72-b976-a30b1ac57dad";
                valueItem.htmlText.editorAlias = "htmlText";
                valueItem.htmlText.editorName = "HtmlText";

                valueItem.orientation = new DDLOrientation();
                valueItem.orientation.value = "Select Orientation";
                valueItem.orientation.dataTypeGuid = "0adb1187-d2c7-4487-b904-aaa1cf175375";
                valueItem.orientation.editorAlias = "orientation";
                valueItem.orientation.editorName = "Orientation";

                valueItem.quote = new HtmlText();
                valueItem.quote.value = "<p>Put Html Content Here</p>";
                valueItem.quote.dataTypeGuid = "ca90c950-0aff-4e72-b976-a30b1ac57dad";
                valueItem.quote.editorAlias = "quote";
                valueItem.quote.editorName = "Quote";
            }
            else if (controlType == CONTROLTYPE_MEDIACONTENT)
            {
                valueItem.media = new Audi.Magazine.Model.Media();
                valueItem.media.value = "umb://media/208abda163b54ba1bc2a3d40fe156bb6";
                valueItem.media.dataTypeGuid = "135d60e0-64d9-49ed-ab08-893c9ba44ae5";
                valueItem.media.editorAlias = "media";
                valueItem.media.editorName = "Media";

                valueItem.contentMediaType = new DDLContentMediaType();
                valueItem.contentMediaType.value = "Select MediaType";
                valueItem.contentMediaType.dataTypeGuid = "c2cf9dcc-fd66-4c30-a780-b46de4f0cfb0";
                valueItem.contentMediaType.editorAlias = "contentMediaType";
                valueItem.contentMediaType.editorName = "MediaType";

                valueItem.altText = new TextString();
                valueItem.altText.value = "Select Alt Text";
                valueItem.altText.dataTypeGuid = "0cc0eba1-9960-42c9-bf9b-60e150b429ae";
                valueItem.altText.editorAlias = "altText";
                valueItem.altText.editorName = "Alt Text";
            }
            else if (controlType == CONTROLTYPE_MEDIAS)
            {
                valueItem.media = new Audi.Magazine.Model.Media();
                valueItem.media.value = "umb://media/208abda163b54ba1bc2a3d40fe156bb6";
                valueItem.media.dataTypeGuid = "135d60e0-64d9-49ed-ab08-893c9ba44ae5";
                valueItem.media.editorAlias = "media";
                valueItem.media.editorName = "Media";

                valueItem.contentMediaType = new DDLContentMediaType();
                valueItem.contentMediaType.value = "Select MediaType";
                valueItem.contentMediaType.dataTypeGuid = "c2cf9dcc-fd66-4c30-a780-b46de4f0cfb0";
                valueItem.contentMediaType.editorAlias = "contentMediaType";
                valueItem.contentMediaType.editorName = "MediaType";

                valueItem.altText = new TextString();
                valueItem.altText.value = "Select Alt Text";
                valueItem.altText.dataTypeGuid = "0cc0eba1-9960-42c9-bf9b-60e150b429ae";
                valueItem.altText.editorAlias = "altText";
                valueItem.altText.editorName = "Alt Text";
            }
            else if (controlType == CONTROLTYPE_MEDIATYPE)
            {
                valueItem.mediaType = new DDLMediaType();
                valueItem.mediaType.value = "Select MediaType";
                valueItem.mediaType.dataTypeGuid = "836d9e1b-0edf-482a-8e5d-daa8cc6a7ba0";
                valueItem.mediaType.editorAlias = "mediaType";
                valueItem.mediaType.editorName = "MediaType";
            }
            else if (controlType == CONTROLTYPE_TEXTSTRING)
            {
                valueItem.textString = new TextString();
                valueItem.textString.value = "Select Url";
                valueItem.textString.dataTypeGuid = "0cc0eba1-9960-42c9-bf9b-60e150b429ae";
                valueItem.textString.editorAlias = "url";
                valueItem.textString.editorName = "Url";

                valueItem.url = new Url();
                valueItem.url.value = "Select Url";
                valueItem.url.dataTypeGuid = "0cc0eba1-9960-42c9-bf9b-60e150b429ae";
                valueItem.url.editorAlias = "url";
                valueItem.url.editorName = "Url";
            }

            return valueItem;
        }

        /// <summary>
        /// returns article by slug from the old CMS.
        /// </summary>
        /// <param name="slugName"></param>
        /// <param name="categories"></param>
        /// <param name="tags"></param>
        /// <param name="categoryCollection"></param>
        /// <param name="migrationArticleInfo"></param>
        /// <param name="sectors"></param>
        /// <returns></returns>
        private CustomArticle GetArticleDetailsBySlug(string slugName, List<CategoryInfo> categories, List<TagInfo> tags, List<dynamic> categoryCollection, MigrationArticleInfo migrationArticleInfo, List<SectorInfo> sectors)
        {
            var responseData = string.Empty;
            MigrationArticleInfo responseInfo = null;

            try
            {
                if(migrationArticleInfo == null)
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(ConfigurationManager.AppSettings["MagazineBaseUrl"]);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        client.DefaultRequestHeaders.Add("X-CSRF-Token", "54Iz2agHG2MV3upB3vOeVLEFdgcJ7o77D7CSOLoPEPI");

                        if (slugName == null) throw new ApplicationException("Missing or invalid parameters");

                        LookUp data = new LookUp() { Alias = slugName };

                        var content = JsonConvert.SerializeObject(data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
                        var payload = new StringContent(content, Encoding.UTF8, "application/json");
                        var response = client.PostAsync("api/node/lookup.json", payload).Result;

                        responseData = response.Content.ReadAsStringAsync().Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            return null;
                        }
                    }

                    if (string.IsNullOrEmpty(responseData) || responseData.GetType() == typeof(JObject))
                    {
                        return null;
                    }

                    var fieldList = GetFieldList();
                    responseInfo = JsonConvert.DeserializeObject<MigrationArticleInfo>(responseData, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(), NullValueHandling = NullValueHandling.Ignore });
                }
                else
                {
                    responseInfo = migrationArticleInfo;
                }

                CustomArticle articleInfo = new CustomArticle();
                articleInfo.Title = responseInfo.title;
                articleInfo.Type = responseInfo.type;

                //Set Subtitle
                if (responseInfo.body != null && responseInfo.body.und.Any())
                {
                    articleInfo.SubTitle = responseInfo.body.und.FirstOrDefault().value;
                }

                //Set Sector
                if (responseInfo.field_sectors != null && responseInfo.field_sectors.und.Any())
                {
                    var sectorId = responseInfo.field_sectors.und.FirstOrDefault().tid;
                    articleInfo.Sectors = sectors.Where(a => a.tid == sectorId).FirstOrDefault().name;
                }

                //Set TileImage
                if (responseInfo.field_tile_image != null &&  responseInfo.field_tile_image.ToString() != "[]" && responseInfo.field_tile_image.GetType() == typeof(JObject))
                {
                    var imageInfo = JsonConvert.DeserializeObject<FieldTileImage>(responseInfo.field_tile_image.ToString(), new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(), NullValueHandling = NullValueHandling.Ignore });
                    articleInfo.TileImage = GetImageInfo(imageInfo.und.FirstOrDefault());
                }

                //Set Image
                if (responseInfo.field_image != null && responseInfo.field_image.und.Any())
                {
                    articleInfo.Image = GetImageInfo(responseInfo.field_image.und.FirstOrDefault());
                }

                //Set Image
                if (responseInfo.field_logo != null && responseInfo.field_logo.und.Any())
                {
                    articleInfo.Logo = GetImageInfo(responseInfo.field_logo.und.FirstOrDefault());
                }

                //Set Heromedia
                if (responseInfo.field_hero_media != null && responseInfo.field_hero_media.und.Any())
                {
                    articleInfo.HeroMedia = GetImageInfo(responseInfo.field_hero_media.und.FirstOrDefault());
                }

                //Set Metatags
                if (responseInfo.metatags != null)
                {
                    articleInfo.MetaTags = new MetaTags() { Description = responseInfo.metatags.und.description.value };
                }

                //Set Link
                if (responseInfo.field_link != null && responseInfo.field_link.ToString() != "[]" && responseInfo.field_link.GetType() == typeof(JObject))
                {
                    var fieldList = GetFieldList();
                    articleInfo.Link = GetJObjectData(responseInfo.field_link.ToString(), fieldList)["url"];
                }

                //Set Categories
                if (responseInfo.field_categories != null)
                {
                    articleInfo.Categories = new List<string>();
                    var itemCategories = categoryCollection.Where(a => a.Id == responseInfo.nid).FirstOrDefault();

                    if(itemCategories != null && itemCategories.Categories != null)
                    {
                        foreach (var category in itemCategories.Categories.ToString().Split(';'))
                        {
                            articleInfo.Categories.Add(category.ToString().Trim());
                        }
                    }
                }

                //Set Tags
                if (responseInfo.field_tags != null && responseInfo.field_tags.ToString() != "[]" && responseInfo.field_tags.GetType() == typeof(JObject))
                {
                    var tagList = JsonConvert.DeserializeObject<FieldTags>(responseInfo.field_tags.ToString(), new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(), NullValueHandling = NullValueHandling.Ignore });

                    articleInfo.Tags = new List<string>();
                    foreach (var tag in tagList.und)
                    {
                        var tagId = tag.tid.Replace(",", string.Empty);
                        var tagItem = tags.FirstOrDefault(a => a.tid.Replace(",", string.Empty) == tagId);
                        if (tagItem != null)
                        {
                            articleInfo.Tags.Add(tagItem.name);
                        }
                    }
                }

                //Set Sections
                var sections = responseInfo.field_sections;
                if(sections != null)
                {
                    List<CustomSection> customSections = new List<CustomSection>();
                    foreach (var section in sections.und)
                    {
                        CustomSection sectionInfo = new CustomSection();

                        sectionInfo.SectionName = GetSectionName(section);
                        sectionInfo.Controls = AddControls(section);

                        customSections.Add(sectionInfo);
                    }

                    articleInfo.Sections = customSections;
                }

                //Set Images to do
                var images = responseInfo.field_images;
                if (images != null)
                {
                    List<CustomSection> customImageSections = new List<CustomSection>();
                    CustomSection sectionInfo = new CustomSection() { SectionName = "sectionGallery" };
                    sectionInfo.Controls = new List<CustomControl>();

                    foreach (var imageInfo in images.und)
                    {
                        CustomControl customControl = new CustomControl() { Name = CONTROLTYPE_MEDIAS, Value = GetImageInfo(imageInfo) };
                        sectionInfo.Controls.Add(customControl);
                    }

                    customImageSections.Add(sectionInfo);

                    if (articleInfo.Sections == null)
                        articleInfo.Sections = new List<CustomSection>();

                    articleInfo.Sections.AddRange(customImageSections);
                }

                return articleInfo;
            }
            catch (Exception e)
            {
                logger.InfoFormat("Unable to save article : {0}", slugName);
                logger.Error(e);
                return null;
            }
        }

        #endregion

        #region Constants

        /// <summary>
        /// returns imageInfo object.
        /// </summary>
        /// <param name="imageInfo"></param>
        /// <returns></returns>
        private Dictionary<string, string> GetImageInfo(Und4 imageInfo)
        {
            Dictionary<string, string> infos = new Dictionary<string, string>();

            infos.Add("filename", imageInfo.filename);
            infos.Add("filemime", imageInfo.filemime);
            infos.Add("type", string.IsNullOrEmpty(imageInfo.type) ? "Image" : imageInfo.type);
            infos.Add("alt", imageInfo.alt);
            infos.Add("title", imageInfo.title);
            infos.Add("uri", imageInfo.uri);

            return infos;
        }

        /// <summary>
        /// returns umbraco sectionname.
        /// </summary>
        /// <param name="section"></param>
        /// <returns></returns>
        private string GetSectionName(Und section)
        {
            var sectionName = section.item.field_section_type.und[0].value;

            Dictionary<string, string> sectionMapping = new Dictionary<string, string>();

            sectionMapping.Add("Quote", "sectionIntro");
            sectionMapping.Add("Left_copy", "sectionLeftCopy");
            sectionMapping.Add("Gallery", "sectionGallery");
            sectionMapping.Add("Right_copy", "sectionRightCopy");
            sectionMapping.Add("Image_short_copy", "sectionImageShortCopy");
            sectionMapping.Add("Text_only", "sectionText");

            return sectionMapping[sectionName];
        }

        /// <summary>
        /// returns list of all the sections with multipleLeblender properties.
        /// </summary>
        /// <returns></returns>
        private List<string> GetLeBlenderDataTypeMulProperties()
        {
            List<string> items = new List<string>();

            items.Add("sectionLeftCopy");
            items.Add("sectionRightCopy");
            items.Add("sectionImageShortCopy");
            items.Add("sectionQuote");

            return items;
        }

        /// <summary>
        /// returns field list.
        /// </summary>
        /// <returns></returns>
        private List<string> GetFieldList()
        {
            List<string> fieldList = new List<string>();

            fieldList.Add("filename");
            fieldList.Add("filemime");
            fieldList.Add("type");
            fieldList.Add("alt");
            fieldList.Add("title");
            fieldList.Add("value");
            fieldList.Add("uri");
            fieldList.Add("url");
            fieldList.Add("field_poster");

            return fieldList;
        }

        /// <summary>
        /// returns list of all the content types to be processed.
        /// </summary>
        /// <returns></returns>
        private List<string> ContentTypes()
        {
            List<string> fieldList = new List<string>();

            fieldList.Add("ambassador");
            fieldList.Add("article");
            fieldList.Add("gallery");
            fieldList.Add("partner");
            fieldList.Add("quote");
            fieldList.Add("video");

            return fieldList;
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public class SectorInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string tid { get; set; }
    }


    /// <summary>
    /// 
    /// </summary>
    public class MigrationCategoryInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string tid { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string image { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string term_description { get; set; }
    }
}