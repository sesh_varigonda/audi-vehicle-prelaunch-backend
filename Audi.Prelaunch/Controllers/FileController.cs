﻿using Audi.Prelaunch.Business.Entities;
using AutoMapper;
using StackExchange.Profiling.Helpers.Dapper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;

namespace Audi.Prelaunch.Cms.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class FileController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        public FileController()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Invoice(string id)
        {
            BookingInfo bookingInfo = null;

            using (System.Data.IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString))
            {
                db.Open();
                bookingInfo = db.Query<BookingInfo>("Select * from audiBooking").Where(a => a.BookingNumber == id).FirstOrDefault();
                db.Close();
            }

            if (bookingInfo == null) return RedirectToAction("error", new { id = "view" });

            Mapper.CreateMap<BookingInfo, BookingGetModel>();
            var data = Mapper.Map<BookingGetModel>(bookingInfo);

            data.BookingId = bookingInfo.BookingNumber;

            //Setting customer data, will be replaced with an API Call to myAudi to GetCustomer Data.
            data.FirstName = "Hemanth";
            data.LastName = "Varigonda";
            data.Email = "sesh.varigonda@303mullenlowe.com.au";

            //Setting
            data.options = new BookingOptionInfo();
            data.options.ExteriorColor = "Red";
            data.options.InteriorColor = "Grey";
            data.options.Packages = new List<string>() { "Standard Inclusions", "Premium Inclusions" };

            ViewBag.BaseUrl = GetBaseUrl();
            
            return View(data);
        }

        private string GetBaseUrl()
        {
            var request = System.Web.HttpContext.Current.Request;
            var baseUrl = string.Format("{0}://{1}", request.Url.Scheme, request.Url.Authority);

            return baseUrl;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Error()
        {
            return View();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class BookingGetModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string BookingId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public BookingOptionInfo options { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class BookingOptionInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string ExteriorColor { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string InteriorColor { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<string> Packages { get; set; }
    }
}