﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using Umbraco.Web.WebApi;
using Swashbuckle.Swagger.Annotations;
using log4net;
using System.Reflection;
using System.Web.Http.Cors;
using Umbraco.Web;
using Audi.Prelaunch.Cms.Service;
using System.Linq;
using Audi.Prelaunch.Models;
using System.Threading.Tasks;
using System.Net.Http;
using System.Configuration;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Net.Http.Formatting;
using Newtonsoft.Json.Serialization;
using System.Web.Http.Description;
using Audi.Prelaunch.Business.Models;
using System.Diagnostics;
using TheArtOfDev.HtmlRenderer.PdfSharp;
using PdfSharp;
using System.IO;
using System.Data.SqlClient;
using StackExchange.Profiling.Helpers.Dapper;
using System.Web.Http.Controllers;
using Audi.Prelaunch.Cms.Business;
using Audi.Prelaunch.Business.Service;
using Audi.Prelaunch.Business.Entities;

namespace Audi.Prelaunch.Cms.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("1/service")]
    [EnableCors("*", "*", "*")]
    public class ServiceController : UmbracoApiController
    {
        ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private const string SOURCE = "Web";

        private static readonly IContentNegotiator _conneg = GlobalConfiguration.Configuration.Services.GetContentNegotiator();

        private static readonly IEnumerable<JsonMediaTypeFormatter> _mediaTypeFormatters = new[] {
        new JsonMediaTypeFormatter {
            SerializerSettings = new JsonSerializerSettings {
                ContractResolver = new DefaultContractResolver()
            }
        }
    };

        /// <summary>
        /// returns vehicle details. (Works)
        /// </summary>
        /// <returns></returns>
        [Route("vehicle")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(VehicleInfo), Description = "success")]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(GenericErrorModel), Description = "missing or invalid parameters")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(GenericErrorModel), Description = "unexpected error")]
        public IHttpActionResult GetVehicle()
        {
            ModelService modelService = new ModelService();

            try
            {
                var contentItem = modelService.GetList();
                return Ok(contentItem);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Unable to return vehicle information, ERROR MESSAGE : {0}", ex.Message);
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// returns vehicle price details by state. (Works)
        /// </summary>
        /// <returns></returns>
        [Route("vehiclePrice")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(PricingResponseMessage), Description = "success")]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(GenericErrorModel), Description = "missing or invalid parameters")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(GenericErrorModel), Description = "unexpected error")]
        public IHttpActionResult GetVehiclePriceByState(string state = "NSW")
        {
            var result = string.Empty;
            var priceInfo = new PricingResponseMessage();

            var variantId = ConfigurationManager.AppSettings["VariantId"];
            var engineId = ConfigurationManager.AppSettings["EngineId"];

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["ApiMyAudiBaseUrl"]);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = client.GetAsync(string.Format("/1/legacy/GetPrice?variantId={0}&state={1}&engineId={2}", variantId, state.ToUpper(), engineId)).Result;

                    result = response.Content.ReadAsStringAsync().Result;
                    if (response.IsSuccessStatusCode)
                    {
                        priceInfo = JsonConvert.DeserializeObject<PricingResponseMessage>(result);
                    }
                    else if (response.StatusCode == HttpStatusCode.InternalServerError)
                    {
                        var exception = JsonConvert.DeserializeObject<Exception>(result);
                        priceInfo = new PricingResponseMessage() { success = false, message = exception.Message };
                    }
                }

                return Ok(priceInfo);
            }
            catch (Exception e)
            {
                logger.Error(e);
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// Retrieves Cart Information by CartId
        /// </summary>
        /// <param name="cartId">Enter the CartId recieved from the email to see the saved options.</param>
        /// <returns></returns>
        [Route("cart/{cartId}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SaveOptionInfo), Description = "success")]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(GenericErrorModel), Description = "missing or invalid parameters")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(GenericErrorModel), Description = "unexpected error")]
        public IHttpActionResult GetCartDetails(string cartId)
        {
            CartService cartService = new CartService();

            try
            {
                var cartInfo = cartService.GetCart(cartId);
                if(cartInfo == null)
                {
                    return NotFound();
                }

                return Ok(cartInfo);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Unable to save vehicle options, ERROR MESSAGE : {0}", ex.Message);
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Save Options and send an email to customer (Works)
        /// </summary>
        /// <param name="saveOptionsRequestModel">Validates the EmailAddress using DataTools Validation API and sends the Email Notification to Customer</param>
        /// <returns></returns>
        [Route("saveOptions")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(SaveOptionInfo), Description = "success")]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(GenericErrorModel), Description = "missing or invalid parameters")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(GenericErrorModel), Description = "unexpected error")]
        public IHttpActionResult PostSaveOptions(SaveOptionInfo saveOptionsRequestModel)
        {
            ModelService modelService = new ModelService();
            EmailService emailService = new EmailService();
            NotificationService notificationService = new NotificationService();

            try
            {
                if (saveOptionsRequestModel == null)
                {
                    return BadRequest("Missing or invalid parameters.");
                }

                var actionContext = ValidateModel(saveOptionsRequestModel, typeof(SaveOptionInfo));
                if (!actionContext.ModelState.IsValid)
                {
                    return BadRequest(actionContext.ModelState);
                }

                if (!emailService.ValidateEmailAddress(saveOptionsRequestModel.EmailAddress))
                {
                    return BadRequest("Invalid EmailAddress");
                }

                notificationService.QueueSaveoptions(saveOptionsRequestModel, SOURCE);

                return Ok();
            }
            catch (BadRequestException e)
            {
                return BadRequest(e.Message);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (ForbiddenException e)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.Forbidden, e.Message));
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Unable to get articles by tag, ERROR MESSAGE : {0}", ex.Message);
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// Returns list of faqs (Works)
        /// </summary>
        /// <returns></returns>
        [Route("getFAQs")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(FAQCollection), Description = "success")]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(GenericErrorModel), Description = "missing or invalid parameters")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(GenericErrorModel), Description = "unexpected error")]
        public IHttpActionResult GetFAQs()
        {
            FAQService faqService = new FAQService();
            

            try
            {
                var categories = new DataTypeService().GetCategories().Select(a=> new CategoryInfo() { Label = a.Name, Slug = a.slug }).ToList();

                var faqCollection = faqService.GetList();
                if (faqCollection.Count > 0)
                {
                    FAQCollection faqList = new FAQCollection() { categories = categories, questions = faqCollection };

                    return Ok(faqList);
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Unable to get faqs, ERROR MESSAGE : {0}", ex.Message);
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// function to create a customer order. (Works)
        /// </summary>
        /// <returns></returns>
        [Route("order/create")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(BookingInfo), Description = "success")]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(GenericErrorModel), Description = "missing or invalid parameters")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(GenericErrorModel), Description = "unexpected error")]
        public IHttpActionResult CreateOrder(OrderRequestModel orderRequestModel)
        {
            BookingService bookingService = new BookingService();
            NotificationService notificationService = new NotificationService();
            EmailService emailService = new EmailService();

            try
            {
                if (orderRequestModel == null)
                {
                    return BadRequest("Missing or invalid parameters.");
                }

                var actionContext = ValidateModel(orderRequestModel, typeof(OrderRequestModel));
                if (!actionContext.ModelState.IsValid)
                {
                    return BadRequest(actionContext.ModelState);
                }


                if (!emailService.ValidateEmailAddress(orderRequestModel.customer.Email))
                {
                    return BadRequest("Invalid EmailAddress");
                }

                //Create a Booking
                var booking = bookingService.CreateBooking(orderRequestModel, SOURCE);

                //Trigger a Notification
                notificationService.QueueBookingConfirmation(orderRequestModel, booking, SOURCE);

                return Ok();
            }
            catch (BadRequestException e)
            {
                return BadRequest(e.Message);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (ForbiddenException e)
            {
                return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.Forbidden, e.Message));
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Unable to get articles by tag, ERROR MESSAGE : {0}", ex.Message);
                return InternalServerError(ex);
            }
        }


        /// <summary>
        /// Returns dealers by model (Works)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("dealersByModel/{model}")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(DealerGetInfo),  Description = "success")]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(ModelErrorModel), Description = "missing or invalid parameters")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(GenericErrorModel), Description = "unexpected error")]
        public IHttpActionResult GetDealersByModel(string model)
        {
            var result = string.Empty;

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["ApiCMSInternalBaseUrl"]);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = client.GetAsync(string.Format("/2/legacy/dealers/model/{0}", model)).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsStringAsync().Result;
                    }
                }

                var dealers = JsonConvert.DeserializeObject<DealerResponseMessage>(result);
                return Ok(dealers);
            }
            catch (Exception e)
            {
                logger.Error(e);
                return InternalServerError(e);
            }
        }

        /// <summary>
        /// [USED FOR TESTING] returns a test token from stripe (Works)
        /// </summary>
        /// <returns></returns>
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("testToken")]
        public IHttpActionResult GetTestToken()
        {
            StripeService stripeService = new StripeService();
            return Ok(stripeService.GetStripeTestToken());
        }


        /// <summary>
        /// returns invoice for booking
        /// </summary>
        /// <param name="id">booking id</param>
        /// <returns></returns>
        [AllowAnonymous]
        [Route("invoice/{id}")]
        [SwaggerResponse(HttpStatusCode.OK, Description = "success")]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(GenericErrorModel), Description = "invalid parameters")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(GenericErrorModel), Description = "unexpected error")]
        public async Task<HttpResponseMessage> GetInvoice(string id)
        {
            BookingInfo bookingInfo = null;

            using (System.Data.IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["umbracoDbDSN"].ConnectionString))
            {
                db.Open();
                bookingInfo = db.Query<BookingInfo>("Select * from audiBooking").Where(a => a.BookingNumber == id).FirstOrDefault();
                db.Close();
            }

            if (bookingInfo == null)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            var assetBaseUrl = ConfigurationManager.AppSettings["AssetBaseUrl"];
            var assetBasePath = ConfigurationManager.AppSettings["AssetBasePath"];
            var baseUrl = ConfigurationManager.AppSettings["ApiBaseUrl"];
            var internalBaseUrl = ConfigurationManager.AppSettings["ApiInternalBaseUrl"];
            var invoiceUrl = String.Format("{0}/file/invoice/{1}", internalBaseUrl, id);

            var eventLog = new EventLog { Source = "Application", Log = "Application" };

            try
            {
                var html = await GetResponse(invoiceUrl);

                if (!String.IsNullOrEmpty(html))
                {
                    var file = PdfGenerator.GeneratePdf(html, PageSize.A4);
                    file.Save(Path.Combine(assetBasePath, "files", "invoice", string.Format("audi_invoice_{0}.pdf", id.ToLower())));
                    invoiceUrl = String.Format("{0}/files/invoice/audi_invoice_{1}.pdf", assetBaseUrl, id.ToLower());
                }
                else
                {
                    invoiceUrl = String.Format("{0}/file/invoice/{1}", baseUrl, id);
                    eventLog.WriteEntry("Invoice PDF: Invoice HTML Fetch Failed", EventLogEntryType.Error);
                }
            }
            catch (Exception e)
            {
                invoiceUrl = String.Format("{0}/file/invoice/{1}", baseUrl, id);
                eventLog.WriteEntry("Invoice PDF: Invoice HTML Fetch Failed, exception : " + e.Message, EventLogEntryType.Error);

            }

            var response = Request.CreateResponse(HttpStatusCode.Moved);
            response.Headers.Location = new Uri(invoiceUrl);
            return response;
        }


        private async Task<string> GetResponse(string url)
        {
            using (var client = new HttpClient())
            using (var response = await client.GetAsync(url))
            using (var content = response.Content)
            {
                return await content.ReadAsStringAsync();
            }
        }

        public HttpActionContext ValidateModel(object model, Type type)
        {
            var validator = Configuration.Services.GetBodyModelValidator();
            var metadataProvider = Configuration.Services.GetModelMetadataProvider();
            var actionContext = new HttpActionContext(ControllerContext, Request.GetActionDescriptor());

            validator.Validate(model, type, metadataProvider, actionContext, String.Empty);
            return actionContext;
        }
    }
}

/// <summary>
/// 
/// </summary>
public class SubscribeRequestModel
{
    /// <summary>
    /// 
    /// </summary>
    public string EmailAddress { get; set; }
}

/// <summary>
/// 
/// </summary>
public class ResponseMessage
{
    /// <summary>
    /// 
    /// </summary>
    public bool success { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public Error error { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public string message { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public List<DealerGetInfo> data { get; set; }
}

/// <summary>
/// 
/// </summary>
public class PricingResponseMessage
{
    /// <summary>
    /// 
    /// </summary>
    public bool success { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public Error error { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public string message { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public PricingInfo data { get; set; }
}

/// <summary>
/// 
/// </summary>
public class PricingInfo
{
    public string MLP { get; set; }
    public string DealerDeliveryFee { get; set; }
    public string LCT { get; set; }
    public object RegistrationFee { get; set; }
    public string StampDuty { get; set; }
    public string VehicleDriveAwayPrice { get; set; }
    public string ImagePath { get; set; }
    public string OptionsPrice { get; set; }
    public string AccessoryPrice { get; set; }
    public string Incentive { get; set; }
    public string HybridDiscount { get; set; }
    public string GeneralIssueNumberPlateFee { get; set; }
    public string VehicleTax { get; set; }
    public string TrafficImprovementFee { get; set; }
    public string RecordingFee { get; set; }
    public string PrescribedFlatFee { get; set; }
    public string EmergencyServicesLevy { get; set; }
    public string AdministrationFee { get; set; }
    public string RoadSafetyLevy { get; set; }
    public string RegistrationPlates { get; set; }
    public string MotorTax { get; set; }
    public string RegistrationAmount { get; set; }
    public string TACCharge { get; set; }
    public string TACGST { get; set; }
    public string TACDuty { get; set; }
    public string RiskZone { get; set; }
    public string TACAmount { get; set; }
    public string CTPFee { get; set; }
    public string CTPGST { get; set; }
    public string CTPDuty { get; set; }
    public string CTPAdminLevy { get; set; }
    public string CTPZone { get; set; }
    public string CTPAmount { get; set; }
    public string TIOCharge { get; set; }
    public string TIOGST { get; set; }
    public string TIOAmount { get; set; }
    public string LifetimeSupportFund { get; set; }
    public string FireLevy { get; set; }
    public string MAIBCharge { get; set; }
    public string MAIBGST { get; set; }
    public string MAIBAmount { get; set; }
    public string TotalRegistration { get; set; }
}

/// <summary>
/// 
/// </summary>
public class DealerResponseMessage
{
    /// <summary>
    /// 
    /// </summary>
    public bool success { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public Error error { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public string message { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public object data { get; set; }
}

/// <summary>
/// 
/// </summary>
public class Error
{
    /// <summary>
    /// 
    /// </summary>
    public int code { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public string description { get; set; }
}

