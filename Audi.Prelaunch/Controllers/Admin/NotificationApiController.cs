﻿using System;
using System.Net;
using System.Web.Http;
using Swashbuckle.Swagger.Annotations;
using log4net;
using System.Reflection;
using System.Web.Http.Cors;
using Audi.Prelaunch.Models;
using Audi.Prelaunch.Business.Models;
using Umbraco.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using AutoMapper;
using Umbraco.Web.Editors;
using Audi.Prelaunch.Business.Service;
using Audi.Prelaunch.Business.Entities;

namespace Audi.Prelaunch.Cms.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("notification")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [PluginController("Notification")]
    public class NotificationApiController : UmbracoAuthorizedJsonController
    {
        ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// returns notification list
        /// </summary>
        /// <returns></returns>
        [Route("")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(VehicleInfo), Description = "success")]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(GenericErrorModel), Description = "missing or invalid parameters")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(GenericErrorModel), Description = "unexpected error")]
        public IHttpActionResult GetBookingNotifications()
        {
            BookingNotificationService bookingNotificationService = new BookingNotificationService();

            try
            {
                var list = bookingNotificationService.GetList();
                return Ok(list);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Unable to return vehicle information, ERROR MESSAGE : {0}", ex.Message);
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// returns update notification list.
        /// </summary>
        /// <returns></returns>
        [Route("{id}/update")]
        [SwaggerResponse(HttpStatusCode.OK, Type = typeof(VehicleInfo), Description = "success")]
        [SwaggerResponse(HttpStatusCode.BadRequest, Type = typeof(GenericErrorModel), Description = "missing or invalid parameters")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, Type = typeof(GenericErrorModel), Description = "unexpected error")]
        public IHttpActionResult UpdateNotification()
        {
            BookingNotificationService bookingNotificationService = new BookingNotificationService();

            try
            {
                var list = bookingNotificationService.GetList();
                return Ok(list);
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("Unable to return vehicle information, ERROR MESSAGE : {0}", ex.Message);
                return InternalServerError(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemsPerPage"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <param name="searchTerm"></param>
        /// <returns></returns>
        public PagedBookingNotificationResult GetPaged(int itemsPerPage, int pageNumber, string sortColumn, string sortOrder, string searchTerm)
        {
            Mapper.CreateMap<BookingNotificationInfo, BookingNotificationGetInfo>()
                .ForMember(dest => dest.NotificationStatusType, opts => opts.MapFrom(src => Enum.Parse(typeof(NotificationStatusType), src.NotificationStatusType.ToString()).ToString()))
                .ForMember(dest => dest.CreateDate, opts => opts.MapFrom(src => src.CreateDate.ToString("dd/MM/yyyy hh:mm:ss tt"))); ;

            var items = new List<BookingInfo>();
            var db = DatabaseContext.Database;

            var currentType = typeof(BookingInfo);

            var query = new Sql().Select("*").From("audiBookingNotification");

            if (!string.IsNullOrEmpty(searchTerm))
            {
                int c = 0;
                foreach (var property in currentType.GetProperties())
                {
                    string before = "WHERE";
                    if (c > 0)
                    {
                        before = "OR";
                    }

                    var columnAttribute = property.GetCustomAttributes(typeof(ColumnAttribute), false);
                    var columnName = property.Name;

                    if (columnAttribute.Any())
                    {
                        columnName = ((ColumnAttribute)columnAttribute.FirstOrDefault()).Name;
                    }

                    query.Append(before + " [" + columnName + "] like @0", "%" + searchTerm + "%");
                    c++;
                }
            }
            if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortOrder))
                query.OrderBy(sortColumn + " " + sortOrder);
            else
            {
                query.OrderBy("createDate desc");
            }

            var p = db.Page<BookingNotificationInfo>(pageNumber, itemsPerPage, query);
            var transformItems = Queryable.AsQueryable(p.Items.AsEnumerable().Select(Mapper.Map<BookingNotificationGetInfo>)).ToList();

            var result = new PagedBookingNotificationResult
            {
                TotalPages = p.TotalPages,
                TotalItems = p.TotalItems,
                ItemsPerPage = p.ItemsPerPage,
                CurrentPage = p.CurrentPage,
                BookingNotification = transformItems
            };

            return result;
        }
    }
}

