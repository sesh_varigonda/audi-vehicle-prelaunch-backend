﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using Umbraco.Web.Mvc;
using Umbraco.Web.Editors;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.SqlSyntax;
using AutoMapper;
using System;
using System.Configuration;
using System.Net.Http.Headers;
using System.Net.Http;
using Newtonsoft.Json;
using Audi.Prelaunch.Business.Entities;
using Audi.Prelaunch.Business.Models;
using Audi.Prelaunch.Business.Enum;
using Audi.Prelaunch.Business.Service;

namespace Audi.Prelaunch.Cms.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("booking")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [PluginController("Booking")]
    public class BookingApiController : UmbracoAuthorizedJsonController
    {
        private static readonly ISqlSyntaxProvider SqlSyntaxProvider = ApplicationContext.Current.DatabaseContext.SqlSyntax;
        private static readonly Database Database = ApplicationContext.Current.DatabaseContext.Database;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<BookingInfo> GetAll()
        {
            return Enumerable.Empty<BookingInfo>();
        }

        /// <summary>
        /// returns list of all the notifications
        /// </summary>
        /// <returns></returns>
        public List<BookingNotificationInfo> GetList()
        {
            var query = new Sql().Select("*").From<BookingNotificationInfo>(SqlSyntaxProvider);
            var bookingNotifications = Database.Fetch<BookingNotificationInfo>(query).Where(a => a.NotificationStatusType == (int)Prelaunch.Business.Enum.NotificationStatusType.Queued).ToList();

            return bookingNotifications;
        }

        /// <summary>
        /// Returns Booking By Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public BookingGetInfo GetById(string id)
        {
            Mapper.CreateMap<BookingInfo, BookingGetInfo>()
                .ForMember(dest => dest.Status, opts => opts.MapFrom(src => Enum.Parse(typeof(BookingStatusType), src.Status.ToString()).ToString()));

            var query = new Sql().Select("*").From<BookingInfo>(SqlSyntaxProvider);
            var bookingInfo = Database.Fetch<BookingInfo>(query).Where(a => a.BookingNumber == id).FirstOrDefault();

            var tradeVehicleQuery = new Sql().Select("*").From<TradeVehicleInfo>(SqlSyntaxProvider);
            var tradeVehicleInfo = Database.Fetch<TradeVehicleInfo>(tradeVehicleQuery).Where(a => a.TradeVehicleId == bookingInfo.TradeVehicleId).FirstOrDefault();

            var booking = Mapper.Map<BookingGetInfo>(bookingInfo);

            //Setting UserProfile
            var userProfile = GetUserProfileById(booking.UserId);

            if (userProfile != null)
            {
                booking.FirstName = userProfile.FirstName;
                booking.LastName = userProfile.LastName;
                booking.EmailAddress = userProfile.Email;
                booking.MobilePhone = userProfile.Mobile;
            }

            //Setting VehicleOptions
            ModelService modelService = new ModelService();
            var vehicleInfo = modelService.GetList();

            var vehicleExteriorInfo = vehicleInfo.exteriors.Where(a => a.Id == bookingInfo.ExteriorColourId).FirstOrDefault();
            if (vehicleExteriorInfo != null)
            {
                booking.VehicleExterior = new VehicleExteriorGetInfo() { Name = vehicleExteriorInfo.Name };
            }

            var vehicleInteriorInfo = vehicleInfo.interiors.Where(a => a.Id == bookingInfo.InteriorColourId).FirstOrDefault();
            if (vehicleInteriorInfo != null)
            {
                booking.VehicleInterior = new VehicleInteriorGetInfo() { Name = vehicleInteriorInfo.Name };
            }

            booking.TradeInVehicle = new OrderTradeVehicleInfo()
            {
                Make = tradeVehicleInfo.Make,
                Model = tradeVehicleInfo.Model,
                Year = tradeVehicleInfo.Year,
                Kilometers = tradeVehicleInfo.Kilometers,
                hasFinance = tradeVehicleInfo.HasFinance,
            };

            //Setting Dealer Details
            var dealerInfo = GetDealerById(booking.DealerId);
            if (dealerInfo != null)
            {
                booking.Dealer = new OrderDealerGetInfo() { Name = dealerInfo.Name, Address = dealerInfo.Address, Suburb = dealerInfo.Suburb, Postcode = dealerInfo.Postcode, State = dealerInfo.State };
            }

            return booking;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dealerId"></param>
        /// <returns></returns>
        public OrderDealerInfo GetDealerById(int dealerId)
        {
            string result = string.Empty;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["ApiCMSInternalBaseUrl"]);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync(string.Format("/2/legacy/dealers/model/{0}", "Prelaunch")).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsStringAsync().Result;
                }
            }

            var dealers = JsonConvert.DeserializeObject<OrderDealerResponseMessage>(result);

            return dealers.data.Where(a => a.ID == dealerId).FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public UserProfileGetModel GetUserProfileById(string userId)
        {
            string result = string.Empty;

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["ApiMyAudiBaseUrl"]);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = client.GetAsync(string.Format("/1/account/userprofile/{0}", userId)).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsStringAsync().Result;
                }
            }

            var userProfile = JsonConvert.DeserializeObject<UserProfileGetModel>(result);

            return userProfile;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="itemsPerPage"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortColumn"></param>
        /// <param name="sortOrder"></param>
        /// <param name="searchTerm"></param>
        /// <returns></returns>
        public PagedBookingResult GetPaged(int itemsPerPage, int pageNumber, string sortColumn,
            string sortOrder, string searchTerm)
        {
            Mapper.CreateMap<BookingInfo, BookingGetInfo>()
                .ForMember(dest => dest.Status, opts => opts.MapFrom(src => Enum.Parse(typeof(BookingStatusType), src.Status.ToString()).ToString()));

            var items = new List<BookingInfo>();
            var db = DatabaseContext.Database;

            var currentType = typeof(BookingInfo);

            var query = new Sql().Select("*").From("audiBooking");

            if (!string.IsNullOrEmpty(searchTerm))
            {
                int c = 0;
                foreach (var property in currentType.GetProperties())
                {
                    string before = "WHERE";
                    if (c > 0)
                    {
                        before = "OR";
                    }

                    var columnAttribute = property.GetCustomAttributes(typeof(ColumnAttribute), false);
                    var columnName = property.Name;

                    if (columnAttribute.Any())
                    {
                        columnName = ((ColumnAttribute)columnAttribute.FirstOrDefault()).Name;
                    }

                    query.Append(before + " [" + columnName + "] like @0", "%" + searchTerm + "%");
                    c++;
                }
            }
            if (!string.IsNullOrEmpty(sortColumn) && !string.IsNullOrEmpty(sortOrder))
                query.OrderBy(sortColumn + " " + sortOrder);
            else
            {
                query.OrderBy("bookingId asc");
            }

            var p = db.Page<BookingInfo>(pageNumber, itemsPerPage, query);
            var transformItems = Queryable.AsQueryable(p.Items.AsEnumerable().Select(Mapper.Map<BookingGetInfo>)).ToList();

            var result = new PagedBookingResult
            {
                TotalPages = p.TotalPages,
                TotalItems = p.TotalItems,
                ItemsPerPage = p.ItemsPerPage,
                CurrentPage = p.CurrentPage,
                Booking = transformItems
            };

            return result;
        }
    }
}


