﻿using Swashbuckle.Swagger.Annotations;
using System;
using System.Net;
using System.Web.Http;
using Umbraco.Web.WebApi;

namespace Audi.Prelaunch.Cms.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class PingController : UmbracoApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [SwaggerResponse(HttpStatusCode.OK, Description = "success")]
        public IHttpActionResult GetPing()
        {
            try
            {
                return Ok();
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}