﻿angular.module("umbraco").controller("PackagePickerController",
        function ($scope, $http) {
            $scope.loading = true;
            $http({ method: 'GET', url: '/services/packages' })
                .success(function (data) {
                    $scope.packages = data;
                    $scope.loading = false;
                })
                .error(function () {
                    $scope.error = "An Error has occured while loading!";
                    $scope.loading = false;
                });
        });