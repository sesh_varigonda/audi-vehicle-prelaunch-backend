﻿angular.module("umbraco").controller("DealerPickerController",
        function ($scope, $http) {
            $scope.loading = true;
            $http({ method: 'GET', url: '/services/dealers' })
                .success(function (data) {
                    $scope.dealers = data;
                    $scope.loading = false;
                })
                .error(function () {
                    $scope.error = "An Error has occured while loading!";
                    $scope.loading = false;
                });
        });