﻿angular.module("umbraco.resources")
	.factory("notificationResource", function ($http) {
	    return {
	        getById: function (id) {
	            return $http.get("backoffice/Notification/NotificationApi/GetById?id=" + id);
	        },
            getPaged: function (itemsPerPage, pageNumber, sortColumn, sortOrder, searchTerm) {
                if (sortColumn == undefined)
                    sortColumn = "";
                if (sortOrder == undefined)
                    sortOrder = "";
                return $http.get("backoffice/Notification/NotificationApi/" + "GetPaged?itemsPerPage=" + itemsPerPage + "&pageNumber=" + pageNumber + "&sortColumn=" + sortColumn + "&sortOrder=" + sortOrder + "&searchTerm=" + searchTerm);
            },
	        exportResults: function () {
	            return $http.get("backoffice/Notification/NotificationApi/getNotificationsExport", {
	                headers: { 'Accept': 'text/csv' }
	            });
	        }
	    };
	});