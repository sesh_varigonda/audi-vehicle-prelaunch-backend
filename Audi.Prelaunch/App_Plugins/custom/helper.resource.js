﻿angular.module("umbraco.resources")
	.factory("helperResource", function ($http) {

	    var attributeType_Object = 3;
	    var propertyView_Object = 'object';

	    return {
	        getAttributesForModel: function (model) {
	            
	            var properties = [];

	            angular.forEach(model.attributes, function (attribute, key) {

	                if (attribute.datatype == attributeType_Object) {

	                    var attributes = [];

	                    var schemaData = attribute.properties[0].value;
	                    var jsonObject = angular.fromJson(schemaData);

	                    for (key in jsonObject.properties) {
	                        attributes.push({
	                            label: jsonObject.properties[key].field,
	                            value: jsonObject.properties[key].value,
	                            description: jsonObject.properties[key].description,
	                            hideLabel: false,
	                            view: getViewByType(jsonObject.properties[key].type),
	                            config: jsonObject.properties[key].config,
	                            type: jsonObject.properties[key].type,
	                            isVisible: true,
	                            isDatabaseProperty: false
	                        });
	                    }

	                    attribute.properties = attributes;
	                }

                    //Added functionality to convert JsonString to an object(Caused parsing issues when not done for Dropdownlist.)
	                if (attribute.properties[0].config != null && attribute.properties[0].config != '') {
	                    attribute.properties[0].config = angular.fromJson(attribute.properties[0].config);
	                }
	            });

	            return model;
	        },

	        saveAttributesInModel: function (model) {
	            var properties = [];
	            angular.forEach(model.attributes, function (attribute, key) {
	                if (attribute.datatype == attributeType_Object) {

	                    var propertyList = []
	                    var schemaData = {};

	                    angular.forEach(attribute.properties, function (property, key) {
	                        if (property.view != propertyView_Object) {
	                            propertyList.push({ field: property.label, value: property.value })
	                        }
	                    });

	                    schemaData.name = attribute.name;
	                    schemaData.properties = propertyList;

	                    attribute.properties.push({
	                        label: attribute.name,
	                        value: angular.toJson(schemaData),
	                        description: attribute.description,
	                        hideLabel: false,
	                        view: propertyView_Object,
	                        isVisible: false,
	                        isDatabaseProperty: true
	                    });
	                }
	            });

	            return model;
	        },

	        addAttributesToModel: function (attributeInfo) {

	            var schemaData = attributeInfo.schema;
	            var propertyList = [];
	            var attribute = {};

	            if (schemaData != null && schemaData != '') {

	                var jsonObject = angular.fromJson(schemaData);
	                for (key in jsonObject.properties) {
	                    propertyList.push({
	                        label: jsonObject.properties[key].field,
	                        value: jsonObject.properties[key].value,
	                        description: jsonObject.properties[key].description,
	                        hideLabel: false,
	                        view: getViewByType(jsonObject.properties[key].type),
	                        type: jsonObject.properties[key].type,
	                        config: jsonObject.properties[key].config,
	                        isVisible: true,
	                        isDatabaseProperty: false
	                    });
	                }

	                attribute = {
	                    id: attributeInfo.attributeId,
	                    name: attributeInfo.groupName,
	                    datatype: attributeInfo.dataType,
                        config: attributeInfo.config,
                        properties: propertyList,
                        iscustom: true
	                };

	            }
	            else {

	                propertyList.push({
	                    label: attributeInfo.attributeCode,
	                    value: '',
	                    description: attributeInfo.description,
	                    hideLabel: false,
                        config: attributeInfo.config,
	                    view: getViewByType(attributeInfo.dataType),
	                    isVisible: true,
	                    isDatabaseProperty: true
	                });

	                attribute = { id: attributeInfo.attributeId, datatype: attributeInfo.dataType, properties: propertyList, iscustom: true };
	            }

	            return attribute;
	        }
	    };

	    function getViewByType(type) {
	        if (type == 'string' || type == 0) {
	            return 'textbox';
	        }
	        if (type == 'boolean' || type == 2) {
	            return 'boolean';
	        }

	        return 'textbox';
	    }
	});