﻿angular.module('umbraco')
    .controller('Notification.OverviewController', function ($scope, notificationResource, notificationsService) {

        $scope.selectedIds = [];

        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.totalPages = 1;

        $scope.reverse = false;

        $scope.searchTerm = "";
        $scope.predicate = 'bookingNotificationId';

        function fetchData() {
            notificationResource.getPaged($scope.itemsPerPage, $scope.currentPage, $scope.predicate, $scope.reverse ? "desc" : "asc", $scope.searchTerm).then(function (response) {
                $scope.notifications = response.data.bookingNotification;
                $scope.totalPages = response.data.totalPages;
            }, function (response) {
                notificationsService.error("Error", "Could not load notifications");
            });
        };

        $scope.order = function (predicate) {
            $scope.reverse = ($scope.predicate === predicate) ? !$scope.reverse : false;
            $scope.predicate = predicate;
            $scope.currentPage = 1;
            fetchData();
        };

        $scope.toggleSelection = function (val) {
            var idx = $scope.selectedIds.indexOf(val);
            if (idx > -1) {
                $scope.selectedIds.splice(idx, 1);
            } else {
                $scope.selectedIds.push(val);
            }
        };

        $scope.isRowSelected = function (id) {
            return $scope.selectedIds.indexOf(id) > -1;
        };

        $scope.isAnythingSelected = function () {
            return $scope.selectedIds.length > 0;
        };

        $scope.getNotificationStatusClass = function (status) {
            if (status == "Processed") {
                return "umb-badge umb-badge--success umb-badge--xs";
            }
            else if (status == "Failed") {
                return "umb-badge umb-badge--danger umb-badge--xs";
            }
            else if (status == "Queued") {
                return "umb-badge umb-badge--grey umb-badge--xs";
            }
            else {
                return "umb-badge umb-badge--warning umb-badge--xs";
            }
        };

        $scope.getNotificationCodeClass = function (status) {
            if (status == "BookingConfirmation") {
                return "umb-badge umb-badge--success umb-badge--xs";
            }
            else if (status == "SaveOptions") {
                return "umb-badge umb-badge--warning umb-badge--xs";
            }
            else {
                return "umb-badge umb-badge--success umb-badge--xs";
            }
        };

        $scope.prevPage = function () {
            if ($scope.currentPage > 1) {
                $scope.currentPage--;
                fetchData();
            }
        };

        $scope.nextPage = function () {
            if ($scope.currentPage < $scope.totalPages) {
                $scope.currentPage++;
                fetchData();
            }
        };

        $scope.setPage = function (pageNumber) {
            $scope.currentPage = pageNumber;
            fetchData();
        };

        $scope.search = function (searchFilter) {
            $scope.searchTerm = searchFilter;
            $scope.currentPage = 1;
            fetchData();
        };

        fetchData();
    });