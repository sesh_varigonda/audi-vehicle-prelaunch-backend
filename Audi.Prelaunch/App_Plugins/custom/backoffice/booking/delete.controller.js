﻿angular.module("umbraco")
.controller("Booking.DeleteController",
	function ($scope, bookingResource, navigationService,treeService) {
	    $scope.delete = function (id) {
            bookingResource.deleteById(id).then(function () {
	            treeService.removeNode($scope.currentNode);
	            navigationService.hideNavigation();
	            
	        });
	    };

	    $scope.cancelDelete = function () {
	        navigationService.hideNavigation();
	    };
	});