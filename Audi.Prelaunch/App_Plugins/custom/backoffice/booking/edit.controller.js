﻿angular.module("umbraco").controller("Booking.EditController",
	function ($scope, $routeParams, bookingResource, helperResource, notificationsService, navigationService, dialogService, $route) {

	    $scope.loaded = false;
	    $scope.propertyName = '';
	    $scope.propertyValue = '';

        bookingResource.getById($routeParams.id).then(function (response) {
            $scope.booking = response.data;
            $scope.loaded = true;
        });

        $scope.content = { tabs: [{ id: 1, label: "Content" }] };

	    $scope.save = function (dealer) {

	        bookingResource.save(helperResource.saveAttributesInModel(dealer)).then(function (response) {
                $scope.booking = helperResource.getAttributesForModel(response.data);
	            $scope.dealerForm.$dirty = false;
	            navigationService.syncTree({ tree: 'bookingTree', path: [-1, -1], forceReload: true });
	            notificationsService.success("Success", dealer.name + " has been saved");
	            $scope.loaded = true;
	        });
	    };

	    $scope.delete = function (attribute, $index) {

	        if (confirm('Are you sure?'))
	        {
	            if (attribute.objectid === undefined) {
	                $scope.dealer.attributes.splice($index, 1);
	                $route.reload();
	            }
	            else {
	                bookingResource.deleteAttributeById(attribute.objectid).then(function (response) {
	                    $scope.dealer = helperResource.getAttributesForModel(response.data);
	                    $scope.dealerForm.$dirty = false;
	                    navigationService.syncTree({ tree: 'dealerTree', path: [-1, -1], forceReload: true });
	                    notificationsService.success("Success", "Attribute has been deleted.");
	                });
	            }
	        }
        };

        $scope.getStatusClass = function (status) {
            if (status == "Paid") {
                return "umb-badge umb-badge--success umb-badge--xs";
            }
            else if (status == "Pending") {
                return "umb-badge umb-badge--warning umb-badge--xs";
            }
            else {
                return "umb-badge umb-badge--danger umb-badge--xs";
            }
        };

	    var dialog;
	    $scope.openDialog = function () {
	        var dialogData = { attributeList: $scope.attributeList };
	        var templateUrl = '/App_Plugins/attribute/attribute.html';

	        var options = { template: templateUrl, show: true, dialogData: dialogData, callback: populateAttribute, closeCallback: closeDialog };

	        dialog = dialogService.open(options);
	    };

	    function closeDialog() {
            //On closing the dialog, do something
	    }

	    function populateAttribute(data) {
	        addRow(data);
	    }

	    function addRow(attributeData) {

	        $scope.dealer.attributes.push(helperResource.addAttributesToModel(attributeData));
	        notificationsService.success("Success", "Attribute " + attributeData.attributeCode + " has been successfully added.");

	    };
	});