﻿angular.module("umbraco").controller("Booking.ExportController",
	function ($scope, $routeParams, bookingResource, notificationsService, navigationService) {

	    $scope.exportResults = function () {

            bookingResource.exportResults().then(function (response) {
	            window.open("backoffice/booking/bookingApi/getDealersExport", "_blank");
	            notificationsService.success("Success", "Booking Information has been successfully exported.");
	        });
	    };

	    $scope.cancelDelete = function () {
	        navigationService.hideNavigation();
	    };
	});