﻿angular.module("umbraco.resources")
	.factory("bookingResource", function ($http) {
	    return {
	        getById: function (id) {
	            return $http.get("backoffice/Booking/BookingApi/GetById?id=" + id);
	        },
	        save: function (booking) {
	            return $http.post("backoffice/Booking/BookingApi/PostSave", angular.toJson(booking));
	        },
	        deleteById: function (id) {
	            return $http.delete("backoffice/Booking/BookingApi/DeleteById?id=" + id);
	        },
	        deleteAttributeById: function (id) {
	            return $http.delete("backoffice/Booking/BookingApi/DeleteAttributeById?id=" + id);
	        },
	        getDefaultAttributes: function () {
	            return $http.get("backoffice/Booking/BookingApi/GetDefaultBooking");
            },
            getPaged: function (itemsPerPage, pageNumber, sortColumn, sortOrder, searchTerm) {
                if (sortColumn == undefined)
                    sortColumn = "";
                if (sortOrder == undefined)
                    sortOrder = "";
                return $http.get("backoffice/Booking/BookingApi/" + "GetPaged?itemsPerPage=" + itemsPerPage + "&pageNumber=" + pageNumber + "&sortColumn=" + sortColumn + "&sortOrder=" + sortOrder + "&searchTerm=" + searchTerm);
            },
	        exportResults: function () {
	            return $http.get("backoffice/booking/bookingApi/getBookingsExport", {
	                headers: { 'Accept': 'text/csv' }
	            });
	        }
	    };
	});