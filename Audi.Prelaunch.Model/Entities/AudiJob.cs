﻿using System;
using System.Runtime.Serialization;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Audi.Prelaunch.Business.Entities
{
    /// <summary>
    /// 
    /// </summary>
    [TableName("audiJob")]
    [DataContract(Name = "jobInfo")]
    [PrimaryKey("jobId", autoIncrement = true)]
    public class JobInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public JobInfo() { }

        /// <summary>
        /// 
        /// </summary>
        [PrimaryKeyColumn(AutoIncrement = true)]
        [DataMember(Name = "jobId")]
        public int JobId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "filename")]
        public string Filename { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "fileStatus")]
        public string FileStatus { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "message")]
        [NullSetting(NullSetting = NullSettings.Null)]
        public string Message { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "createDate")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "updateDate")]
        [NullSetting(NullSetting = NullSettings.Null)]
        public DateTime? UpdateDate { get; set; }
    }
}