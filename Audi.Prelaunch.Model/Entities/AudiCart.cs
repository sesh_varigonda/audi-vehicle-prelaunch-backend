﻿using System;
using System.Runtime.Serialization;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Audi.Prelaunch.Business.Entities
{
    /// <summary>
    /// 
    /// </summary>
    [TableName("audiCart")]
    [DataContract(Name = "cartInfo")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class CartInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public CartInfo() { }

        /// <summary>
        /// 
        /// </summary>
        [PrimaryKeyColumn(AutoIncrement = true)]
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "cartId")]
        public string CartId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "emailAddress")]
        public string EmailAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "jsonData")]
        [Length(4000)]
        public string JsonData { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "createDate")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "createSource")]
        [NullSetting(NullSetting = NullSettings.Null)]
        public string CreateSource { get; set; }
    }
}