﻿using System;
using System.Runtime.Serialization;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Audi.Prelaunch.Business.Entities
{
    /// <summary>
    /// 
    /// </summary>
    [TableName("audiNotification")]
    [DataContract(Name = "notificationInfo")]
    [PrimaryKey("notificationId", autoIncrement = true)]
    public class NotificationInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public NotificationInfo() { }

        /// <summary>
        /// 
        /// </summary>
        [PrimaryKeyColumn(AutoIncrement = true)]
        [DataMember(Name = "notificationId")]
        public int NotificationId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "notificationCode")]
        public string NotificationCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "source")]
        public string Source { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "description")]
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "type")]
        public string Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "externalId")]
        public int ExternalId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "status")]
        public int Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "createDate")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "createSource")]
        public string CreateSource { get; set; }
    }
}