﻿using System;
using System.Runtime.Serialization;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Audi.Prelaunch.Business.Entities
{
    /// <summary>
    /// 
    /// </summary>
    [TableName("audiTradeVehicle")]
    [DataContract(Name = "tradeVehicleInfo")]
    [PrimaryKey("tradeVehicleId", autoIncrement = true)]
    public class TradeVehicleInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public TradeVehicleInfo() { }

        /// <summary>
        /// 
        /// </summary>
        [PrimaryKeyColumn(AutoIncrement = true)]
        [DataMember(Name = "tradeVehicleId")]
        public int TradeVehicleId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "make")]
        public string Make { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "model")]
        public string Model { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "year")]
        public string Year { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "kilometers")]
        public string Kilometers { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "hasFinance")]
        public bool HasFinance { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "createDate")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "createSource")]
        public string CreateSource { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "updateDate")]
        public DateTime UpdateDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "updateSource")]
        public string UpdateSource { get; set; }
    }
}