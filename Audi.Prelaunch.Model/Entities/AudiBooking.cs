﻿using System;
using System.Runtime.Serialization;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Audi.Prelaunch.Business.Entities
{
    /// <summary>
    /// 
    /// </summary>
    [TableName("audiBooking")]
    [DataContract(Name = "bookingInfo")]
    [PrimaryKey("bookingId", autoIncrement = true)]
    public class BookingInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public BookingInfo() { }

        /// <summary>
        /// 
        /// </summary>
        [PrimaryKeyColumn(AutoIncrement = true)]
        [DataMember(Name = "bookingId")]
        public int BookingId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "bookingNumber")]
        public string BookingNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "userId")]
        public string UserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "email")]
        public string EmailAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "dealerCode")]
        public string DealerCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "exteriorColourId")]
        public string ExteriorColourId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "interiorColourId")]
        public string InteriorColourId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "packages")]
        public string Packages { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "chargeToken")]
        public string ChargeToken { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "chargeIdentifier")]
        public string ChargeIdentifier { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "totalAmount")]
        public int TotalAmount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "description")]
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "tradeIn")]
        public bool TradeIn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "tradeVehicleId")]
        public int TradeVehicleId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "status")]
        public int Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "contactCharging")]
        public bool ContactCharging { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "contactFinance")]
        public bool ContactFinance { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "commissionNumber")]
        public string CommissionNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "idsCode")]
        public string IDSCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "createDate")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "createSource")]
        public string CreateSource { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [NullSetting(NullSetting = NullSettings.Null)]
        [DataMember(Name = "updateDate")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "updateSource")]
        [NullSetting(NullSetting = NullSettings.Null)]
        public string UpdateSource { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Ignore()]
        [DataMember(Name = "tradeVehicle")]
        public TradeVehicleInfo TradeVehicle { get; set; }
    }
}