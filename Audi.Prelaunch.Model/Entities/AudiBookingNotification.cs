﻿using System;
using System.Runtime.Serialization;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Audi.Prelaunch.Business.Entities
{
    /// <summary>
    /// 
    /// </summary>
    [TableName("audiBookingNotification")]
    [DataContract(Name = "bookingNotificationInfo")]
    [PrimaryKey("bookingNotificationId", autoIncrement = true)]
    public class BookingNotificationInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public BookingNotificationInfo() { }

        /// <summary>
        /// 
        /// </summary>
        [PrimaryKeyColumn(AutoIncrement = true)]
        [DataMember(Name = "bookingNotificationId")]
        public int BookingNotificationId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "bookingId")]
        public int BookingId { get; set; }


        [DataMember(Name = "notificationCode")]
        public string NotificationCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "content")]
        [Length(4000)]
        public string Content { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "notificationStatusType")]
        public int NotificationStatusType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "createDate")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "createSource")]
        public string CreateSource { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "updateDate")]
        [NullSetting(NullSetting = NullSettings.Null)]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "updateSource")]
        [NullSetting(NullSetting = NullSettings.Null)]
        public string UpdateSource { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Ignore()]
        [DataMember(Name = "notification")]
        public NotificationInfo Notification { get; set; }
    }
}