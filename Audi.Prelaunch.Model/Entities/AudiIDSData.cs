﻿using System;
using System.Runtime.Serialization;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Audi.Prelaunch.Business.Entities
{
    /// <summary>
    /// 
    /// </summary>
    [TableName("audiIDSData")]
    [DataContract(Name = "IDSDataInfo")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class IDSDataInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public IDSDataInfo() { }

        /// <summary>
        /// 
        /// </summary>
        [PrimaryKeyColumn(AutoIncrement = true)]
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "jobId")]
        public int JobId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "commissionNumber")]
        public string CommissionNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "idsCode")]
        public string IDSCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "dealerCode")]
        public string DealerCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "dealerName")]
        public string DealerName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "email")]
        public string Email { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "createDate")]
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "createSource")]
        public string CreateSource { get; set; }
    }
}