﻿using System.Runtime.Serialization;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.DatabaseAnnotations;

namespace Audi.Prelaunch.Business.Entities
{
    /// <summary>
    /// 
    /// </summary>
    [TableName("audiPaintCode")]
    [DataContract(Name = "paintCodeInfo")]
    [PrimaryKey("Id", autoIncrement = true)]
    public class PaintCodeInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public PaintCodeInfo() { }

        /// <summary>
        /// 
        /// </summary>
        [PrimaryKeyColumn(AutoIncrement = true)]
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "code")]
        public string Code { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "description")]
        public string Description { get; set; }
    }
}