﻿namespace Audi.Prelaunch.Business.Enum
{
    public enum NotificationStatusType
    {
        Queued,
        Processing,
        Processed,
        Failed
    }
}
