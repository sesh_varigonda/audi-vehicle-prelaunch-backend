﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audi.Prelaunch.Business.Enum
{
    /// <summary>
    /// 
    /// </summary>
    public enum FileStatusType
    {
        /// <summary>
        /// 
        /// </summary>
        Queued,

        /// <summary>
        /// 
        /// </summary>
        Processing,

        /// <summary>
        /// 
        /// </summary>
        Processed,

        /// <summary>
        /// 
        /// </summary>
        Failed
    }
}
