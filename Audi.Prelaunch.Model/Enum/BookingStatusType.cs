﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audi.Prelaunch.Business.Enum
{
    /// <summary>
    /// 
    /// </summary>
    public enum BookingStatusType
    {
        /// <summary>
        /// 
        /// </summary>
        Pending,

        /// <summary>
        /// 
        /// </summary>
        Paid,

        /// <summary>
        /// 
        /// </summary>
        Cancelled
    }
}
