﻿using Audi.Prelaunch.Business.Processors;
using Our.Umbraco.Ditto;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Audi.Prelaunch.Models
{
    #region Common Classes

    /// <summary>
    /// 
    /// </summary>
    public class GenericErrorModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string Message { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ModelErrorModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ModelStateErrorModel ModelState { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ModelStateErrorModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string[] PropertyName { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class NameValuePair
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Value { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ContentInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Alias { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, object> Properties { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<ContentInfo> Children { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool isPublished { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Url { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ImageInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string Src { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Width { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Height { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Alt { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class MediaInfo
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string Url { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("umbracoAltText")]
        public string AltText { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("umbracoWidth")]
        public string Width { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("umbracoHeight")]
        public string Height { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string X { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Y { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class HomeInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("urlName")]
        [SlugProcessor(Order = 1)]
        public string Slug { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SubTitle { get; set; }
    }

    #endregion

    #region SaveOptions


    /// <summary>
    /// 
    /// </summary>
    public class SaveOptionInfo
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string EmailAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string JsonData { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class CartGetInfo
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string EmailAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string JsonData { get; set; }
    }

    #endregion

    #region DriveAwayPrice

    /// <summary>
    /// 
    /// </summary>
    public class DriveAwayPriceRequestModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string PackageId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int DealerId { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DriveAwayPriceResponseModel
    {
        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("dealer")]
        [DealerJsonProcessor(Order = 1)]
        public DriveAwayDealerGetInfo Dealer { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("package")]
        [PackageJsonProcessor(Order = 2)]
        public PackageGetInfo Package { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int PackagePrice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int DealerListedPrice { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DriveAwayDealerGetInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string State { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DealerGetInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DealerBusinessManagerEmailAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DealerPrincipalEmailAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DealerCode { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PackageGetInfo
    {
        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("key")]
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
    }

    #endregion

    #region Prelaunch


    /// <summary>
    /// 
    /// </summary>
    public class DealerInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public int DealerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DealerPackageResponseModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Price { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DealerPriceResponseModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int BasePrice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<DealerPackageResponseModel> Packages { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ResponseMessage
    {
        /// <summary>
        /// 
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Limit { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public object Data { get; set; }
    }


    #endregion

    #region FAQ

    /// <summary>
    /// 
    /// </summary>
    public class FAQ
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("faqs")]
        [FAQProcessor(Order = 2)]
        public List<FAQInfo> Items { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class FAQInfo
    {
        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("key")]
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("category")]
        [SlugProcessor(Order = 1)]
        public string Category { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Question { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Answer { get; set; }
    }

    public class CategoryInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Slug { get; set; }
    }


    /// <summary>
    /// 
    /// </summary>
    public class FAQCollection
    {
        /// <summary>
        /// 
        /// </summary>
        public List<CategoryInfo> categories { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<FAQInfo> questions { get; set; }
    }

    #endregion
}
