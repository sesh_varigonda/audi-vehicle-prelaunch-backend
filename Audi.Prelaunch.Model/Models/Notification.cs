﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Audi.Prelaunch.Business.Models
{
    /// <summary>
    /// 
    /// </summary>
    public enum NotificationStatusType
    {
        /// <summary>
        /// 
        /// </summary>
        Queued,

        /// <summary>
        /// 
        /// </summary>
        Processing,

        /// <summary>
        /// 
        /// </summary>
        Processed,

        /// <summary>
        /// 
        /// </summary>
        Failed
    }

    /// <summary>
    /// 
    /// </summary>
    public enum RecordStatusType
    {
        /// <summary>
        /// 
        /// </summary>
        Disabled,

        /// <summary>
        /// 
        /// </summary>
        Active
    }
}