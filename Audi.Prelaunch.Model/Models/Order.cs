﻿using Audi.Prelaunch.Business.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Audi.Prelaunch.Business.Models
{
    #region Booking

    /// <summary>
    /// 
    /// </summary>
    public class BookingDeliveryAddressInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Suburb { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Postcode { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class OrderRequestModel
    {
        /// <summary>
        /// 
        /// </summary>
        public OrderOptionsRequestModel options { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public OrderCustomerRequestModel customer { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public OrderPaymentRequestModel payment { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public OrderTradeInRequestModel tradeInDetails { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public bool TradeIn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public bool ContactFinance { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public bool ContactCharging { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string DealerCode { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class OrderOptionsRequestModel
    {
        /// <summary>
        /// 
        /// </summary>
        public List<OrderPackageRequestModel> Packages { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public OrderExteriorColourRequestModel ExteriorColor { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public OrderInteriorColourRequestModel InteriorColor { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class OrderPackageRequestModel
    {
        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "Package Id is Required Field.")]
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "Package Name is Required Field.")]
        public string Name { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class OrderExteriorColourRequestModel
    {
        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "ExteriorColor Id is Required Field.")]
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "ExteriorColor Name is Required Field.")]
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "ExteriorColor Code is Required Field.")]
        public string Code { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class OrderInteriorColourRequestModel
    {
        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "InteriorColour Id is Required Field.")]
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "InteriorColour Name is Required Field.")]
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "InteriorColour Code is Required Field.")]
        public string Code { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class OrderPaymentRequestModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string PaymentType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string ChargeToken { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public int Amount { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class OrderTradeInRequestModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string Make { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Year { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Kilometers { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool HasFinance { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class OrderCustomerRequestModel
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string UserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "Customer Firstname is Required Field.")]
        public string FirstName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "Customer Lastname is Required Field.")]
        public string LastName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "Customer Email is Required Field.")]
        public string Email { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "Customer Mobile is Required Field.")]
        public string Mobile { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "Customer Postcode is Required Field.")]
        public string HomePostcode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "Customer Address is Required Field.")]
        public string HomeAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "Customer Suburb is Required Field.")]
        public string HomeSuburb { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required(ErrorMessage = "Customer State is Required Field.")]
        public string HomeState { get; set; }
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    [DataContract(Name = "pagedBooking", Namespace = "")]
    public class PagedBookingResult
    {
        [DataMember(Name = "booking")]
        public List<BookingGetInfo> Booking { get; set; }

        [DataMember(Name = "currentPage")]
        public long CurrentPage { get; set; }

        [DataMember(Name = "itemsPerPage")]
        public long ItemsPerPage { get; set; }

        [DataMember(Name = "totalPages")]
        public long TotalPages { get; set; }

        [DataMember(Name = "totalItems")]
        public long TotalItems { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    [DataContract(Name = "pagedBookingNotification", Namespace = "")]
    public class PagedBookingNotificationResult
    {
        [DataMember(Name = "bookingNotification")]
        public List<BookingNotificationGetInfo> BookingNotification { get; set; }

        [DataMember(Name = "currentPage")]
        public long CurrentPage { get; set; }

        [DataMember(Name = "itemsPerPage")]
        public long ItemsPerPage { get; set; }

        [DataMember(Name = "totalPages")]
        public long TotalPages { get; set; }

        [DataMember(Name = "totalItems")]
        public long TotalItems { get; set; }
    }

    [DataContract]
    public class BookingNotificationGetInfo
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "bookingNotificationId")]
        public int BookingNotificationId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "bookingId")]
        public int BookingId { get; set; }


        [DataMember(Name = "notificationCode")]
        public string NotificationCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "content")]
        public string Content { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "status")]
        public string NotificationStatusType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "createDate")]
        public string CreateDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "createSource")]
        public string CreateSource { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "updateDate")]
        public DateTime? UpdateDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "updateSource")]
        public string UpdateSource { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "notification")]
        public NotificationInfo Notification { get; set; }
    }

    #region BookingAdmin

    [DataContract]
    public class BookingGetInfo
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "bookingId")]
        public int BookingId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "bookingNumber")]
        public string BookingNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "userId")]
        public string UserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "firstName")]
        public string FirstName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "lastName")]
        public string LastName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "email")]
        public string EmailAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "mobilePhone")]
        public string MobilePhone { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public int DealerId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "dealer")]
        public OrderDealerGetInfo Dealer { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "vehicleExterior")]
        public VehicleExteriorGetInfo VehicleExterior { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "vehicleInterior")]
        public VehicleInteriorGetInfo VehicleInterior { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "tradeInVehicle")]
        public OrderTradeVehicleInfo TradeInVehicle { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "packages")]
        public string Packages { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ChargeToken { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ChargeIdentifier { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "amount")]
        public int TotalAmount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "description")]
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "status")]
        public string Status { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Name = "createDate")]
        public string CreateDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CreateSource { get; set; }
    }

    public class UserProfileGetModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
    }

    [DataContract]
    public class OrderTradeVehicleInfo
    {
        [DataMember(Name = "make")]
        public string Make { get; set; }

        [DataMember(Name = "model")]
        public string Model { get; set; }

        [DataMember(Name = "year")]
        public string Year { get; set; }

        [DataMember(Name = "kilometers")]
        public string Kilometers { get; set; }

        [DataMember(Name = "hasFinance")]
        public bool hasFinance { get; set; }
    }

    [DataContract]
    public class OrderDealerGetInfo
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "address")]
        public string Address { get; set; }

        [DataMember(Name = "suburb")]
        public string Suburb { get; set; }

        [DataMember(Name = "state")]
        public string State { get; set; }

        [DataMember(Name = "postcode")]
        public string Postcode { get; set; }
    }

    [DataContract]
    public class VehicleExteriorGetInfo
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }
    }

    [DataContract]
    public class VehicleInteriorGetInfo
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }
    }

    [DataContract]
    public class PackageGetInfo
    {
        [DataMember(Name = "name")]
        public string Name { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class OrderDealerResponseMessage
    {
        /// <summary>
        /// 
        /// </summary>
        public bool success { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Error error { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<OrderDealerInfo> data { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class Error
    {
        /// <summary>
        /// 
        /// </summary>
        public int code { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string description { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class OrderDealerInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Suburb { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Postcode { get; set; }
    }

    #endregion
}