﻿using Audi.Prelaunch.Business.Processors;
using Our.Umbraco.Ditto;
using System.Collections.Generic;

namespace Audi.Prelaunch.Business.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class VehicleInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ModelId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string VersionId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [MediaUrlProcessor(Order = 4)]
        [UmbracoProperty("termsAndConditionsUrl")]
        public string TermsAndConditionsUrl { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [ExteriorProcessor(Order = 1)]
        [UmbracoProperty("exteriors")]
        public List<ExteriorInfo> exteriors { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [InteriorProcessor(Order = 2)]
        [UmbracoProperty("interiors")]
        public List<InteriorInfo> interiors { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [PackageProcessor(Order = 3)]
        [UmbracoProperty("packages")]
        public List<PackageInfo> packages { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ExteriorInfo
    {
        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("key")]
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("image")]
        [MediaUrlProcessor(Order = 1)]
        public List<string> MainImages { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("thumbnail")]
        [MediaUrlProcessor(Order = 2)]
        public string ThumbnailImage { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class InteriorInfo
    {
        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("key")]
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Seats { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Dashboard { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Carpet { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("image")]
        [MediaUrlProcessor(Order = 1)]
        public string MainImage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("thumbnailImage")]
        [MediaUrlProcessor(Order = 2)]
        public string ThumbnailImage { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PackageInfo
    {
        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("key")]
        public string Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Price { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("isIncluded")]
        public bool IsIncluded { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [PackageFeatureProcessor(Order = 1)]
        [UmbracoProperty("features")]
        public List<PackageFeatureInfo> features { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PackageFeatureInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("image")]
        [MediaUrlProcessor(Order = 1)]
        public string Image { get; set; }
    }
}
