﻿using Audi.Prelaunch.Business.Model;
using Audi.Prelaunch.Business.Processors;
using Audi.Prelaunch.Models;
using Our.Umbraco.Ditto;
using System;
using System.Collections.Generic;

namespace Audi.Prelaunch.Business.Models
{
    #region Search Models

    /// <summary>
    /// 
    /// </summary>
    public class SearchResultInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("urlName")]
        public string Slug { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("subtitle")]
        [StripHtmlProcessor(Order = 1)]
        public string SubTitle { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("tileImage")]
        public MediaInfo TileImage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("profileImage")]
        [MediaProcessor(Order = 1)]
        public MediaInfo ProfileImage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [UmbracoProperty("createDate")]
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SeoMetaDescription { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [TagProcessor(Order = 2)]
        public string[] Tags { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DelimitedList("categories")]
        public List<Category> Categories { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class SearchResponseInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Slug { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public MediaInfo TileImage { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AuthorName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DateCreated { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string[] Tags { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<Category> Categories { get; set; }
    }

    #endregion
}