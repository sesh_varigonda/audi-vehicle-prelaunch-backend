﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audi.Prelaunch.Business.Model
{
    public class Image
    {
        public string src { get; set; }
        public string width { get; set; }
        public string height { get; set; }
        public string alt { get; set; }
    }

    public class Category
    {
        public string Name { get; set; }
        public string slug { get; set; }
    }

    public class DropdownItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string slug { get; set; }
    }
}
