﻿using System;
using ImageProcessor.Imaging;
using System.IO;
using ImageProcessor;

namespace Audi.Prelaunch.Business.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public class ImageHelper
    {
        /// <summary>
        /// Creates a cropped version of the image at the size specified in the parameters
        /// </summary>
        /// <param name="originalFilePath">The full path of the original file</param>
        /// <param name="newFilePath">The full path of the new file</param>
        /// <param name="width">The new image width</param>
        /// <param name="height">The new image height</param>
        /// <returns>A bool to show if the method was successful or not</returns>
        public bool CreateCroppedImage(string originalFilePath, string newFilePath, int width, int height)
        {
            ImageFactory imageFactory = new ImageFactory();
            bool success = false;

            try
            {
                imageFactory.Load(originalFilePath);
                ResizeLayer layer = new ResizeLayer(new System.Drawing.Size(width, height), ResizeMode.Crop, AnchorPosition.Center);

                //Performs auto-rotation to ensure that EXIF defined rotation is reflected in the final image.
                imageFactory.AutoRotate();

                imageFactory.Resize(layer);
                imageFactory.Save(newFilePath);
                success = true;
            }
            catch (Exception)
            {
                success = false;
            }
            finally
            {
                imageFactory.Dispose();
            }
            return success;
        }

        /// <summary>
        /// Creates a new file path using the original one and adding a suffix to the file name
        /// </summary>
        /// <param name="filePath">The full path of the original file</param>
        /// <param name="fileNameSuffix">The suffix to be used at the end of the file name in the new file path</param>
        /// <returns>The new file path</returns>
        public string GetNewFilePath(string filePath, string fileNameSuffix)
        {
            FileInfo fileInfo = new FileInfo(filePath);
            string folderPath = fileInfo.DirectoryName;
            string fileExtension = fileInfo.Extension;
            string fullFileName = fileInfo.Name;
            string fileNameWithoutExtension = fullFileName.Substring(0, fullFileName.Length - fileExtension.Length);
            return string.Format("{0}\\{1}{2}{3}", folderPath, fileNameWithoutExtension, fileNameSuffix, fileExtension);
        }


        /// <summary>
        /// Creates a new file path using the original one and adding a suffix to the file name
        /// </summary>
        /// <param name="filePath">The full path of the original file</param>
        /// <param name="fileNameSuffix">The suffix to be used at the end of the file name in the new file path</param>
        /// <returns>The new file path</returns>
        public string GetNewFileName(string filePath, string fileNameSuffix)
        {
            FileInfo fileInfo = new FileInfo(filePath);
            string folderPath = fileInfo.DirectoryName;
            string fileExtension = fileInfo.Extension;
            string fullFileName = fileInfo.Name;
            string fileNameWithoutExtension = fullFileName.Substring(0, fullFileName.Length - fileExtension.Length);
            return string.Format("{0}{1}{2}", fileNameWithoutExtension, fileNameSuffix, fileExtension);
        }

        /// <summary>
        /// Deletes a file, if it exists
        /// </summary>
        /// <param name="filePath">The full path of the file to delete</param>
        /// <returns>A bool to show if the method was successful or not</returns>
        public bool DeleteFile(string filePath)
        {
            bool success = false;
            try
            {
                if (System.IO.File.Exists(filePath))
                {
                    System.IO.File.Delete(filePath);
                }
                success = true;
            }
            catch (System.Exception)
            {
                success = false;
            }
            return success;
        }

        /// <summary>
        /// Renames a file by using the Move method.
        /// </summary>
        /// <param name="sourceFileName">The full path of the source file</param>
        /// <param name="destFileName">The full path of the destination file</param>
        /// <returns>A bool to show if the method was successful or not</returns>
        public bool RenameFile(string sourceFileName, string destFileName)
        {
            bool success = false;
            try
            {
                if (System.IO.File.Exists(sourceFileName) && !System.IO.File.Exists(destFileName))
                {
                    System.IO.File.Move(sourceFileName, destFileName);
                    success = true;
                }
            }
            catch (System.Exception)
            {
                success = false;
            }
            return success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        public string ToRelative(Uri uri)
        {
            return uri.IsAbsoluteUri ? uri.PathAndQuery : uri.OriginalString;
        }

    }
}