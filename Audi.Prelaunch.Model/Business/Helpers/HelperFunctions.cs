﻿using CsvHelper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace Audi.Prelaunch.Business.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public class HelperFunctions
    {
        /// <summary>
        /// function to write data to the csv file.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="filePath"></param>
        public void WriteToCSVFile(List<dynamic> data, string filePath)
        {
            using (var csv = new CsvWriter(new StreamWriter(filePath)))
            {
                var firstRecord = data.First() as IDictionary<string, object>;
                var columnList = firstRecord.Keys;

                foreach (var item in columnList)
                {
                    csv.WriteField(item, false);
                }

                csv.NextRecord();

                foreach (var item in data)
                {
                    try
                    {
                        var expandoDict = item as IDictionary<string, object>;
                        foreach (var value in expandoDict.Values)
                        {
                            csv.WriteField(value == null ? string.Empty : value.ToString(), false);
                        }

                        csv.NextRecord();
                    }
                    catch
                    {
                        //Supressing  the Exception
                    }
                }
            }
        }

        /// <summary>
        /// function to parse a csv file and write it to a dynamic object.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public List<dynamic> ParseCSVFile(string filename)
        {
            Dictionary<string, int> headers = new Dictionary<string, int>();
            List<dynamic> records = new List<dynamic>();

            using (var sr = new StreamReader(filename))
            {
                var reader = new CsvReader(sr);
                return reader.GetRecords<dynamic>().ToList();
            }
        }

        /// <summary>
        /// Parses and translates a json string to a dictionary object. 
        /// </summary>
        /// <param name="jsonString"></param>
        /// <param name="fieldList"></param>
        /// <returns></returns>
        public Dictionary<string, string> GetJObjectData(string jsonString, List<string> fieldList)
        {
            Dictionary<string, string> keyValuePairs = new Dictionary<string, string>();

            JObject payloadItems = JsonConvert.DeserializeObject<JObject>(jsonString, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            foreach (var property in payloadItems.Properties())
            {
                foreach (var data in property)
                {
                    if (property.Type != JTokenType.Array && property.Type != JTokenType.Object)
                    {
                        var jObject = data as JArray;
                        foreach (JObject content in jObject.Children<JObject>())
                        {
                            foreach (JProperty prop in content.Properties())
                            {
                                if (fieldList.Contains(prop.Name))
                                {
                                    keyValuePairs.Add(prop.Name, prop.Value.ToString());
                                }
                            }
                        }
                    }
                }
            }

            return keyValuePairs;
        }
    }
}