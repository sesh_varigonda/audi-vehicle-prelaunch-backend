﻿using log4net;
using Umbraco.Core;

namespace Audi.Prelaunch.Business.Helpers
{
    public class MemoryCaching
    {
        static ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Get Content By Cachekey
        /// </summary>
        public static object GetContent(string cacheKey)
        {
            log.InfoFormat("Getting Cache Item {0} Value {1}", cacheKey, ApplicationContext.Current.ApplicationCache.RuntimeCache.GetCacheItem(cacheKey));
            return ApplicationContext.Current.ApplicationCache.RuntimeCache.GetCacheItem(cacheKey);
        }

        /// <summary>
        /// Set Content By Cachekey and Content
        /// </summary>
        public static void SetContent(string cacheKey, object contentItem)
        {
            object cachedItem = ApplicationContext.Current.ApplicationCache.RuntimeCache.GetCacheItem(cacheKey, () => contentItem);

            log.InfoFormat("Setting Cache Item {0} Value {1}", cacheKey, contentItem);
            ApplicationContext.Current.ApplicationCache.RuntimeCache.InsertCacheItem(cacheKey, () => contentItem);
        }
    }
}