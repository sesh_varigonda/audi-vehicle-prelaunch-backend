﻿using System;
using Umbraco.Core;
using umbraco;
using System.Xml.XPath;
using System.Configuration;
using log4net;
using System.Text.RegularExpressions;
using Umbraco.Core.Models;
using System.Collections.Generic;
using Umbraco.Core.Services;
using System.Linq;
using System.Text;

namespace Audi.Prelaunch.Business.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    public class UmbracoHelperFunctions
    {
        private static ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static string PROTOCOL_HTTP = "http";
        private static string pattern = @"^[a-zA-Z0-9_]+$";

        /// <summary>
        /// Returns the Media Source Url by MediaId
        /// </summary>
        /// <param name="mediaId">Umbraco MediaId</param>
        /// <returns></returns>
        public static Model.Image GetPathByMediaId(string mediaId)
        {
            Model.Image imageInfo = new Model.Image();
            try
            {
                if (string.IsNullOrEmpty(mediaId)) return imageInfo;

                var data = library.GetMedia(int.Parse(mediaId), true).Current;
                XPathNodeIterator nodes = data.SelectDescendants(XPathNodeType.All, false);

                while (nodes.MoveNext())
                {
                    var node = nodes.Current;
                    if (node.Name == "umbracoFile")
                    {
                        string urlPath = node.InnerXml;
                        if (urlPath != null)
                        {
                            if (!urlPath.ToLower().Contains(PROTOCOL_HTTP))
                            {
                                imageInfo.src = string.Format("{0}{1}", ConfigurationManager.AppSettings["BaseUrl"], urlPath);
                            }
                            else
                            {
                                imageInfo.src = urlPath;
                            }
                        }
                    }
                    if(node.Name == "umbracoWidth")
                    {
                        imageInfo.width = node.InnerXml;
                    }
                    if (node.Name == "umbracoHeight")
                    {
                        imageInfo.height = node.InnerXml;
                    }
                }

                return imageInfo;
            }
            catch(Exception ex)
            {
                log.Error(ex.StackTrace);
            }

            return imageInfo;
        }


        /// <summary>
        /// Converting a string value to Boolean
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool ParseString(string value)
        {
            if(string.IsNullOrEmpty(value))
            {
                return false;
            }

            return Convert.ToBoolean(int.Parse(value));
        }


        /// <summary>
        /// Retriving the Dropdownlist value by the IdValue
        /// </summary>
        /// <param name="idValue"></param>
        /// <returns></returns>
        public static string GetDropdownListValue(string idValue)
        {
            try
            {
                if (string.IsNullOrEmpty(idValue)) return string.Empty;

                int result;
                var output = int.TryParse(idValue, out result);

                if (output == true)
                {
                    return umbraco.library.GetPreValueAsString(int.Parse(idValue));
                }
            }
            catch (Exception ex)
            {
                log.Error(ex.StackTrace);
            }

            return idValue;
        }


        /// <summary>
        /// Convert a string to a slugname.
        /// </summary>
        /// <param name="contentId"></param>
        /// <returns></returns>
        public static string GetSlugName(int contentId)
        {
            int ExcludeParts = 2;
            string url = umbraco.library.NiceUrl(contentId);

            //Removing special characters from the url
            url = url.Replace("#", string.Empty).Replace("(", string.Empty).Replace(")", string.Empty);

            if(string.IsNullOrEmpty(url))
            {
                return GetUnPublishedSlug(contentId);
            }

            var urlParts = url.Split('/');
            if (urlParts.Length >= ExcludeParts)
            {
                return urlParts[urlParts.Length - ExcludeParts];
            }
            else
            {
                return string.Empty;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentId"></param>
        /// <returns></returns>
        public static string GetUnPublishedSlug(int contentId)
        {
            var contentName = ApplicationContext.Current.Services.ContentService.GetById(contentId).Name.ToLower();
            string slugName = Regex.Replace(contentName, pattern, "").Replace(" ", "-");

            return slugName.ToLower();
        }

        /// <summary>
        /// saving multiple dropdown values to the umbraco property field.
        /// </summary>
        /// <param name="editableNode"></param>
        /// <param name="propAlias"></param>
        /// <param name="values"></param>
        public static void SetMultipleDropDownValue(IContent editableNode, string propAlias, List<string> values)
        {
            // Grab data type service
            IDataTypeService dts = ApplicationContext.Current.Services.DataTypeService;

            //find the property on the content node by its alias
            Property prop = editableNode.Properties.FirstOrDefault(a => a.Alias == propAlias);

            if (prop != null)
            {
                //get data type from the property
                var dt = dts.GetDataTypeDefinitionById(prop.PropertyType.DataTypeDefinitionId);

                //get the id of the prevalue for 'val'  
                List<int> preValues = new List<int>();
                foreach (var val in values)
                {
                    var selectedItems = dts.GetPreValuesCollectionByDataTypeId(dt.Id).PreValuesAsDictionary.Where(d => d.Value.Value == val);
                    if (selectedItems.Any())
                    {
                        int preValueId = selectedItems.Select(f => f.Value.Id).First();
                        preValues.Add(preValueId);
                    }
                }

                if (preValues != null && preValues.Any())
                {
                    editableNode.SetValue(propAlias, String.Join(",", preValues));
                }
            }
        }

        /// <summary>
        /// saving dropdown value to the umbraco property field.
        /// </summary>
        /// <param name="editableNode"></param>
        /// <param name="propAlias"></param>
        /// <param name="val"></param>
        public static void SetDropDownValue(IContent editableNode, string propAlias, string val)
        {
            // Grab data type service
            IDataTypeService dts = ApplicationContext.Current.Services.DataTypeService;

            //find the property on the content node by its alias
            Property prop = editableNode.Properties.FirstOrDefault(a => a.Alias == propAlias);

            if (prop != null)
            {
                //get data type from the property
                var dt = dts.GetDataTypeDefinitionById(prop.PropertyType.DataTypeDefinitionId);

                //get the id of the prevalue for 'val'  
                int preValueId = dts.GetPreValuesCollectionByDataTypeId(dt.Id).PreValuesAsDictionary.Where(d => d.Value.Value.ToLower() == val.ToLower()).Select(f => f.Value.Id).First();

                editableNode.SetValue(propAlias, preValueId);

            }
        }

        /// <summary>
        /// returns datatypeId by dataTypeName.
        /// </summary>
        /// <param name="dataTypeName"></param>
        /// <returns></returns>
        public static int GetDataTypeId(string dataTypeName)
        {
            var dataTypeService = ApplicationContext.Current.Services.DataTypeService;
            var allTypes = dataTypeService.GetAllDataTypeDefinitions();
            return allTypes.First(x => dataTypeName.InvariantEquals(x.Name)).Id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataTypeName"></param>
        /// <returns></returns>
        public static PreValueCollection GetPreValues(string dataTypeName)
        {
            int dataTypeId = GetDataTypeId(dataTypeName);
            var dts = ApplicationContext.Current.Services.DataTypeService;
            var preValues = dts.GetPreValuesCollectionByDataTypeId(dataTypeId);
            return preValues;
        }


        /// <summary>
        /// Returns PrevalueId by DatatypeName and Value
        /// </summary>
        /// <param name="dataTypeName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public int GetPreValueIdByDataTypeAndValue(string dataTypeName, string value)
        {
            var dataTypeService = ApplicationContext.Current.Services.DataTypeService;

            var dataTypeId = GetDataTypeId(dataTypeName);

            //get data type from the property
            var dt = dataTypeService.GetDataTypeDefinitionById(dataTypeId);

            //get the id of the prevalue for 'val'  
            int preValueId = dataTypeService.GetPreValuesCollectionByDataTypeId(dt.Id).PreValuesAsDictionary.Where(d => d.Value.Value.ToLower() == value.ToLower()).Select(f => f.Value.Id).First();

            return preValueId;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="preValueId"></param>
        /// <returns></returns>
        public static string GetPreValueString(string preValueId)
        {
            int Id;
            if (int.TryParse(preValueId, out Id))
            {
                return library.GetPreValueAsString(Id);
            }

            return string.Empty;
        }

        /// <summary>
        /// function to translate tagList to a umbraco taglist.
        /// </summary>
        /// <param name="tags"></param>
        /// <returns></returns>
        public string GetDBTags(List<string> tags)
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var tag in tags)
            {
                stringBuilder.AppendFormat("\"{0}\",", tag);
            }

            return string.Format("[{0}]", stringBuilder.ToString().TrimEnd(","));
        }
    }
}