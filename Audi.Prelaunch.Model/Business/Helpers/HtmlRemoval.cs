﻿using System.Text.RegularExpressions;

namespace Audi.Prelaunch.Business.Helpers
{
    /// <summary>
    /// Methods to remove HTML from strings.
    /// </summary>
    public static class HtmlRemoval
    {
        /// <summary>
        /// Remove HTML from string with Regex.
        /// </summary>
        public static string StripTagsRegex(string source)
        {
            return Regex.Replace(source, "<.*?>", string.Empty);
        }

        /// <summary>
        /// Compiled regular expression for performance.
        /// </summary>
        static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        /// <summary>
        /// Remove HTML from string with compiled Regex.
        /// </summary>
        public static string StripTagsRegexCompiled(string source)
        {
            return _htmlRegex.Replace(source, string.Empty);
        }

        /// <summary>
        /// Remove HTML tags from string using char array.
        /// </summary>
        public static string StripTagsCharArray(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            return new string(array, 0, arrayIndex);
        }

        /// <summary>
        /// Strips style tags from the html tags
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string StripStyleAttributes(string source)
        {
            string pattern = @"<([^>]*)(\sstyle="".+?""(\s|))(.*?)>";

            Regex r = new Regex(pattern);
            string htmlContent = r.Replace(source, "<$1>");
            
            //$3 applies for extra space if any.
            //string htmlContent = r.Replace(source, "<$1$3>");

            return htmlContent;
        }

        /// <summary>
        /// Strips class tags from the html tags
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string StripClassAttributes(string source)
        {
            string pattern = @"<([^>]*)(\sclass="".+?""(\s|))(.*?)>";

            Regex r = new Regex(pattern);
            string htmlContent = r.Replace(source, "<$1>");

            return htmlContent;
        }


        /// <summary>
        /// Removes Empty paragraphs Tags from the html.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string StripEmptyParagraphTag(string input)
        {
            int startPIndex = input.IndexOf("<p>");
            //if found a <p> tag
            if (startPIndex != -1)
            {
                //if <p> tag lies in between content, do not strip it.
                string preP = input.Substring(0, startPIndex);
                for (int i = 0; i < preP.Length; ++i)
                {
                    if (preP[i] != '\r' || preP[i] != '\n')
                        return input;
                }
                //if ending of p tag at end, then strip contents before
                if (input.LastIndexOf("</p>") == input.Length - 4)
                    return input.Substring(startPIndex + 3, input.Length - (startPIndex + 3) - 4);
            }

            return input;
        }

        /// <summary>
        /// Removes paragraphs Tags from the html.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string StripParagraphTag(string input)
        {
            input = input.Replace("<p>", string.Empty);
            input = input.Replace("</p>", string.Empty);

            return input;
        }

        /// <summary>
        /// Removes Empty Span Tags from the html.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string StripEmptySpanTags(string input)
        {
            int startPIndex = input.IndexOf("<span>");
            //if found a <p> tag
            if (startPIndex != -1)
            {
                //if <p> tag lies in between content, do not strip it.
                string preP = input.Substring(0, startPIndex);
                for (int i = 0; i < preP.Length; ++i)
                {
                    if (preP[i] != '\r' || preP[i] != '\n')
                        return input;
                }
                //if ending of p tag at end, then strip contents before
                if (input.LastIndexOf("</span>") == input.Length - 4)
                    return input.Substring(startPIndex + 3, input.Length - (startPIndex + 3) - 4);
            }

            return input;
        }

        /// <summary>
        /// Removes Span Tags from the html.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string StripSpanTags(string input)
        {
            input = input.Replace("<span>", string.Empty);
            input = input.Replace("</span>", string.Empty);

            return input;
        }

        /// <summary>
        /// function to strip html tags.
        /// </summary>
        /// <param name="htmlString"></param>
        /// <returns></returns>
        public static string StripHTML(string htmlString)
        {
            htmlString = StripHtmlTags(htmlString);

            string pattern = @"<(.|\n)*?>";
            string regexOutput = Regex.Replace(htmlString, pattern, string.Empty);

            regexOutput = regexOutput.Replace(System.Environment.NewLine, string.Empty);
            regexOutput = regexOutput.Replace("&nbsp;", string.Empty);

            return regexOutput;
        }

        /// <summary>
        /// function to strip html tags like style, paragraph and span.
        /// </summary>
        /// <param name="htmlString"></param>
        /// <returns></returns>
        public static string StripHtmlTags(string htmlString)
        {
            //Removes Style Attributes
            htmlString = StripStyleAttributes(htmlString);

            //Removes Class Attributes
            htmlString = StripClassAttributes(htmlString);

            //Removes Paragraph Tags
            //htmlString = StripParagraphTag(htmlString);

            //Removes Span Tags
            //htmlString = StripSpanTags(htmlString);

            //Removes Style Tags
            htmlString = StripStyleTags(htmlString);

            return htmlString;
        }

        /// <summary>
        /// function to strip html tags like style, paragraph and span.
        /// </summary>
        /// <param name="htmlString"></param>
        /// <returns></returns>
        public static string StripQuoteHtmlTags(string htmlString)
        {
            //Removes Style Attributes
            htmlString = StripStyleAttributes(htmlString);

            //Removes Class Attributes
            htmlString = StripClassAttributes(htmlString);

            //Removes EM Attributes
            htmlString = StripEMAttributes(htmlString);

            //Removes Strong Attributes
            htmlString = StripStrongAttributes(htmlString);

            //Removes Paragraph Tags
            //htmlString = StripParagraphTag(htmlString);

            //Removes Span Tags
            //htmlString = StripSpanTags(htmlString);

            //Removes Style Tags
            htmlString = StripStyleTags(htmlString);

            return htmlString;
        }

        /// <summary>
        /// Returns the First Paragraph from the Html Content.
        /// </summary>
        /// <param name="htmlString"></param>
        /// <returns></returns>
        public static string StripEMAttributes(string htmlString)
        {
            Match m = Regex.Match(htmlString, @"<em>\s*(.+?)\s*</em>");
            if (m.Success)
            {
                htmlString = htmlString.Replace(m.Groups[0].Value, (m.Groups[1].Value));
            }

            return htmlString;
        }

        /// <summary>
        /// Returns the First Paragraph from the Html Content.
        /// </summary>
        /// <param name="htmlString"></param>
        /// <returns></returns>
        public static string StripStrongAttributes(string htmlString)
        {
            Match m = Regex.Match(htmlString, @"<strong>\s*(.+?)\s*</strong>");
            if (m.Success)
            {
                htmlString = htmlString.Replace(m.Groups[0].Value, (m.Groups[1].Value));
            }

            return htmlString;
        }

        /// <summary>
        /// function to strip style tags.
        /// </summary>
        /// <param name="htmlString"></param>
        /// <returns></returns>
        public static string StripStyleTags(string htmlString)
        {
            if (string.IsNullOrEmpty(htmlString))
            {
                return string.Empty;
            }

            int startIndex = htmlString.IndexOf("<style");
            int endIndex = htmlString.IndexOf("</style>");

            if (startIndex > -1)
            {
                htmlString = htmlString.Replace(htmlString.Substring(startIndex, (endIndex - startIndex) + 8), string.Empty);
            }

            return htmlString;
        }

        /// <summary>
        /// Returns the First Paragraph from the Html Content.
        /// </summary>
        /// <param name="htmlString"></param>
        /// <returns></returns>
        public static string HtmlGetFirstParagraph(string htmlString)
        {
            Match m = Regex.Match(htmlString, @"<p\s*(.+?)\s*</p>");
            if (m.Success)
            {
                return m.Groups[1].Value;
            }
            else
            {
                return string.Empty;
            }
        }


        /// <summary>
        /// Returns the Paragraph Group from the Html Content.
        /// </summary>
        /// <param name="htmlString"></param>
        /// <param name="occurence"></param>
        /// <returns></returns>
        public static string HtmlGetParagraphByOccurence(string htmlString, int occurence)
        {
            var matchCollection = Regex.Matches(htmlString, @"<p\s*(.+?)\s*</p>");

            var match = matchCollection[occurence];
            if (match.Success)
            {
                return match.Captures[0].Value;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Returns the Paragraph Count for the Html
        /// </summary>
        /// <param name="htmlString"></param>
        /// <returns></returns>
        public static int GetParagraphCount(string htmlString)
        {
            var m = Regex.Matches(htmlString, @"<p\s*(.+?)\s*</p>");
            return m.Count;
        }
    }
}