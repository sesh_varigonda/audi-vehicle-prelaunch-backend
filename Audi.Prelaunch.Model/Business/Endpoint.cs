﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Dynamic;
using System.Net.Http;

namespace Audi.Prelaunch.Business
{
    /// <summary>
    /// 
    /// </summary>
    public class Endpoint : IDisposable
    {
        readonly string _baseUrl = ConfigurationManager.AppSettings["MagazineBaseUrl"];
        readonly HttpClient _client = new HttpClient();

        /// <summary>
        /// 
        /// </summary>
        public HttpClient Client
        {
            get { return _client; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Endpoint()
        {
            _client.BaseAddress = new Uri(_baseUrl);
            _client.DefaultRequestHeaders.Accept.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        public void Dispose()
        {
            _client.Dispose();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public dynamic ClearEmptyFields<T>(T obj)
        {
            var t = obj.GetType();
            var result = new ExpandoObject() as IDictionary<string, object>;

            foreach (var pr in t.GetProperties())
            {
                var val = pr.GetValue(obj);

                if (val is string && string.IsNullOrWhiteSpace(val.ToString())) continue;
                if (val == null) continue;

                result.Add(pr.Name, val);
            }
            return result;
        }
    }
}