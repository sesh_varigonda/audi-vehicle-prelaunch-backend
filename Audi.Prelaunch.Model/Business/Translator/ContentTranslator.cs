﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core;
using Umbraco.Web;
using umbraco.cms.businesslogic.web;
using Umbraco.Core.Models;

namespace Audi.Prelaunch.Business.Translator
{
    /// <summary>
    /// 
    /// </summary>
    public class ContentTranslator
    {
        private bool _mIsPublished = true;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IsPublished"></param>
        public ContentTranslator(bool IsPublished = true)
        {
            _mIsPublished = IsPublished;
        }

        /// <summary>
        /// Returns content by Id based on whether the content has been published or not.
        /// </summary>
        /// <param name="contentId">Id of the content being retrieved.</param>
        /// <returns>Returns ContentInfo Object.</returns>
        public ContentInfo GetContentById(int contentId)
        {
            object contentInfo = null;
            if(_mIsPublished)
            {
                var umbracoHelper = new Umbraco.Web.UmbracoHelper(Umbraco.Web.UmbracoContext.Current);
                contentInfo = umbracoHelper.TypedContent(contentId);
            }
            else
            {
                contentInfo = ApplicationContext.Current.Services.ContentService.GetById(contentId);
            }

            if (contentInfo == null) return null;

            return GetContent(contentInfo);
        }


        /// <summary>
        /// Translates the IContent/IPublished content into ContentInfo object.
        /// </summary>
        /// <param name="data">This param accepts data of type IContent or IPublishedContent.</param>
        /// <returns>Returns ContentInfo Object</returns>
        public ContentInfo GetContent(object data)
        {
            Dictionary<string, object> properties = new Dictionary<string,object>();

            if (data.GetType() == typeof(ContentInfo))
            {
                return (ContentInfo)data;
            }
            else if(data.GetType() == typeof(Content))
            {
                var content = (IContent)data;
                var contentTypeAlias = new Document(content.Id).ContentType.Alias;

                foreach(var property in content.Properties)
                {
                    properties.Add(property.Alias, property.Value);
                }

                var contentInfo = new ContentInfo()
                { 
                    Id = content.Id, 
                    Alias = contentTypeAlias, 
                    Properties = properties, 
                    Name = content.Name, 
                    isPublished = content.Published, 
                    Path = content.Path,
                    UpdateDate = content.UpdateDate,
                    CreateDate = content.CreateDate
                };

                if(content.Children().Any())
                {
                    foreach(IContent contentData in content.Children())
                    {
                        if (contentInfo.Children == null)
                        {
                            contentInfo.Children = new List<ContentInfo>();
                        }

                        contentInfo.Children.Add(GetContent(contentData));
                    }
                }

                return contentInfo;
            }
            else
            {
                var content = (IPublishedContent)data;

                foreach(var property in content.Properties)
                {
                    properties.Add(property.PropertyTypeAlias, property.DataValue);
                }

                var contentInfo = new ContentInfo() 
                { 
                    Id = content.Id, 
                    Alias = content.DocumentTypeAlias, 
                    Properties = properties, 
                    Name = content.Name, 
                    isPublished = true,
                    Path = content.Path,
                    UpdateDate = content.UpdateDate,
                    CreateDate = content.CreateDate,
                    UrlName = content.UrlName
                };

                if (content != null && content.Children != null && content.Children.Any())
                {
                    foreach (IPublishedContent contentData in content.Children())
                    {
                        if(contentInfo.Children == null)
                        {
                            contentInfo.Children = new List<ContentInfo>();
                        }

                        contentInfo.Children.Add(GetContent(contentData));
                    }
                }

                return contentInfo;
            }
        }


        #region Private Functions

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ContentInfo GetContentItem(object data)
        {
            Dictionary<string, object> properties = new Dictionary<string, object>();

            if (data.GetType() == typeof(ContentInfo))
            {
                return (ContentInfo)data;
            }
            else if (data.GetType() == typeof(Content))
            {
                var content = (IContent)data;
                var contentTypeAlias = new umbraco.cms.businesslogic.web.Document(content.Id).ContentType.Alias;

                foreach (var property in content.Properties)
                {
                    properties.Add(property.Alias, property.Value);
                }

                var contentInfo = new ContentInfo() { Id = content.Id, Alias = contentTypeAlias, Properties = properties, Name = content.Name, isPublished = content.Published, Path = content.Path };

                if (content.Children().Any())
                {
                    foreach (IContent contentData in content.Children())
                    {
                        if (contentInfo.Children == null)
                        {
                            contentInfo.Children = new List<ContentInfo>();
                        }

                        contentInfo.Children.Add(GetContentItem(contentData));
                    }
                }

                return contentInfo;
            }
            else
            {
                var content = (IPublishedContent)data;

                foreach (var property in content.Properties)
                {
                    properties.Add(property.PropertyTypeAlias, property.DataValue);
                }

                var contentInfo = new ContentInfo() { Id = content.Id, Alias = content.DocumentTypeAlias, Properties = properties, Name = content.Name, isPublished = true, Path = content.Path };

                if (content != null && content.Children != null && content.Children.Any())
                {
                    foreach (IPublishedContent contentData in content.Children())
                    {
                        if (contentInfo.Children == null)
                        {
                            contentInfo.Children = new List<ContentInfo>();
                        }

                        contentInfo.Children.Add(GetContentItem(contentData));
                    }
                }

                return contentInfo;
            }
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public class ContentInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Alias { get; set;}

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, object> Properties { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<ContentInfo> Children { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool isPublished { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string UrlName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime UpdateDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateDate { get; set; }
    }
}