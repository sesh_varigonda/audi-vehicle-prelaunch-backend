﻿using Audi.Prelaunch.Business.Models;
using Examine;
using Examine.SearchCriteria;
using Our.Umbraco.Ditto;
using System.Linq;
using Umbraco.Web;
using log4net;
using System.Reflection;
using System.Collections.Generic;
using Umbraco.Core.Models;
using Umbraco.Core;
using System.IO;
using System.Configuration;
using umbraco;
using Audi.Prelaunch.Business.Helpers;

namespace Audi.Prelaunch.Business.Service
{
    /// <summary>
    /// This service contains all the functions related to Article.
    /// </summary>
    public class ModelService
    {
        ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// returns list of all the articles
        /// </summary>
        /// <returns></returns>
        public VehicleInfo GetList()
        {
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);

            var searcher = ExamineManager.Instance.DefaultSearchProvider;
            var criteria = searcher.CreateSearchCriteria(UmbracoExamine.IndexTypes.Content).Field("isPublished", "true").And().GroupedOr(new string[] { "nodeTypeAlias" }, new string[] { "model" });

            ISearchCriteria filter = criteria.Compile();
            var vehicleList = umbracoHelper.TypedSearch(filter).As<VehicleInfo>().Distinct().ToList();

            return vehicleList.FirstOrDefault();
        }

        /// <summary>
        /// Saves the mediaItem and returns the ID.
        /// </summary>
        /// <param name="article"></param>
        /// <param name="fileName"></param>
        /// <param name="filePath"></param>
        public IMedia CreateMediaItem(string fileName, string filePath)
        {
            IMedia mediaImage = null;
            var mediaService = ApplicationContext.Current.Services.MediaService;

            UmbracoHelperFunctions umbracoHelperFunctions = new UmbracoHelperFunctions();

            using (FileStream fileStream = new FileStream(filePath, FileMode.Open))
            {
                var mediaFolderId = int.Parse(ConfigurationManager.AppSettings["ExteriorMediaFolderId"]);

                mediaImage = mediaService.CreateMedia(fileName, mediaFolderId, "Image");

                mediaImage.SetValue("umbracoFile", fileName, fileStream);
                mediaImage.SetValue("umbracoAltText", "test");

                mediaService.Save(mediaImage);

                return mediaImage;
            }
        }

        /// <summary>
        /// Delete a mediaItem by Id
        /// </summary>
        /// <param name="mediaId"></param>
        public void DeleteMediaItem(string mediaId)
        {
            IMedia mediaImage = null;
            var mediaService = ApplicationContext.Current.Services.MediaService;

            mediaImage = mediaService.GetById(int.Parse(mediaId));
            mediaService.Delete(mediaImage);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public List<umbraco.cms.businesslogic.media.Media> GetMediaByName(string name)
        {
            return uQuery.GetMediaByXPath("descendant::*[@nodeName='" + name + "']").ToList();
        }
    }
}