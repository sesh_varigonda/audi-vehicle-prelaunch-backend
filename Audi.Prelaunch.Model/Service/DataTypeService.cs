﻿using System.Collections.Generic;
using System.Linq;
using Audi.Prelaunch.Business.Models;
using Umbraco.Core;
using log4net;
using Umbraco.Web;
using Audi.Prelaunch.Business.Model;

namespace Audi.Prelaunch.Cms.Business
{
    /// <summary>
    /// This service contains all the functions related to DataType.
    /// </summary>
    public class DataTypeService
    {
        #region ConfigurationItems

        ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// 
        /// </summary>
        public DataTypeService()
        {
        }

        #endregion

        /// <summary>
        /// returns the list of all the categories.
        /// </summary>
        /// <returns></returns>
        public List<Category> GetCategories()
        {
            List<Category> categories = new List<Category>();

            var dataTypeService = ApplicationContext.Current.Services.DataTypeService;
            var allTypes = dataTypeService.GetAllDataTypeDefinitions();
            var typeId = allTypes.First(x => "Dropdown - Categories".InvariantEquals(x.Name)).Id;
            var categoryList = dataTypeService.GetPreValuesByDataTypeId(typeId);

            foreach (string category in categoryList)
            {
                if(category != "0")
                {
                    categories.Add(new Category() { Name = category, slug = category.ToLower().Replace(" ", "-") });
                }
            }

            return categories;
        }

        /// <summary>
        /// returns the list of dropdown items by type.
        /// </summary>
        /// <returns></returns>
        public List<DropdownItem> GetDropdownItemsByType(string type)
        {
            List<DropdownItem> dropdownItems = new List<DropdownItem>();

            var dataTypeService = ApplicationContext.Current.Services.DataTypeService;
            var allTypes = dataTypeService.GetAllDataTypeDefinitions();
            var typeId = allTypes.First(x => type.InvariantEquals(x.Name)).Id;
            var dropdownList = dataTypeService.GetPreValuesByDataTypeId(typeId);

            foreach (string dropdownitem in dropdownList)
            {
                dropdownItems.Add(new DropdownItem() { Name = dropdownitem, slug = dropdownitem.ToLower().Replace(" ", "-") });
            }

            return dropdownItems;
        }
    }
}