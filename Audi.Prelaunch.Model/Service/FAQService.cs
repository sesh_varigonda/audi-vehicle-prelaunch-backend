﻿using Examine;
using Examine.SearchCriteria;
using Our.Umbraco.Ditto;
using System.Linq;
using Umbraco.Web;
using log4net;
using System.Reflection;
using System.Collections.Generic;
using Audi.Prelaunch.Business.Models;
using Audi.Prelaunch.Models;

namespace Audi.Prelaunch.Cms.Service
{
    /// <summary>
    /// This service contains all the functions related to Article.
    /// </summary>
    public class FAQService
    {
        ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// returns list of faqs
        /// </summary>
        /// <returns></returns>
        public List<FAQInfo> GetList()
        {
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);

            var searcher = ExamineManager.Instance.DefaultSearchProvider;
            var criteria = searcher.CreateSearchCriteria(UmbracoExamine.IndexTypes.Content).Field("isPublished", "true").And().GroupedOr(new string[] { "nodeTypeAlias" }, new string[] { "faqs" });

            ISearchCriteria filter = criteria.Compile();
            var faq = umbracoHelper.TypedSearch(filter).As<FAQ>().Distinct().FirstOrDefault();

            return faq.Items;
        }
    }
}