﻿using Audi.Prelaunch.Business.Processors;
using Audi.Prelaunch.Business.Models;
using log4net;
using Our.Umbraco.Ditto;
using System;
using System.Configuration;
using System.Reflection;
using System.Xml.XPath;
using umbraco;
using Umbraco.Web;
using Umbraco.Web.Models;
using Audi.Prelaunch.Business.Model;
using Audi.Prelaunch.Models;

namespace Audi.Prelaunch.Cms.Service
{
    /// <summary>
    /// This service contains all the functions related to Umbraco MediaService.
    /// </summary>
    public class MediaService
    {
        private const string DEFAULT_FOCAL_PERCENT = "50%";
        private static string PROTOCOL_HTTP = "http";
        ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// returns mediainfo by url.
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public MediaInfo GetMediaByUrl(string url)
        {
            var helper = new UmbracoHelper(UmbracoContext.Current);

            var mediaItem = helper.TypedMedia(url);

            if (mediaItem == null) return null;

            var mediaInfo = mediaItem.As<MediaInfo>();

            //Set focal point
            var focalPoint = GetImageFocalPoint(mediaInfo.Id);
            if (focalPoint != null)
            {
                mediaInfo.X = focalPoint.x;
                mediaInfo.Y = focalPoint.y;
            }
            else
            {
                mediaInfo.X = DEFAULT_FOCAL_PERCENT;
                mediaInfo.Y = DEFAULT_FOCAL_PERCENT;
            }

            return mediaInfo;
        }

        /// <summary>
        /// Returns Image FocalPoint
        /// </summary>
        /// <param name="mediaId"></param>
        /// <returns></returns>
        public FocalPoint GetImageFocalPoint(string mediaId)
        {
            decimal focalPointLeft = 0;
            decimal focalPointTop = 0;

            try
            {
                decimal imageWidth = 0;
                decimal imageHeight = 0;
                if (string.IsNullOrEmpty(mediaId) || mediaId == "0") return null;

                var crops = new UmbracoHelper(UmbracoContext.Current).Media(mediaId).GetPropertyValue<ImageCropDataSet>("umbracoFile");

                if (crops == null || crops.FocalPoint == null) return null;

                var image = GetImageProperties(mediaId);

                if (image == null || string.IsNullOrEmpty(image.width) || string.IsNullOrEmpty(image.height)) return null;

                imageWidth = decimal.Parse(image.width);
                imageHeight = decimal.Parse(image.height);

                focalPointLeft = ((crops.FocalPoint.Left * imageWidth) / imageWidth) * 100;
                focalPointTop = ((crops.FocalPoint.Top * imageHeight) / imageHeight) * 100;
            }
            catch
            {
                //Suppress the Exception
            }

            return new FocalPoint() { x = string.Format("{0}%", Convert.ToInt32(focalPointLeft)), y = string.Format("{0}%", Convert.ToInt32(focalPointTop)) };
        }


        /// <summary>
        /// Returns the Media Source Url by MediaId
        /// </summary>
        /// <param name="mediaId">Umbraco MediaId</param>
        /// <returns></returns>
        public Image GetImageProperties(string mediaId)
        {
            Image imageInfo = new Image();
            try
            {
                if (string.IsNullOrEmpty(mediaId)) return imageInfo;

                var data = library.GetMedia(int.Parse(mediaId), true).Current;
                XPathNodeIterator nodes = data.SelectDescendants(XPathNodeType.All, false);

                while (nodes.MoveNext())
                {
                    var node = nodes.Current;
                    if (node.Name == "umbracoFile")
                    {
                        string urlPath = node.InnerXml;
                        if (urlPath != null)
                        {
                            if (!urlPath.ToLower().Contains(PROTOCOL_HTTP))
                            {
                                imageInfo.src = string.Format("{0}{1}", ConfigurationManager.AppSettings["BaseUrl"], urlPath);
                            }
                            else
                            {
                                imageInfo.src = urlPath;
                            }
                        }
                    }
                    if (node.Name == "umbracoWidth")
                    {
                        imageInfo.width = node.InnerXml;
                    }
                    if (node.Name == "umbracoHeight")
                    {
                        imageInfo.height = node.InnerXml;
                    }
                }

                return imageInfo;
            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace);
            }

            return imageInfo;
        }
    }
}