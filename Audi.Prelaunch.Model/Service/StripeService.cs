﻿using log4net;
using System.Reflection;
using System.Configuration;
using Stripe;
using System;
using System.Collections.Generic;
using Audi.Prelaunch.Business.Entities;
using Audi.Prelaunch.Business.Models;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Linq;
using Audi.Prelaunch.Models;

namespace Audi.Prelaunch.Cms.Service
{
    /// <summary>
    /// This service contains all the functions related to Article.
    /// </summary>
    public class StripeService
    {
        ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetStripeTestToken()
        {
            var apiKey = ConfigurationManager.AppSettings["StripeApiKey"];
            StripeConfiguration.SetApiKey(apiKey);

            try
            {
                var options = new TokenCreateOptions
                {
                    Card = new CreditCardOptions
                    {
                        Number = "4242424242424242",
                        ExpYear = 2019,
                        ExpMonth = 12,
                        Cvc = "123"
                    }
                };

                var service = new TokenService();
                Token stripeToken = service.Create(options);

                return stripeToken.Id;
            }
            catch (StripeException e)
            {
                throw new ApplicationException(e.StripeError.Message);
            }
        }

        /// <summary>
        /// This function validates the card payment details.
        /// </summary>
        /// <param name="booking"></param>
        /// <param name="source"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public string ValidateCardPayment(BookingInfo booking, string source, decimal amount)
        {
            var apiKey = ConfigurationManager.AppSettings["StripeApiKey"];
            StripeConfiguration.SetApiKey(apiKey);

            try
            {
                var chargeOptions = new ChargeCreateOptions
                {
                    Amount = (int)amount * 100,
                    Currency = "aud",
                    Description = "Charged via Audi Prelaunch towards order",
                    SourceId = booking.ChargeToken,
                    Capture = false
                };

                var chargeService = new ChargeService();
                var result = chargeService.Create(chargeOptions);

                return result.Id;
            }
            catch (StripeException e)
            {
                throw new ForbiddenException("Payment could not be processed - " + e.StripeError.Message);
            }
        }

        /// <summary>
        /// This function captures the card payment.
        /// </summary>
        /// <param name="chargeIdentifier"></param>
        /// <param name="source"></param>
        public void CaptureCardPayment(string chargeIdentifier, string source)
        {
            var apiKey = ConfigurationManager.AppSettings["StripeApiKey"];
            StripeConfiguration.SetApiKey(apiKey);

            try
            {
                var chargeService = new ChargeService();
                chargeService.Capture(chargeIdentifier, null);
            }
            catch (StripeException e)
            {
                throw new ForbiddenException("Payment could not be processed - " + e.StripeError.Message);
            }
        }

        /// <summary>
        /// This function updates the card payment details.
        /// </summary>
        /// <param name="booking"></param>
        /// <param name="orderRequestModel"></param>
        /// <param name="chargeIdentifier"></param>
        public void UpdateCardPayment(BookingInfo booking, OrderRequestModel orderRequestModel,  string chargeIdentifier)
        {
            var apiKey = ConfigurationManager.AppSettings["StripeApiKey"];
            StripeConfiguration.SetApiKey(apiKey);

            var dealerInfo = GetDealers().Where(a => a.DealerCode == booking.DealerCode).FirstOrDefault();
            var dealerName = dealerInfo != null ? dealerInfo.Name : string.Empty;

            try
            {
                var metaData = new Dictionary<string, string>
                                    {
                                        {"Boooking number", booking.BookingNumber},
                                        {"Primary Attendee", String.Format("{0} {1} <{2}>", orderRequestModel.customer.FirstName, orderRequestModel.customer.LastName, orderRequestModel.customer.Email)},
                                        {"Options", String.Format("Interior Colour : {0}, Exterior Colour : {1}, Packages : {2}", orderRequestModel.options.InteriorColor.Name, orderRequestModel.options.ExteriorColor.Name, string.Join(":", orderRequestModel.options.Packages.Select(a=>a.Name).ToList()) )},
                                        {"Source", "Prelaunch"},
                                        {"Dealer Name", dealerName},
                                    };

                var chargeOptions = new ChargeUpdateOptions
                {
                    Description = booking.Description + " with Order number " + booking.BookingNumber,
                    Metadata = metaData
                };

                var chargeService = new ChargeService();
                chargeService.Update(chargeIdentifier, chargeOptions);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Returns List of Dealers from myAudi CMS.
        /// </summary>
        /// <returns></returns>
        public List<DealerGetInfo> GetDealers()
        {
            var result = string.Empty;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["ApiCMSInternalBaseUrl"]);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    var response = client.GetAsync(string.Format("/2/legacy/dealers/model/{0}", "Prelaunch")).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        result = response.Content.ReadAsStringAsync().Result;
                    }
                }

                var responseMessage = JsonConvert.DeserializeObject<ResponseMessage>(result);
                return responseMessage.data;
            }
            catch (Exception e)
            {
                logger.Error(e);
            }

            return null;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ResponseMessage
    {
        /// <summary>
        /// 
        /// </summary>
        public bool success { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Error error { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string message { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<DealerGetInfo> data { get; set; }
    }

}