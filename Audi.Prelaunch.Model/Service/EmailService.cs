﻿using System.Configuration;
using System.Reflection;
using System.Text.RegularExpressions;
using log4net;
using System.Collections.Generic;
using System.Linq;
using RestSharp;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Net;

namespace Audi.Prelaunch.Business.Service
{
    /// <summary>
    /// This service contains all the functions related to Subsribing an EmailAddress.
    /// </summary>
    public class EmailService
    {
        ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        #region Validate Email

        /// <summary>
        /// function to validate emailaddress.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public bool ValidateEmailAddress(string emailAddress)
        {
            try
            {
                return CheckEmail(emailAddress);
            }
            catch
            {
                if (!ValidateEmailRegex(emailAddress))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// function to check and validate emailaddress using qrs email validation API.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        private bool CheckEmail(string emailAddress)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

            string basicAuthCredentials = ConfigurationManager.AppSettings["EmailBasicAuthCredentials"];
            var urlEncodedString = string.Format("email={0}&DepartmentCode={1}", emailAddress, "AUDI");

            var client = new RestClient(string.Format("{0}validateemail?{1}", ConfigurationManager.AppSettings["emailValidationServiceURL"], urlEncodedString));
            var request = new RestRequest(Method.GET);

            request.AddHeader("Authorization", "Basic " + basicAuthCredentials);

            IRestResponse response = client.Execute(request);

            if (!response.IsSuccessful)
            {
                return false;
            }

            var responseData = JsonConvert.DeserializeObject<List<EmailValidation2Info>>(response.Content, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(), NullValueHandling = NullValueHandling.Ignore }).FirstOrDefault();

            if (responseData.StatusCode == "0" || responseData.StatusCode == "1" || responseData.StatusCode == "3")
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// function to validate emailaddress using a regular expression.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        private bool ValidateEmailRegex(string emailAddress)
        {
            string regexExpression = "^((\"[\\w-\\s]+\")|([\\w-]+(?:\\.[\\w-]+)*)|(\"[\\w-\\s]+\")([\\w-]+(?:\\.[\\w-]+)*))(@((?:[\\w-]+\\.)*\\w[\\w-]{0,66})\\.([a-z]{2,6}(?:\\.[a-z]{2})?)$)|(@\\[?((25[0-5]\\.|2[0-4][0-9]\\.|1[0-9]{2}\\.|[0-9]{1,2}\\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\\]?$)";

            Regex regex = new Regex(regexExpression);
            Match match = regex.Match(emailAddress);

            if (match.Success)
                return true;

            return false;
        }

        #endregion
    }

    /// <summary>
    /// 
    /// </summary>
    public class Entry
    {
        /// <summary>
        /// 
        /// </summary>
        public string key { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string value { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class VerificationResults
    {
        /// <summary>
        /// 
        /// </summary>
        public List<Entry> entry { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class EmailValidationInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string domain { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string emailAddress { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string errorText { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string mailHost { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string user { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public VerificationResults verificationResults { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class EmailValidation2Info
    {
        /// <summary>
        /// 
        /// </summary>
        public string StatusCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string StatusDescription { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EmailAccount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EmailDomain { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Connected { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Disposable { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RoleAddress { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ErrorMessage { get; set; }
    }
}