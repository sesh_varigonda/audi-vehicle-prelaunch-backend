﻿using System;
using System.Linq;
using Audi.Prelaunch.Business.Entities;
using Audi.Prelaunch.Business.Models;
using Audi.Prelaunch.Business.Enum;
using HashidsNet;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.SqlSyntax;

namespace Audi.Prelaunch.Cms.Service
{
    /// <summary>
    /// 
    /// </summary>
    public class BookingService
    {
        private static readonly ISqlSyntaxProvider SqlSyntaxProvider = ApplicationContext.Current.DatabaseContext.SqlSyntax;
        private static readonly Database Database = ApplicationContext.Current.DatabaseContext.Database;

        /// <summary>
        /// Saving the Booking details in the database.
        /// </summary>
        /// <param name="orderRequestModel"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public BookingInfo CreateBooking(OrderRequestModel orderRequestModel, string source)
        {
            StripeService stripeService = new StripeService();
            BookingInfo bookingInfo = new BookingInfo() {  ChargeToken = orderRequestModel.payment.ChargeToken, TotalAmount = orderRequestModel.payment.Amount};

            //-- validate credit card via stripe --
            bookingInfo.ChargeIdentifier = stripeService.ValidateCardPayment(bookingInfo, source, bookingInfo.TotalAmount);

            TradeVehicleInfo tradeInDetails = null;
            if (orderRequestModel.TradeIn)
            {
                tradeInDetails = AddTradeInDetails(new TradeVehicleInfo()
                {
                    Make = orderRequestModel.tradeInDetails.Make,
                    Model = orderRequestModel.tradeInDetails.Model,
                    Kilometers = orderRequestModel.tradeInDetails.Kilometers,
                    Year = orderRequestModel.tradeInDetails.Year,
                    HasFinance = orderRequestModel.tradeInDetails.HasFinance
                });
            }

            //-- create booking as pending -- 
            var booking = AddBooking(new BookingInfo()
            {
                ChargeToken = bookingInfo.ChargeToken,
                ChargeIdentifier = bookingInfo.ChargeIdentifier,
                UserId = orderRequestModel.customer.UserId,
                EmailAddress = orderRequestModel.customer.Email,
                Status = (int)BookingStatusType.Pending,
                Description = string.Empty,
                TotalAmount = orderRequestModel.payment.Amount,
                ExteriorColourId = orderRequestModel.options.ExteriorColor.Id,
                InteriorColourId = orderRequestModel.options.InteriorColor.Id,
                Packages = string.Join(",", orderRequestModel.options.Packages.Select(a => a.Id).ToArray()),
                DealerCode = orderRequestModel.DealerCode,
                TradeIn = orderRequestModel.TradeIn,
                TradeVehicleId = orderRequestModel.TradeIn ? tradeInDetails.TradeVehicleId : 0,
                CreateDate = DateTime.UtcNow,
                CreateSource = "Web",
                UpdateDate = DateTime.UtcNow,
                UpdateSource = "Web",
                BookingNumber = "-",
                ContactCharging = bookingInfo.ContactCharging,
                ContactFinance = bookingInfo.ContactFinance,
                CommissionNumber = string.Empty,
                IDSCode = string.Empty
            }
                );

            //-- charge credit card via stripe --
            stripeService.CaptureCardPayment(bookingInfo.ChargeIdentifier, source);

            //-- generate booking number --
            var bookingNumber = GenerateBookingNumber(booking.BookingId, "AUDI_Prelaunch_ORDER");

            //-- activate order and assign order number --
            booking.BookingNumber = bookingNumber;
            booking.Status = (int) BookingStatusType.Paid;

            //Update Booking
            booking = UpdateBooking(booking);

            //-- Update Credit Details Via Stripe
            stripeService.UpdateCardPayment(booking, orderRequestModel, booking.ChargeIdentifier);

            return booking;
        }

        private static BookingInfo AddBooking(BookingInfo bookingInfo)
        {
            //var query = new Sql().Select("*").From<BookingInfo>(SqlSyntaxProvider);
            //var recordExists = Database.Fetch<BookingInfo>(query).Any();

            bookingInfo.UpdateDate = DateTime.UtcNow;
            bookingInfo.UpdateSource = "Web";
            bookingInfo.CreateDate = DateTime.UtcNow;
            bookingInfo.CreateSource = "Web";

            Database.Save(bookingInfo);

            return bookingInfo;
        }

        private static TradeVehicleInfo AddTradeInDetails(TradeVehicleInfo tradeInInfo)
        {
            //var query = new Sql().Select("*").From<BookingInfo>(SqlSyntaxProvider);
            //var recordExists = Database.Fetch<BookingInfo>(query).Any();

            tradeInInfo.UpdateDate = DateTime.UtcNow;
            tradeInInfo.UpdateSource = "Web";
            tradeInInfo.CreateDate = DateTime.UtcNow;
            tradeInInfo.CreateSource = "Web";

            Database.Save(tradeInInfo);

            return tradeInInfo;
        }

        private static BookingInfo UpdateBooking(BookingInfo bookingInfo)
        {
            bookingInfo.UpdateDate = DateTime.UtcNow;
            bookingInfo.UpdateSource = "Web";

            Database.Update(bookingInfo);

            return bookingInfo;
        }

        private static string GenerateBookingNumber(int bookingId, string salt)
        {
            var hashids = new Hashids(salt, 8, "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
            return hashids.Encode(bookingId, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);
        }
    }
}