﻿using System;
using System.Linq;
using Audi.Prelaunch.Business.Entities;
using Audi.Prelaunch.Business.Models;
using Audi.Prelaunch.Models;
using HashidsNet;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.SqlSyntax;

namespace Audi.Prelaunch.Cms.Service
{
    /// <summary>
    /// 
    /// </summary>
    public class CartService
    {
        private static readonly ISqlSyntaxProvider SqlSyntaxProvider = ApplicationContext.Current.DatabaseContext.SqlSyntax;
        private static readonly Database Database = ApplicationContext.Current.DatabaseContext.Database;

       /// <summary>
       /// Returns Cart Details by CartId
       /// </summary>
       /// <param name="cartId"></param>
       /// <returns></returns>
        public CartGetInfo GetCart(string cartId)
        {
            var query = new Sql().Select("*").From<CartInfo>(SqlSyntaxProvider);
            var cartInfo = Database.Fetch<CartInfo>(query).Where(a=>a.CartId == cartId).FirstOrDefault();

            if(cartInfo != null)
            {
                return new CartGetInfo() { EmailAddress = cartInfo.EmailAddress, JsonData = cartInfo.JsonData };
            }

            return null;
        }
    }
}