﻿using Audi.Prelaunch.Business.Entities;
using Audi.Prelaunch.Business.Models;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using Umbraco.Core.Persistence.SqlSyntax;

namespace Audi.Prelaunch.Business.Service
{
    /// <summary>
    /// 
    /// </summary>
    public class BookingNotificationService
    {
        private static readonly ISqlSyntaxProvider SqlSyntaxProvider = ApplicationContext.Current.DatabaseContext.SqlSyntax;
        private static readonly Database Database = ApplicationContext.Current.DatabaseContext.Database;

        /// <summary>
        /// returns list of all the notifications
        /// </summary>
        /// <returns></returns>
        public List<BookingNotificationInfo> GetList()
        {
            var query = new Sql().Select("*").From<BookingNotificationInfo>(SqlSyntaxProvider);
            var bookingNotifications = Database.Fetch<BookingNotificationInfo>(query).Where(a=>a.NotificationStatusType == (int)NotificationStatusType.Queued).ToList();

            return bookingNotifications;
        }
    }
}