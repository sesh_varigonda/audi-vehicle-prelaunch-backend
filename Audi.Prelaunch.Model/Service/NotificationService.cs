﻿using Audi.Prelaunch.Business.Entities;
using Audi.Prelaunch.Business.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using Umbraco.Core;
using Umbraco.Core.Persistence;
using System.Linq;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System.Text;
using Audi.Prelaunch.Marketo.Models;
using Audi.Prelaunch.Models;

namespace Audi.Prelaunch.Cms.Service
{
    /// <summary>
    /// 
    /// </summary>
    public class NotificationService
    {
        private static readonly Database Database = ApplicationContext.Current.DatabaseContext.Database;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingNotificationInfo"></param>
        /// <param name="source"></param>
        public void AddBookingNotification(BookingNotificationInfo bookingNotificationInfo, string source)
        {
            bookingNotificationInfo.CreateDate = DateTime.Now;
            bookingNotificationInfo.CreateSource = source;

            bookingNotificationInfo.UpdateDate = DateTime.Now;
            bookingNotificationInfo.UpdateSource = source;

            Database.Save(bookingNotificationInfo);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bookingNotificationInfo"></param>
        /// <param name="source"></param>
        public void UpdateBookingNotification(BookingNotificationInfo bookingNotificationInfo, string source)
        {
            bookingNotificationInfo.UpdateDate = DateTime.Now;
            bookingNotificationInfo.UpdateSource = source;

            Database.Update(bookingNotificationInfo);
        }

        public string AddCart(SaveOptionInfo saveOptionsRequestModel, string source)
        {
            CartInfo cartInfo = new CartInfo();

            cartInfo.CartId = Guid.NewGuid().ToString();
            cartInfo.EmailAddress = saveOptionsRequestModel.EmailAddress;
            cartInfo.FirstName = saveOptionsRequestModel.FirstName;
            cartInfo.LastName = saveOptionsRequestModel.LastName;
            cartInfo.JsonData = saveOptionsRequestModel.JsonData;
            cartInfo.CreateDate = DateTime.Now;
            cartInfo.CreateSource = source;

            Database.Save(cartInfo);

            return cartInfo.CartId;
        }

        /// <summary>
        /// Adding a OrderConfirmation to Notification Table.
        /// </summary>
        /// <param name="booking"></param>
        /// <param name="source"></param>
        /// <param name="orderRequestModel"></param>
        /// <returns></returns>
        public void QueueBookingConfirmation(OrderRequestModel orderRequestModel, BookingInfo booking, string source)
        {
            var invoiceUrl = string.Format("{0}/1/service/invoice/{1}", ConfigurationManager.AppSettings["ApiBaseUrl"], booking.BookingNumber);

            List<CampaignRequestToken> tokens = new List<CampaignRequestToken>()
            {
                new CampaignRequestToken() { name = "firstName", value = orderRequestModel.customer.FirstName},
                new CampaignRequestToken() { name = "lastName", value = orderRequestModel.customer.LastName},
                new CampaignRequestToken() { name = "email", value = orderRequestModel.customer.Email},
            };

            tokens.Add(new CampaignRequestToken() { name = "{{my.InvoiceUrl}}", value = invoiceUrl });

            var dealers = new StripeService().GetDealers();
            var dealerInfo = dealers.Where(a => a.ID == int.Parse(orderRequestModel.DealerCode)).FirstOrDefault();

            string payload = JsonConvert.SerializeObject(orderRequestModel);
            tokens.Add(new CampaignRequestToken() { name = "{{my.orderDetails}}", value = PersonaliseContent(payload, orderRequestModel, dealerInfo) });

            if(dealerInfo != null)
            {
                //tokens.Add(new CampaignRequestToken() { name = "{{my.dealerEmailAddress}}", value = dealerInfo.Email });
                tokens.Add(new CampaignRequestToken() { name = "{{my.dealerEmailAddress}}", value = orderRequestModel.customer.Email });
                tokens.Add(new CampaignRequestToken() { name = "{{my.dealerName}}", value = dealerInfo.Name });
            }

            AddBookingNotification(new BookingNotificationInfo()
            {
                BookingId = booking.BookingId,
                NotificationCode = "BookingConfirmation",
                Content = JsonConvert.SerializeObject(tokens)
            }, source);
        }

        /// <summary>
        /// Adding SaveOptions to the Notification Table.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="saveOptionsRequestModel"></param>
        /// <returns></returns>
        public void QueueSaveoptions(SaveOptionInfo saveOptionsRequestModel, string source)
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            dictionary.Add("Email", saveOptionsRequestModel.EmailAddress);
            dictionary.Add("FirstName", saveOptionsRequestModel.FirstName);
            dictionary.Add("LastName", saveOptionsRequestModel.LastName);

            List<CampaignRequestToken> tokens = new List<CampaignRequestToken>();
            foreach(var key in dictionary.Keys)
            {
                tokens.Add(new CampaignRequestToken() { name = key, value = dictionary[key] });
            }

            //Saved Options EmailToken
            var cartId = AddCart(saveOptionsRequestModel, source);
            var saveOptionsUrl = string.Format("{0}/order/design?cartId={1}", ConfigurationManager.AppSettings["WebsiteBaseUrl"], cartId);
            tokens.Add(new CampaignRequestToken() { name = "{{my.SaveOptionsUrl}}", value = saveOptionsUrl });

            AddBookingNotification(new BookingNotificationInfo()
            {
                BookingId = 0,
                NotificationCode = "SaveOptions",
                Content = JsonConvert.SerializeObject(tokens)
            }, source);
        }

        /// <summary>
        /// Added functionality to Send these details to the Dealer and Audi Australia.
        /// </summary>
        /// <param name="payload"></param>
        /// <param name="orderRequestModel"></param>
        /// <returns></returns>
        private string PersonaliseContent(string payload, OrderRequestModel orderRequestModel, DealerGetInfo dealerInfo)
        {
            JObject payloadItems = JsonConvert.DeserializeObject<JObject>(payload, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.AppendFormat("<div style=\"font-family:Calibri; \"><b>{0}</b></div>", "Prelaunch Order Details:");
            stringBuilder.Append("<br/>");

            foreach (var property in payloadItems.Properties())
            {
                foreach (var data in property)
                {
                    if (data.Type != JTokenType.Array && data.Type != JTokenType.Object)
                    {
                        if (!RemoveFields().Where(a => a == property.Name).Any())
                        {
                            stringBuilder.AppendFormat("{0}:{1}<br/>", FieldMapping().ContainsKey(property.Name) ? FieldMapping()[property.Name] : property.Name, data);
                        }

                        if(property.Name == "DealerCode")
                        {
                            stringBuilder.AppendFormat("{0}:{1}<br/>", "Selected Dealer", dealerInfo.Name);
                        }
                    }
                }
            }

            foreach (var property in payloadItems.Properties())
            {
                foreach (var data in property)
                {
                    if (data.Type == JTokenType.Object)
                    {
                        if(!RemoveSections().Where(a=>a == property.Name).Any())
                        {
                            var jObject = data as JObject;

                            stringBuilder.Append("<br/>");
                            stringBuilder.Append("<br/>");
                            stringBuilder.AppendFormat("<div style=\"font-family:Calibri; \"><b>{0}</b></div>", FieldMapping().ContainsKey(property.Name) ? FieldMapping()[property.Name] : property.Name);
                            stringBuilder.Append("<br/>");

                            foreach (var subProperty in jObject.Properties())
                            {
                                foreach (var subPropertyItem in subProperty)
                                {
                                    if (!RemoveFields().Where(a => a == subProperty.Name).Any())
                                    {
                                        stringBuilder.AppendFormat("{0}:", FieldMapping().ContainsKey(subProperty.Name) ? FieldMapping()[subProperty.Name] : subProperty.Name);
                                        if(subPropertyItem.Type != JTokenType.Array && subPropertyItem.Type != JTokenType.Object)
                                        {
                                            stringBuilder.AppendFormat("{0}", subPropertyItem);
                                        }
                                        else
                                        {
                                            if (subProperty.Name == "Packages")
                                            {
                                                stringBuilder.AppendFormat("{0}", string.Join(",", orderRequestModel.options.Packages.Select(a => a.Name).ToArray()));
                                            }
                                            else if (subProperty.Name == "ExteriorColor")
                                            {
                                                stringBuilder.AppendFormat("{0}", orderRequestModel.options.ExteriorColor.Name);
                                            }
                                            else if (subProperty.Name == "InteriorColor")
                                            {
                                                stringBuilder.AppendFormat("{0}", orderRequestModel.options.InteriorColor.Name);
                                            }
                                        }
                                    }
                                }
                                stringBuilder.Append("<br/>");
                            }
                        }
                    }
                }
            }

            return stringBuilder.ToString();
        }

        /// <summary>
        /// Returns FieldMapping List
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> FieldMapping()
        {
            Dictionary<string, string> items = new Dictionary<string, string>();

            items.Add("options", "Selected Options");
            items.Add("customer", "Customer Details");
            items.Add("tradeInDetails", "TradeIn Details");
            items.Add("LastName", "Last Name");
            items.Add("FirstName", "First Name");
            items.Add("HomePostcode", "Home Postcode");
            items.Add("HomeAddress", "Home Address");
            items.Add("HomeSuburb", "Home Suburb");
            items.Add("HomeState", "Home State");
            items.Add("HasFinance", "Has Finance");
            items.Add("TradeIn", "Trade In");
            items.Add("ContactFinance", "Finance Contact");
            items.Add("ContactCharging", "Jet Charging Contact");
            items.Add("ExteriorColor", "Exterior Color");
            items.Add("InteriorColor", "Interior Color");

            return items;
        }

        /// <summary>
        /// Returns The List of Fields to be Removed.
        /// </summary>
        /// <returns></returns>
        private List<string> RemoveFields()
        {
            List<string> items = new List<string>();

            items.Add("UserId");
            items.Add("DealerCode");

            return items;
        }

        /// <summary>
        /// Returns The List of Sections to be Removed.
        /// </summary>
        /// <returns></returns>
        private List<string> RemoveSections()
        {
            List<string> items = new List<string>();

            items.Add("payment");

            return items;
        }
    }
}