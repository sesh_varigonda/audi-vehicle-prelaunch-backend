﻿using System;
using Newtonsoft.Json;
using Our.Umbraco.Ditto;
using System.Collections.Generic;
using Newtonsoft.Json.Serialization;
using Audi.Prelaunch.Business.Models;

namespace Audi.Prelaunch.Business.Processors
{
    /// <summary>
    /// 
    /// </summary>
    public class PackageJsonProcessor : DittoProcessorAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override object ProcessValue()
        {
            return GetValue(Value);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public object GetValue(object Value)
        {
            try
            {
                if ((Value == null) || ((String.IsNullOrEmpty(Value.ToString())))) return null;

                if (Value != null)
                {
                    return JsonConvert.DeserializeObject<PackageGetInfo>(Value.ToString(), new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver(), NullValueHandling = NullValueHandling.Ignore, Formatting = Formatting.None });
                }
            }
            catch
            {
                //Suppress the exception.
            }

            return new List<string>().ToArray();
        }
    }
}