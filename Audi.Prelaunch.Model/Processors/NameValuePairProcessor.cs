﻿using System;
using Our.Umbraco.Ditto;
using Umbraco.Core.Models;
using System.Collections.Generic;
using Audi.Prelaunch.Business.Models;
using Audi.Prelaunch.Models;

namespace Audi.Prelaunch.Business.Processors
{
    /// <summary>
    /// 
    /// </summary>
    public class NameValuePairProcessor : DittoProcessorAttribute
    {
        /// <summary>
        /// translates umbraco property data to carousel object.
        /// </summary>
        /// <returns></returns>
        public override object ProcessValue()
        {
            if ((Value == null) || ((String.IsNullOrEmpty(Value.ToString())))) return null;

            if (Value != null)
            {
                List<NameValuePair> nameValueList = new List<NameValuePair>();
                var nameValueItems = ((List<IPublishedContent>)Value);

                foreach(var item in nameValueItems)
                {
                    var nameValue = item.As<NameValuePair>();
                    nameValueList.Add(nameValue);
                }

                return nameValueList;
            }

            return null;

        }
    }
}