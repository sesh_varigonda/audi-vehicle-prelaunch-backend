﻿using System;
using Our.Umbraco.Ditto;

namespace Audi.Prelaunch.Business.Processors
{
    /// <summary>
    /// 
    /// </summary>
    public class DateProcessor : DittoProcessorAttribute
    {
        /// <summary>
        /// translates umbraco property data to datetime value.
        /// </summary>
        /// <returns></returns>
        public override object ProcessValue()
        {
            return GetValue(Value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public object GetValue(object Value)
        {
            if ((Value == null) || ((String.IsNullOrEmpty(Value.ToString())))) return null;

            if (Value != null && !string.IsNullOrEmpty(Value.ToString()))
            {
                return Convert.ToDateTime(Value).ToString("s");
            }

            return null;
        }
    }
}