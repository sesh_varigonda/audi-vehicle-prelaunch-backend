﻿using System;
using Umbraco.Web;
using Our.Umbraco.Ditto;
using Umbraco.Core.Models;
using System.Linq;
using System.Collections.Generic;
using Audi.Prelaunch.Business.Translator;
using Audi.Prelaunch.Business.Model;

namespace Audi.Prelaunch.Business.Processors
{
    /// <summary>
    /// 
    /// </summary>
    public class DelimitedListAttribute : DittoProcessorAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        protected string _delimiter;

        /// <summary>
        /// 
        /// </summary>
        protected string _propertyName;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="delimiter"></param>
        public DelimitedListAttribute(string propertyName, string delimiter = ",")
        {
            _delimiter = delimiter;
            _propertyName = propertyName;
        }

        /// <summary>
        /// translates umbraco property data to a arraylist.
        /// </summary>
        /// <returns></returns>
        public override object ProcessValue()
        {
            return GetValue(Value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public object GetValue(object Value)
        {
            var umbracoHelper = new UmbracoHelper(UmbracoContext.Current);

            if (Value.GetType() == typeof(string))
            {
                var categories = Value.ToString();

                return string.IsNullOrWhiteSpace(categories)
                    ? new List<Category>()
                    : categories.Split(new[] { _delimiter }, StringSplitOptions.RemoveEmptyEntries)
                        .Select(x => new Category() { Name = umbracoHelper.GetPreValueAsString(int.Parse(x)), slug = umbracoHelper.GetPreValueAsString(int.Parse(x)).Trim().ToLower().Replace(" ", "-") })
                        .Where(x => !string.IsNullOrWhiteSpace(x.Name))
                        .ToList();
            }
            else if (Value != null || Value.GetType() == typeof(IPublishedContent))
            {
                var content = new ContentTranslator().GetContent((IPublishedContent)Value);

                if (content.Properties.ContainsKey(_propertyName))
                {
                    var categories = content.Properties[_propertyName].ToString();

                    return string.IsNullOrWhiteSpace(categories)
                        ? new List<Category>()
                        : categories.Split(new[] { _delimiter }, StringSplitOptions.RemoveEmptyEntries)
                            .Select(x => new Category() { Name = x.Trim(), slug = x.Trim().ToLower().Replace(" ", "-") })
                            .Where(x => !string.IsNullOrWhiteSpace(x.Name))
                            .ToList();
                }
            }

            return new List<Category>();
        }
    }
}