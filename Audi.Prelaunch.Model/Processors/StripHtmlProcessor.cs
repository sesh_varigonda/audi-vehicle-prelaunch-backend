﻿using System;
using Audi.Prelaunch.Business.Helpers;
using Our.Umbraco.Ditto;

namespace Audi.Prelaunch.Business.Processors
{
    /// <summary>
    /// 
    /// </summary>
    public class StripHtmlProcessor : DittoProcessorAttribute
    {
        /// <summary>
        /// function to strip html content from the umbraco property data.
        /// </summary>
        /// <returns></returns>
        public override object ProcessValue()
        {
            return GetValue(Value);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public object GetValue(object Value)
        {
            if ((Value == null) || ((String.IsNullOrEmpty(Value.ToString())))) return null;

            if (Value != null && !string.IsNullOrEmpty(Value.ToString()))
            {
                return HtmlRemoval.StripHtmlTags(Value.ToString());
            }

            return null;
        }
    }
}