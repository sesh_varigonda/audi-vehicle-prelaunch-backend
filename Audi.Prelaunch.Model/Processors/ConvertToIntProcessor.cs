﻿using System;
using Our.Umbraco.Ditto;

namespace Audi.Prelaunch.Business.Processors
{
    /// <summary>
    /// 
    /// </summary>
    public class ConvertToIntProcessor : DittoProcessorAttribute
    {
        /// <summary>
        /// translates umbraco property data to datetime value.
        /// </summary>
        /// <returns></returns>
        public override object ProcessValue()
        {
            if ((Value == null) || ((String.IsNullOrEmpty(Value.ToString())))) return 0;

            if (Value != null && !string.IsNullOrEmpty(Value.ToString()))
            {
                return Convert.ToInt32(Value);
            }

            return 0;

        }
    }
}