﻿using System;
using Our.Umbraco.Ditto;
using Umbraco.Core.Models;
using System.Collections.Generic;
using Audi.Prelaunch.Business.Models;
using Audi.Prelaunch.Business.Models;

namespace Audi.Prelaunch.Business.Processors
{
    /// <summary>
    /// 
    /// </summary>
    public class PackageProcessor : DittoProcessorAttribute
    {
        /// <summary>
        /// translates umbraco property data to carousel object.
        /// </summary>
        /// <returns></returns>
        public override object ProcessValue()
        {
            if ((Value == null) || ((String.IsNullOrEmpty(Value.ToString())))) return null;

            if (Value != null)
            {
                List<PackageInfo> packages = new List<PackageInfo>();
                var packagesItems = ((List<IPublishedContent>)Value);

                foreach(var item in packagesItems)
                {
                    var packageInfo = item.As<PackageInfo>();
                    packages.Add(packageInfo);
                }

                return packages;
            }

            return null;

        }
    }
}