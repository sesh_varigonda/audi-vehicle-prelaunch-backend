﻿using System;
using Umbraco.Web;
using Our.Umbraco.Ditto;
using Audi.Prelaunch.Business.Models;
using Umbraco.Core.Models;
using System.Collections.Generic;
using System.Linq;
using Audi.Prelaunch.Models;

namespace Audi.Prelaunch.Business.Processors
{
    /// <summary>
    /// 
    /// </summary>
    public class MediaUrlProcessor : DittoProcessorAttribute
    {
        /// <summary>
        /// translates umbraco property data to media url.
        /// </summary>
        /// <returns></returns>
        public override object ProcessValue()
        {
            return GetValue(Value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public object GetValue(object Value)
        {
            if ((Value == null) || ((String.IsNullOrEmpty(Value.ToString())))) return null;

            if (Value.GetType() == typeof(Media))
            {
                return (Value as Media);
            }

            var imageUrl = Value.ToString();

            try
            {
                if (imageUrl != null && !string.IsNullOrEmpty(imageUrl.ToString()))
                {
                    if (Value.GetType() == typeof(List<IPublishedContent>))
                    {
                        var mediaInfos = ((List<IPublishedContent>)Value).As<MediaInfo>();
                        return mediaInfos.Select(a => a.Url).ToList();
                    }
                    else
                    {
                        var mediaInfo = ((IPublishedContent)Value).As<MediaInfo>();
                        return mediaInfo.Url;
                    }
                }
            }
            catch
            {
                //Suppress Exception.
            }

            return null;
        }
    }
}