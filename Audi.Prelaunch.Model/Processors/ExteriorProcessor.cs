﻿using System;
using Our.Umbraco.Ditto;
using Umbraco.Core.Models;
using System.Collections.Generic;
using Audi.Prelaunch.Business.Models;
using Audi.Prelaunch.Business.Models;

namespace Audi.Prelaunch.Business.Processors
{
    /// <summary>
    /// 
    /// </summary>
    public class ExteriorProcessor : DittoProcessorAttribute
    {
        /// <summary>
        /// translates umbraco property data to carousel object.
        /// </summary>
        /// <returns></returns>
        public override object ProcessValue()
        {
            if ((Value == null) || ((String.IsNullOrEmpty(Value.ToString())))) return null;

            if (Value != null)
            {
                List<ExteriorInfo> exteriors = new List<ExteriorInfo>();
                var exteriorsItems = ((List<IPublishedContent>)Value);

                foreach(var item in exteriorsItems)
                {
                    var exteriorInfo = item.As<ExteriorInfo>();
                    exteriors.Add(exteriorInfo);
                }

                return exteriors;
            }

            return null;

        }
    }
}