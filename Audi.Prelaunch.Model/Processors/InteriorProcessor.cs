﻿using System;
using Our.Umbraco.Ditto;
using Umbraco.Core.Models;
using System.Collections.Generic;
using Audi.Prelaunch.Business.Models;
using Audi.Prelaunch.Business.Models;

namespace Audi.Prelaunch.Business.Processors
{
    /// <summary>
    /// 
    /// </summary>
    public class InteriorProcessor : DittoProcessorAttribute
    {
        /// <summary>
        /// translates umbraco property data to carousel object.
        /// </summary>
        /// <returns></returns>
        public override object ProcessValue()
        {
            if ((Value == null) || ((String.IsNullOrEmpty(Value.ToString())))) return null;

            if (Value != null)
            {
                List<InteriorInfo> interiors = new List<InteriorInfo>();
                var interiorsItems = ((List<IPublishedContent>)Value);

                foreach(var item in interiorsItems)
                {
                    var interiorInfo = item.As<InteriorInfo>();
                    interiors.Add(interiorInfo);
                }

                return interiors;
            }

            return null;

        }
    }
}