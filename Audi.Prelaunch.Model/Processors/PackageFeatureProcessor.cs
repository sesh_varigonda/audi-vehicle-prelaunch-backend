﻿using System;
using Our.Umbraco.Ditto;
using Umbraco.Core.Models;
using System.Collections.Generic;
using Audi.Prelaunch.Business.Models;

namespace Audi.Prelaunch.Business.Processors
{
    /// <summary>
    /// 
    /// </summary>
    public class PackageFeatureProcessor : DittoProcessorAttribute
    {
        /// <summary>
        /// translates umbraco property data to carousel object.
        /// </summary>
        /// <returns></returns>
        public override object ProcessValue()
        {
            if ((Value == null) || ((String.IsNullOrEmpty(Value.ToString())))) return null;

            if (Value != null)
            {
                List<PackageFeatureInfo> packageFeatures = new List<PackageFeatureInfo>();
                var packageFeaturesItems = ((List<IPublishedContent>)Value);

                foreach(var item in packageFeaturesItems)
                {
                    var packageFeatureInfo = item.As<PackageFeatureInfo>();
                    packageFeatures.Add(packageFeatureInfo);
                }

                return packageFeatures;
            }

            return null;

        }
    }
}