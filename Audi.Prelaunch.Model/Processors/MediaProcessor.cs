﻿using System;
using Umbraco.Web;
using Our.Umbraco.Ditto;
using Umbraco.Core.Models;
using Audi.Prelaunch.Business.Models;
using System.Xml.XPath;
using umbraco;
using System.Configuration;
using log4net;
using System.Reflection;
using Umbraco.Web.Models;
using Audi.Prelaunch.Models;

namespace Audi.Prelaunch.Business.Processors
{
    /// <summary>
    /// 
    /// </summary>
    public class MediaProcessor : DittoProcessorAttribute
    {
        ILog logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static string PROTOCOL_HTTP = "http";

        private const string DEFAULT_FOCAL_PERCENT = "50%";

        /// <summary>
        /// translates umbraco property data to media object.
        /// </summary>
        /// <returns></returns>
        public override object ProcessValue()
        {
            return GetValue(Value);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public object GetValue(object Value)
        {
            if ((Value == null) || ((String.IsNullOrEmpty(Value.ToString())))) return null;

            if (Value.GetType() == typeof(Umbraco.Core.Models.Media))
            {
                return (Value as Umbraco.Core.Models.Media);
            }

            var imageUrl = Value.ToString();

            try
            {
                if (imageUrl != null && !string.IsNullOrEmpty(imageUrl.ToString()))
                {
                    var helper = new UmbracoHelper(UmbracoContext.Current);
                    var image = helper.TypedMedia(imageUrl);

                    MediaInfo mediaInfo = null;
                    if (Value.GetType() == typeof(string))
                    {
                        mediaInfo = image.As<MediaInfo>();
                    }
                    else
                    {
                        mediaInfo = ((IPublishedContent)Value).As<MediaInfo>();
                    }

                    //Set focal point
                    var focalPoint = GetImageFocalPoint(mediaInfo.Id);
                    if(focalPoint != null)
                    {
                        mediaInfo.X = focalPoint.x;
                        mediaInfo.Y = focalPoint.y;
                    }
                    else
                    {
                        mediaInfo.X = DEFAULT_FOCAL_PERCENT;
                        mediaInfo.Y = DEFAULT_FOCAL_PERCENT;
                    }

                    return mediaInfo;
                }
            }
            catch
            {
                //Suppress Exception.
                //throw ex;
            }

            return null;
        }

        /// <summary>
        /// Returns Image FocalPoint
        /// </summary>
        /// <param name="mediaId"></param>
        /// <returns></returns>
        public FocalPoint GetImageFocalPoint(string mediaId)
        {
            decimal focalPointLeft = 0;
            decimal focalPointTop = 0;

            try
            {
                decimal imageWidth = 0;
                decimal imageHeight = 0;
                if (string.IsNullOrEmpty(mediaId) || mediaId == "0") return null;

                var crops = new UmbracoHelper(UmbracoContext.Current).Media(mediaId).GetPropertyValue<ImageCropDataSet>("umbracoFile");

                if (crops == null || crops.FocalPoint == null) return null;

                var image = GetImageProperties(mediaId);

                if (image == null || string.IsNullOrEmpty(image.width) || string.IsNullOrEmpty(image.height)) return null;

                imageWidth = decimal.Parse(image.width);
                imageHeight = decimal.Parse(image.height);

                focalPointLeft = ((crops.FocalPoint.Left * imageWidth) / imageWidth) * 100;
                focalPointTop = ((crops.FocalPoint.Top * imageHeight) / imageHeight) * 100;
            }
            catch
            {
                //Suppress the Exception
            }

            return new FocalPoint() { x = string.Format("{0}%", Convert.ToInt32(focalPointLeft)), y = string.Format("{0}%", Convert.ToInt32(focalPointTop)) };
        }


        /// <summary>
        /// Returns the Media Source Url by MediaId
        /// </summary>
        /// <param name="mediaId">Umbraco MediaId</param>
        /// <returns></returns>
        public Model.Image GetImageProperties(string mediaId)
        {
            Model.Image imageInfo = new Model.Image();
            try
            {
                if (string.IsNullOrEmpty(mediaId)) return imageInfo;

                var data = library.GetMedia(int.Parse(mediaId), true).Current;
                XPathNodeIterator nodes = data.SelectDescendants(XPathNodeType.All, false);

                while (nodes.MoveNext())
                {
                    var node = nodes.Current;
                    if (node.Name == "umbracoFile")
                    {
                        string urlPath = node.InnerXml;
                        if (urlPath != null)
                        {
                            if (!urlPath.ToLower().Contains(PROTOCOL_HTTP))
                            {
                                imageInfo.src = string.Format("{0}{1}", ConfigurationManager.AppSettings["BaseUrl"], urlPath);
                            }
                            else
                            {
                                imageInfo.src = urlPath;
                            }
                        }
                    }
                    if (node.Name == "umbracoWidth")
                    {
                        imageInfo.width = node.InnerXml;
                    }
                    if (node.Name == "umbracoHeight")
                    {
                        imageInfo.height = node.InnerXml;
                    }
                }

                return imageInfo;
            }
            catch (Exception ex)
            {
                logger.Error(ex.StackTrace);
            }

            return imageInfo;
        }

    }

    public class FocalPoint
    {
        public string x { get; set; }
        public string y { get; set; }
    }
}