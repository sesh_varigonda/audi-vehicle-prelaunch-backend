﻿using System;
using Our.Umbraco.Ditto;
using Umbraco.Core.Models;
using System.Collections.Generic;
using Audi.Prelaunch.Business.Models;
using Audi.Prelaunch.Models;

namespace Audi.Prelaunch.Business.Processors
{
    /// <summary>
    /// 
    /// </summary>
    public class FAQProcessor : DittoProcessorAttribute
    {
        /// <summary>
        /// translates umbraco property data to carousel object.
        /// </summary>
        /// <returns></returns>
        public override object ProcessValue()
        {
            if ((Value == null) || ((String.IsNullOrEmpty(Value.ToString())))) return null;

            if (Value != null)
            {
                List<FAQInfo> faqCollection = new List<FAQInfo>();
                var faqItems = ((List<IPublishedContent>)Value);

                foreach(var item in faqItems)
                {
                    var faqInfo = item.As<FAQInfo>();
                    faqCollection.Add(faqInfo);
                }

                return faqCollection;
            }

            return null;

        }
    }
}