﻿using System;
using Newtonsoft.Json;
using Our.Umbraco.Ditto;
using System.Collections.Generic;
using Umbraco.Core.Models;
using Audi.Prelaunch.Business.Models;
using Newtonsoft.Json.Serialization;
using System.Linq;
using Umbraco.Web;
using Audi.Prelaunch.Business.Translator;

namespace Audi.Prelaunch.Business.Processors
{
    /// <summary>
    /// 
    /// </summary>
    public class TagProcessor : DittoProcessorAttribute
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override object ProcessValue()
        {
            return GetValue(Value);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="Value"></param>
        /// <returns></returns>
        public object GetValue(object Value)
        {
            try
            {
                if ((Value == null) || ((String.IsNullOrEmpty(Value.ToString())))) return null;

                if (Value != null && !string.IsNullOrEmpty(Value.ToString()) && Value.GetType() == typeof(string[]))
                {
                    return (string[])Value;
                }
                else
                {
                    var content = new ContentTranslator().GetContent((IPublishedContent)Value);
                    if (content.Properties.ContainsKey("tags"))
                    {
                        var tags = JsonConvert.DeserializeObject<List<string>>(content.Properties["tags"].ToString(), new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() }).ToArray();
                        return tags;
                    }
                }
            }
            catch
            {
                //Suppress the exception.
            }

            return new List<string>().ToArray();
        }
    }
}